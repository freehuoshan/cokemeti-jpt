<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@include file="top.jsp" %>
<html>
<head>
    <title>Login | JPT CRM</title>
    <link href="<%=basePath%>/static/imgs/favicon.ico" rel="SHORTCUT ICON" >
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/static/css/login.css"/>
</head>
<body>
<div class="lhead">
    <div>
    	<img style="margin-bottom:-60px" width="10%" , height="15%" src="<%=basePath%>/static/imgs/webwxgetmsgimg.jpg"/><span style="margin-top:0;font-size:20px;margin-left:-40px">六度智囊</span>
    </div>
</div><!--lhead end-->
<div class="lcontent">
    <div class="loginform">
        <p class="f30 mt_80 loginform_title">欢迎登录</p>
        <form action="login" id="submitForm" method="post">
            <div class="lginfo relative mt_32">
                <input type="text" class="lg_text itext form-control"  id="username" name="username" placeholder="用户名">
                <em class="red error f12 ml_10">${msg}</em>
            </div>
            <div class="lginfo relative mt_22">
                <input type="password" class="lg_text itext form-control"  id="password" name="password" placeholder="密码">
            </div>
<!--             <div class="lginfo relative mt_22 clearfix">
                <input type="text" class="lg_text lg_text_yz itext form-control fleft" value="" id="code" name="code" placeholder="验证码">
                <img src="/imgCode" id="kaptchaImage" alt=""  class="fleft yanzh_img">
            </div> -->
            <div class="lginfo relative mt_22">
                <a href="" title="" class="blue">忘记密码？</a>
            </div>
            <div class="lginfo relative mt_22 clearfix">
                <input type="submit" class="lgsubmit" value="登录">
            </div>
        </form>
    </div>
</div><!--lcontent end-->
<div class="lfoot tc">
    <span class="c6">©2014-2016 JPT &nbsp;&nbsp;浙ICP备15016159</span>
</div><!--lfoot end-->

<script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery.validate.js"></script>
<script type="text/javascript">
$(function(){

    lfootMargin();
    $(window).resize(function(){
        lfootMargin();
    });

    function lfootMargin(){
        var w_height = $(window).height(),
            h_height = $('.lhead').outerHeight(),
            c_height = $('.lcontent').outerHeight(),
            f_height = $('.lfoot').outerHeight(),
            h = w_height - h_height -c_height - f_height;
        if(h>50){
            $('.lfoot').css('margin-top',h);
        }else{
            $('.lfoot').css('margin-top','50px');
        }

    }
        
        $('#username').focus();
   
    
    var validate = $("#submitForm").validate({
        rules:{
            username:{
                required:true,
                noinputCheck:true
            },
            password:{
                required:true,
                rangelength:[6,20]
            }
        },
        messages:{
            username:{
                required:"请输入用户名",
                noinputCheck:'请输入用户名'
            },
            password:{
                required: "请填写密码",
                rangelength: "密码为6-20位"
            }                 
        },
        errorElement:"em",
        errorPlacement: function(error, element) { 
		    error.appendTo(element.parent()).addClass("absolute lgerror");   
		}
    }); 
	
});
</script>
</body>
</html>