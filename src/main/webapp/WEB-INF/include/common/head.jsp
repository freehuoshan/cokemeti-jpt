<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!-- top navigation -->
    <div class="top_nav">
      <div class="nav_menu">
        <nav>
          <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
          </div>

          <ul class="nav navbar-nav navbar-right">
            <li class="">
              <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="<%=basePath%>/static/imgs/user.png" alt=""> ${sessionScope.username}
                <span class=" fa fa-angle-down"></span>
              </a>
              <ul class="dropdown-menu dropdown-usermenu pull-right">
                <li><a href="javascript:;">个人中心</a></li>
                <li><a href="<%=basePath%>/logout"><i class="fa fa-sign-out pull-right"></i>退出</a></li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </div>
<!-- /top navigation -->