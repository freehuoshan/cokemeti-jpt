            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<%=basePath%>/static/imgs/user.png" alt="<%=basePath%>/static/imgs/user.png" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> ${sessionScope.username}</h2>
              </div>
            </div>
            <br />