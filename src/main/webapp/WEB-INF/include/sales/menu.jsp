<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-stethoscope"></i> 客户 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<%=basePath%>/sales/customer/index">客户列表</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-user"></i> 专家库 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<%=basePath%>/analyst/professional/search">搜索</a></li>
						<li><a href="<%=basePath%>/analyst/professional/file">上传专家库</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> 项目 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<%=basePath%>/analyst/project/list">项目</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> 统计 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->