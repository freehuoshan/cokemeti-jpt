<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
<head>
	    <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
</head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
           <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<%=basePath%>/static/imgs/user.png" alt="<%=basePath%>/static/imgs/user.png" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
              </div>
            </div>
            <br />
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        	
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>客户列表</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li ><a style="color:#5A738E;" class="" href="<%=basePath%>/sales/add"><i class="fa fa-plus"></i> 添加客户</a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                    </p>
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                    </table>
                  </div>
                </div>
              </div>
        
        </div>
        
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    <!-- Datatables -->
	<link rel="stylesheet" type="text/css" href="<%=path%>/static/css/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/static/vendors/easyui/themes/metro-gray/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/static/vendors/easyui/themes/icon.css"/>
	<script type="text/javascript" src="<%=path%>/static/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/static/vendors/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/static/vendors/easyui/locale/easyui-lang-zh_CN.js"></script>
    <script>
    $(document).ready(function() {
    	datagrid = $('#datatable-buttons').datagrid({
    		url:'<%=path%>/sales/customers',
    		queryParams : {},			
         	singleSelect:true, 
    		loadMsg:'数据加载中......',
    		//fit: true, 
    		nowrap: true,
    		idField : 'id',
    		striped:true, 
    		fitColumns: true,
    		remoteSort: false ,
    		pagination:true,
    		width:'100%', 
    	    height:'auto', 
    	    pageSize:30,
    		pageList: [10,20,30,50,100],
    		loading:true,
    		rownumbers:true,
    		columns:[[
    				//查询计划详情
    				{field:"id",title:'id',width:20,align:'center',hidden:true},
    				{field:'username',title:'客户名称',width:10,align:'center',
    					formatter:function(value,row,index){
    						return '<a  href="<%=path%>/sales/edit/'+ row.id+'">'+ row.username +'</a>';
    					}
    				},
    				{field:'name',title:'公司名称',width:40,align:'center'},
    				{field:'status',title:'状态',width:10,align:'center',
    					formatter:function(value,row,index){
    						if(value==1){
    							return "新注册";
    						}else if(value==2){
    							return "活跃";
    						}else if(value==3){
    							return "即将到期";
    						}else if(value==4){
    							return "注销";
    						}
    					}
    				},
    				{field:'level',title:'等级',width:10,align:'center'},
    				{field:'amount',title:'账户余额',width:10,align:'center'},
    				/* {field:'action',title:'操作',width:70,align:'center',
    					formatter:function(value,row,index){
    						if(row.ischeck==0 && row.isposting==0){
    							return '<a class="ybtn" href="#" onclick="return showalert('+ row.id +');">审核</a>';
    						}else if(row.ischeck==1){
    							return '<a class="ybtn" href="#" onclick="return showalert('+ row.id +');">查看详情</a>';
    						}
    					}
    				}, */
    		]],
    		onLoadSuccess : function(data) {
    			/* var panel = $(this).datagrid('getPanel');   
              var td = panel.find('div.datagrid-body td'); 
           	 td.css({"backgroundcolor":"red"});
    			screenHeight();
    			if(data.rows.length==0){
    				var body = $(this).data().datagrid.dc.body2;
    				body.find('table tbody').append('<tr><td width="'+body.width()+'" style="height: 25px; text-align: center;" colspan="6">暂无数据</td></tr>');
    			} */
    		},
    		onDblClickRow :function(rowIndex,rowData){
    			
    		}
    	});
    	
    	 var p = datagrid.datagrid('getPager');
    	 $(p).pagination({
    		showPageList:true,
    		beforePageText: '第', 
    		afterPageText: '页	 共 {pages} 页',
    		displayMsg: '当前显示 {from} - {to} 条记录	 共 {total} 条记录',
    		onBeforeRefresh:function(){ 
				alert("ddd");
    		}
    	 });
    });
    </script>
  </body>
</html>