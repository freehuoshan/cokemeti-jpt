<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
        <!-- 项目 -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>项目信息</h3>
              </div>
            </div>
            
            <!-- 基本信息 -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>项目列表 <small style="color:red"></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-projects" class="table table-striped table-bordered ">
                      <thead>
                        <tr>
                          <th>项目名称</th>
                          <th>客户</th>
                          <th>项目状态</th>
                          <th>开始时间</th>
                          <th>结束时间</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
        	</div>
        </div><!-- 项目 -->