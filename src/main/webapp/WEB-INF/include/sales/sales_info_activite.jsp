<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
        <!-- 客户活动 -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>客户活动</h3>
              </div>
            </div>
            
            <!-- 基本信息 -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>活动列表 <small style="color:red"></small></h2>
               		 <small style="color: red">
						<button id="activite" type="button" class="btn btn-success"  data-toggle="modal" data-target=".bs-modal-requirement">新增活动</button>
					 </small>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="activite-datatable-buttons" class="table table-striped table-bordered">
	 					<thead>
	                        <tr>
	                          <th>活动主题</th>
	                          <th>联系方式</th>
	                          <th>开始时间</th>
	                          <th>结束时间</th>
	                          <th>活动内容</th>
	                          <th>删除操作</th>
	                          <th>编辑操作</th>
	                        </tr>
	                   </thead>
                    </table>
                  </div>
                </div>
              </div>
        	</div>
        </div><!-- 客户活动 -->