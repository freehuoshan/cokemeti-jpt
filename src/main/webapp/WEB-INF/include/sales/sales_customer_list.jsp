<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
<head>
	    <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
</head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        	
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>客户列表
               		 <small style="color: red">
						<button id ="add" type="button" class="btn btn-success"  data-toggle="modal" data-target=".bs-modal-requirement">添加客户</button>
					 </small>                    
                    </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
	 					<thead>
	                        <tr>
	                          <th>客户名称</th>
	                          <th>公司名称</th>
	                          <th>状态</th>
	                          <th>等级</th>
	                          <th>账户余额</th>
	                        </tr>
	                   </thead>
                    </table>
                  </div>
                </div>
              </div>
        </div>
        
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

   
    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- Datatables -->
    <script src="<%=basePath%>/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    <script>
    $(document).ready(function() {
    	if ($("#datatable-buttons").length) {        	  
            var table = $("#datatable-buttons").DataTable({
              "processing": true,
              "serverSide": true,
              "stateSave": true,
              "ajax":{
              	url:'<%=basePath%>/sales/customer/getlist',
              	type:'POST',
              },
              "columns": [
                          { "data": "username" },
                          { "data": "name" },
                          { "data": "status" },
                          { "data": "level" },
                          { "data": "amount" }
                      ],
         	  "rowId":"id",
              "searching":true,
              "searchDelay": -1,
              "info":false,
          	  "ordering":false
            });
          }
    	
    	$("#datatable-buttons tbody").delegate('tr','dblclick',function(){
    		var id = $(this).attr("id");
    		location.href = '<%=basePath%>/sales/customer/edit/'+id;
    	});
    	
    	$("#add").click(function(){
    		location.href="<%=basePath%>/sales/customer/add"
    	});
    });
    </script>
  </body>
</html>