<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
 <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
     <link href="<%=basePath%>/static/css/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    	
    	.properties_name{
    		    margin-top: 10px;
    			text-align: right;
    	}
    	
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
             <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <c:if test="${type==0}">
                <h3>添加活动</h3>
              </c:if>
              <c:if test="${type==1}">
                <h3>修改活动</h3>
              </c:if>
              </div>
            </div>
            
            <!-- 基本信息 -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>活动信息 <small style="color:red"></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <br />
                  <form id="form" class="form-horizontal form-label-left input_mask">

                  	<input type="hidden" value="${vo.id}" id="id">
                  	<input type="hidden" value="${activityInfo.id}" id="activityId">
                  	<!-- 第一行 -->
                  	<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">活动主题：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control " value="${activityInfo.theme}"  placeholder="活动主题" id="theme">
                      </div>
                      
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">活动方式：</label>
						<div class="col-md-2 col-sm-6 col-xs-12">
                          <select id ="method" class="form-control" value="${activityInfo.method}">
                            <option>电话</option>
                            <option>邮件</option>
                            <option>其他</option>
                          </select>
                        </div>
					  
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">活动时间：</label>
						<div class="col-md-3 xdisplay_inputx form-group has-feedback">
                             <input type="text" class="form-control has-feedback-left"  id="single_cal" placeholder="First Name" aria-describedby="inputSuccess2Status2">
                             <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                             <span id="inputSuccess2Status2" class="sr-only">(success)</span>
	                   </div>
					</div>
					
					<!-- 第二行 -->
					<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">客户：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control " placeholder="客户" id="customer"name="customer" value="${vo.username}">
                      </div>
					  <!-- <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">项目名称：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control" placeholder="项目名称" id="projectid"name="projectid">
                      </div> -->
                        <label class="col-md-1 col-sm-6 col-xs-12 properties_name">项目名称：</label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                          <select class="form-control" id="projectid" value="${activityInfo.projectId}">
                          	<option id="0">Choose option</option>
                          	<c:forEach var="item" items="${pjlist}" step="1">
	                            <option value="${item.id}" id="${item.id}">${item.name}</option>
                          	</c:forEach>
                          </select>
                        </div>
                      <%-- <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">创建者：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control" placeholder="创建者" id="creater"name="creater" value="${vo.creater}">
                      </div> --%>
					</div>
					
					<!-- 第二行 -->
					<div class="form-group">
					  <label class=" col-md-1 col-sm-2 col-xs-12 properties_name">活动内容：</label>
                      <div class="col-md-10 col-sm-10 col-xs-12  ">
                       <textarea class="form-control" rows="9"  placeholder="活动内容" cols="9"id="description" name="description" >${activityInfo.content}</textarea>
                      </div>
					</div>
					
                    <!-- 第三行 -->  
                    <div class="form-group">
                        <div class="col-md-12 col-sm-9 col-xs-12 " style="text-align : right;">
                          <button id="submit_buttion" type="button" class="btn btn-primary ">修改并保存</button>
                         <!--  <button type="submit" class="btn btn-success">Submit</button> -->
                        </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
        	</div>
        </div>
       </div>
       </div>
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- bootstrap-daterangepicker -->
    <script src="<%=basePath%>/static/vendors/moment/min/moment.min.js"></script>
    <script src="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    <script src="<%=basePath%>/static/js/sweetalert/sweetalert.min.js"></script>
    <script>
    $('#submit_buttion').click(function(){
    	var theme = $("#theme").val();
    	var method = $("#method").val();
    	var single_cal = $("#single_cal").val();
    	var projectid = $("#projectid").find("option:selected").attr("id");
    	var description = $("#description").val();
    	var type = '${type}';
    	var id = '${vo.id}';
    	var activityId = $("#activityId").val();
    	if(type == 0){
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/sales/activite/save",
    			data:{'id':id,'type':type,'theme':theme,'method':method,'single_cal':single_cal,'projectid':projectid,'description':description},
    			dataType:'text',
    			success:function(d){
    				var result = JSON.parse(d);
    				alert(result.msg)
    				if(result.code==100){
    					location.href = '<%=basePath%>/sales/customer/edit/'+result.data;
    				}
    			}
    		});
    	}
    	else{
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/sales/activite/editoperate",
    			data:{'id':id,'type':type,'theme':theme,'method':method,'single_cal':single_cal,'projectid':projectid,'description':description,"activityId":activityId},
    			dataType:'text',
    			success:function(d){
    				if(d == "成功"){
    					swal({
    	 					title: "",
    	 					text: d,
    	 					type: "success"
    	 				});
    					setTimeout(function(){
    						location.href='<%=basePath%>/sales/customer/index';
    					},1000);
    					
    				}
    				else{
    					swal({
    	 					title: "",
    	 					text: d,
    	 					type: "error"
    	 				});
    				}
    			}
    		});
    	}
    	
    	
    	/* $('#single_cal2').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_2"
          }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
          }); */
    	
    });
    $(function(){
    	if($('#type').val()==1){
	    var txt = '${vo.orgNo}';
	    $("#description").html(txt);
    	}
    	$("#projectid").val('${activityInfo.projectId}');
    	$("#method").val('${activityInfo.method}');
    });
    $('#single_cal').daterangepicker({
    	timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'YYYY/MM/DD HH:mm'
        }
    	});
    </script>
  </body>
</html>