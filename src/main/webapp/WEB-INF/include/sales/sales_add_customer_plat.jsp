<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
 <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    	
    	.properties_name{
    		    margin-top: 10px;
    			text-align: right;
    	}
    	
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <input type="hidden" value="${type}" id="type">
			<%@include file="sales_info_custom.jsp" %>
			<%@include file="sales_info_attachment.jsp" %>
			<c:if test="${type==1}">
				<%@include file="sales_info_project.jsp" %>
				<%@include file="sales_info_activite.jsp" %>
			</c:if>
       </div>
       </div>
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <script src="<%=basePath%>/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    
        <!-- Dropzone.js -->
    <script src="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
	<script src="<%=basePath%>/static/js/sweetalert/sweetalert.min.js"></script>
    <script>
    var atttable;
    
    $('#delete_button').click(function(){
    	swal({
	        title: "您确定要删除这条信息吗",
	        text: "删除后将无法恢复，请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "删除",
	        cancelButtonText: "取消",
	        closeOnConfirm: false
	    }, function(){
	    	var id = $("#id").val();
	    	$.ajax({
				type:"post",
				url:"<%=basePath%>/sales/customer/delete",
				data:{'id':id},
				dataType:'text',
				success:function(d){
					if(d == '成功'){
						swal({
		 					title: "",
		 					text: d,
		 					type: "success"
		 				});
						setTimeout(function(){
							location.href='<%=basePath%>/sales/customer/index';
						},1000);
						
					}
					else{
						swal({
		 					title: "",
		 					text: d,
		 					type: "error"
		 				});
					}
				}
			});
	    });
    });
    
    $('#submit_buttion').click(function(){
    	var name = $("#name").val();
    	var phone = $("#phone").val();
    	var email = $("#email").val();
    	var company = $("#company").val();
    	var industry = $("#industry").val();
    	var website = $("#website").val();
    	var description = $("#description").val();
    	var prepay = $("#prepay").val();
    	var type = $("#type").val();
    	var id = $("#id").val();
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/sales/customer/save",
			data:{'id':id,'type':type,'prepay':prepay,'name':name,'phone':phone,'email':email,'company':company,'industry':industry,'website':website,'description':description},
			dataType:'text',
			success:function(d){
				var result = JSON.parse(d);
				alert(result.msg)
				if(result.code==100){
	                location.href='<%=basePath%>/sales/customer/index';
				}
			}
		});
    });
    $('#activite').click(function(){
    	var id = $("#id").val();
        location.href='<%=basePath%>/sales/activite/add/' + id;
    });
    $(function(){
    	if($('#type').val()==1){
		    getAttachementInfo();//查询附件
    	}
    	//活动
    	if ($("#activite-datatable-buttons").length) {
    		var customerid = $("#id").val();
            var table = $("#activite-datatable-buttons").DataTable({
              "processing": true,
              "serverSide": true,
              "stateSave": true,
              "ajax":{
              	url:'<%=basePath%>/sales/activite/getlist',
              	type:'POST',
              	data:{customerid:customerid}
              },
              "columns": [
                          { "data": "theme" },
                          { "data": "method" },
                          { "data": "starttime" },
                          { "data": "endtime" },
                          { "data": "content" },
                          { "data": "id"},
                          { "data": "id"}
                      ],
             "columnDefs":[
                          	{
                           	targets: 5,
                           	render: function (data, type, row, meta) {
                           		return '<button type="button" onclick="deleteActivity('+data+','+customerid+')" class="btn btn-success btn-xs">删除</button>';
                           	}
                      	  	},
   	                   	 {
   	                        	targets: 6,
   	                        	render: function (data, type, row, meta) {
   	                        		return '<button type="button" onclick="toUpdatePage('+data+')" class="btn btn-success btn-xs">编辑</button>';
   	                        	}
   	                   	  	}
                      ], 
               
         	  "rowId":"id",
              "searching":true,
              "searchDelay": -1,
              "info":false,
          	  "ordering":false
            });
       }
    	
    	//项目
    	if ($("#datatable-projects").length) {
    		var customerid = $("#id").val();
            var table = $("#datatable-projects").DataTable({
	              "responsive": true,
	              "processing": true,
	              "serverSide": true,
	              "stateSave": true,
	              "ajax":{
	              	url:'<%=basePath%>/analyst/project/list',
	              	type:'POST',
	              	data:{customerid:customerid}
	              },
	              "columns": [
	                          { "data": "name" },
	                          { "data": "customerId" },
	                          { "data": "status" },
	                          { "data": "startDate" },
	                          { "data": "endDate" }
	                      ],
	         	  "rowId":"id",
	              "searching":true,
	              "searchDelay": -1,
	              "info":false,
	          	  "ordering":false
        	});
            
            $('#datatable-projects tbody').on( 'dblclick', 'tr', function () {
            	var data = table.row( this ).data();;
        		location.href = '<%=basePath%>/analyst/project/'+data.id;
       	 	});
    	}
    });
    
    function toUpdatePage(activityId){
    	location.href = '<%=basePath%>/sales/activite/edit/activity/'+activityId;
    	<%-- $.ajax({
			type:"post",
			url:"<%=basePath%>/sales/activite/edit/activity/",
			data:{"activityId":activityId},
			dataType:'text',
			success:function(activityJson){
				alert($.parseJSON(activityJson));
				
			}
		}); --%>
    	
    }
    
    function deleteActivity(activityId,customId){
    	swal({
	        title: "您确定要删除这条信息吗",
	        text: "删除后将无法恢复，请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "删除",
	        cancelButtonText: "取消",
	        closeOnConfirm: false
	    }, function(){
	    	$.ajax({
				type:"post",
				url:"<%=basePath%>/sales/activite/delete",
				data:{"activityId" : activityId},
				dataType:'text',
				success:function(d){
					if(d == "成功"){
						swal({
		 					title: "",
		 					text: d,
		 					type: "success"
		 				});
						setTimeout(function(){
							location.href='<%=basePath%>/sales/customer/edit/'+customId;
						},1000);
						
					}
					else{
						swal({
		 					title: "",
		 					text: d,
		 					type: "error"
		 				});
					}
				}
			});
	    });
    	
	
	}
    
    Dropzone.autoDiscover = false;
    var dropz = new Dropzone("#myDropzone", {
  	  url: "<%=basePath%>/file/upload",
        addRemoveLinks: true,
        dictRemoveLinks: "x",
        dictCancelUpload: "x",
        maxFiles: 10,
        maxFilesize: 50,
		  init: function() {
            this.on("success", function(file, response) {
          	 if(response != "0"){
          		 var string = response;
          		 var arr = string.split('.');
          		 var id = arr[0];
          		 var data = response;
          		 var html = '<input type="hidden" id='+id+' value="'+data+ '" data-filename ="' +file.name+'"/>';
          		 $("#files").append(html);
          	 }
            });
            this.on("removedfile", function(file) {
       		 var string = file.name;
      		 var arr = string.split('.');
      		 var id = arr[0];
               $('#'+id).remove();
            });
        }
    });
  
    $('#report_save').click(function(){
		var files='';
		var filenames='';
    	$("#files input[type='hidden']").each(function(){
    	   files = files+"|"+$(this).val()
    	   filenames = filenames + "|" + $(this).data("filename");
    	});  
		if(files != ''){
			var customerid = $("#id").val();
			if(customerid=='')
				alert("请先创建客户");
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/sales/attachement/add",
    			data:{"reports":files,"filenames":filenames, "customerid":customerid},
    			dataType:'json',
    			async:false,
    			success:function(d){
    				alert(d.msg);
    				$("#report_close").click();
    				dropz.removeAllFiles();
    				getAttachementInfo();
    			}
    		});
		}else{
			alert("请上传附件");
		}
    });
    
    function getAttachementInfo(){
    	var url = '<%=request.getSession().getAttribute("url")%>';
    	if ($("#attachement-datatable-buttons").length) {
    		var customerid = $("#id").val();
    		if(typeof(atttable)=='undefined'){
    			atttable = $("#attachement-datatable-buttons").DataTable({
	              "processing": true,
	              "serverSide": true,
	              "stateSave": true,
	              "ajax":{
	              	url:'<%=basePath%>/sales/attachement/getlist',
	              	type:'POST',
	              	data:{customerid:customerid}
	              },
	              "columns": [
	                          { "data": "realname" },
	                          { "data": "ctime" },
	                          { "data": "id" }
	                      ],
                    "columnDefs":[
                       	{
                        	targets: 0,
                        	render: function (data, type, row, meta) {
                            return '<a href="'+url+row.name+'">' + data +'</a>';
                        	}
                   	  	},
	                   	 {
	                        	targets: 2,
	                        	render: function (data, type, row, meta) {
	                        		return '<button type="button" onclick="deleteAttachement('+data+')" class="btn btn-success btn-xs">删除</button>';
	                        	}
	                   	  	}
                   ],  
	         	  "rowId":"id",
	              "searching":true,
	              "searchDelay": -1,
	              "info":false,
	          	  "ordering":false
	            });
    		}else{
    			atttable.ajax.reload();
    		}
       }
    	
    }
    
    function deleteAttachement(attachementId){
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/sales/attachement/delete",
			data:{"attachementId" : attachementId},
			dataType:'text',
			success:function(d){
				if(d == "成功"){
					swal({
	 					title: "",
	 					text: d,
	 					type: "success"
	 				});
					setTimeout(function(){
						location.href='<%=basePath%>/sales/customer/index';
					},1000);
					
				}
				else{
					swal({
	 					title: "",
	 					text: d,
	 					type: "error"
	 				});
				}
			}
		});
    }
    </script>
  </body>
</html>