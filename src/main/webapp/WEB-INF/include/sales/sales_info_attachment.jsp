<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!-- 附件信息 -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>附件信息</h3>
              </div>
            </div>
            
            <!-- 基本信息 -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>附件列表 <small style="color:red"></small>
               		 <small style="color: red">
						<button type="button" class="btn btn-success"  data-toggle="modal" data-target=".bs-modal-report">上传附件</button>
					</small>
                    </h2>
                    <div id="files">
					</div>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  				<div class="modal fade bs-modal-report" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close" id="report_close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
				                        
									<div class="modal-body">
 										<div class="dropzone" id="myDropzone">
											<div class="am-text-success dz-message">
												将文件拖拽到此处,或点此打开文件管理器选择文件
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">取消</button>
										<button type="button" id="report_save" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
				<div class="clearfix"></div>
				<div class="x_content">
	                    <table id="attachement-datatable-buttons" class="table table-striped table-bordered">
		 					<thead>
		                        <tr>
		                          <th>文件名</th>
		                          <th>创建时间</th>
		                          <th>删除操作</th>
		                        </tr>
		                   </thead>
	                    </table>
				</div>
			</div>
                </div>
              </div>
        	</div>
        </div><!-- 附件信息 -->