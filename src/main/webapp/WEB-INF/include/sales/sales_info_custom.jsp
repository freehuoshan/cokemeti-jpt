<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <c:if test="${type==0}">
                <h3>添加客户</h3>
              </c:if>
              <c:if test="${type==1}">
                <h3>修改信息</h3>
              </c:if>
              </div>
            </div>
            
            <!-- 基本信息 -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>基本信息 <small style="color:red"></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <br />
                  <form id="form" class="form-horizontal form-label-left input_mask">

                  	<input type="hidden" value="${id}" id="id">
                  	<!-- 第一行 -->
                  	<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">客户姓名：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control "  placeholder="客户姓名" id="name" value="${vo.username}">
                      </div>
					  
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">电话：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control "  placeholder="电话" id="phone" name="phone" value="${vo.phone}">
                      </div>
                      
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">电子邮件：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control"  placeholder="电子邮件" id="email" name="email" value="${vo.creditNo}">
                      </div>
					</div>
					
					<!-- 第二行 -->
					<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">公司名称：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control " placeholder="公司名称" id="company"name="company" value="${vo.name}">
                      </div>
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">行业：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control" placeholder="行业" id="industry"name="industry" value="${vo.industry}">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">公司网址：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control" placeholder="公司网址" id="website"name="website" value="${vo.address}">
                      </div>
                      <c:if test="${type==1}">
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">创建者：</label>
                      <div class="col-md-1 col-sm-3 col-xs-8  ">
                        <input type="text" disabled="true" class="form-control" placeholder="创建者" id="website"name="createUser" value="${create.username}">
                      </div>
                      </c:if>
					</div>
					
					<!-- 第二行 -->
					<div class="form-group">
					  <label class=" col-md-1 col-sm-2 col-xs-12 properties_name">公司简介：</label>
                      <div class="col-md-10 col-sm-10 col-xs-12  ">
                       <textarea class="form-control" rows="9" placeholder="公司简介" cols="9"id="description" name="description" >${vo.orgNo}</textarea>
                      </div>
					</div>
					
					<div class="form-group">
                       <c:if test="${type==1}">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">账户余额：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control " placeholder="账户余额" id="prepay"name="prepay" value="${vo.amount}">
					</div>
                       </c:if>
                       <c:if test="${type==0}">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">预存金额：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control " placeholder="预存金额" id="prepay"name="prepay" value="${vo.amount}">
					</div>
                       </c:if>
                      
                    <!-- 第三行 -->  
                    <div class="form-group">
                        <div class="col-md-8 col-sm-6 col-xs-8 " style="text-align : right;">
                         <c:if test="${type==1}">
                          <button id="delete_button" type="button" class="btn btn-primary ">删除</button>
                         </c:if>
                          <button id="submit_buttion" type="button" class="btn btn-primary ">修改并保存</button>
                         <!--  <button type="submit" class="btn btn-success">Submit</button> -->
                        </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
        	</div>
        </div>