<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-stethoscope"></i> 客户 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<%=basePath%>/sales/customer/index">客户列表</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-user"></i> 专家库 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<%=basePath%>/analyst/professional/search">搜索</a></li>
					  <li><a href="<%=basePath%>/analyst/professional/file">上传专家库</a></li>
					  <li><a href="<%=basePath%>/analyst/professional/tosingleadd">添加新专家</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> 项目 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<%=basePath%>/analyst/project/list">项目</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> 统计 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    </ul>
                  </li>
                  
                   <c:if test="${sessionScope.role == 1}">
                  <li><a><i class="fa fa-desktop"></i> 用户管理 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<%=basePath%>/user/to_list">用户列表</a></li>
                    </ul>
                  </li>
                  </c:if>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->