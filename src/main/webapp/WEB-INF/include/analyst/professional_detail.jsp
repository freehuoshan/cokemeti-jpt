<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
    <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/sweetalert/sweetalert.css" rel="stylesheet">
    
    <style type="text/css">
    	
    	.properties_name{
    		    margin-top: 10px;
    			text-align: right;
    	}
    	
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            
            
            <div class="page-title">
              <div class="title_left">
                <h3>专家详情</h3>
              </div>
            </div>
            
            <!-- 基本信息 -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>基本信息 <small style="color:red">${msg}</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <br />
                  <form class="form-horizontal form-label-left input_mask">
                  	<input type="hidden" id="id" value="${professional.id}"></input>
                  	<!-- 第一行 -->
                  	<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">姓名：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control "  placeholder="姓名" value="${professional.name}" id="name">
                      </div>
					  <label class=" col-md-1 col-sm-6 col-xs-12"></label>
					  
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">联系方式：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control"  placeholder="联系方式" value="${professional.phone}" id="phone">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12"></label>
                      
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">付款方式：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control"  placeholder="付款方式" value="${professional.rewardType}" id="rewardType">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12"></label>
					</div>
					
					<!-- 第二行 -->
					<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">邮箱：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control " placeholder="邮箱" value="${professional.email}" id="email">
                      </div>
					  <label class=" col-md-1 col-sm-6 col-xs-12"></label>
					  
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">默认价格：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control" placeholder="默认价格" value="${professional.price}" id="price">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12"></label>
                      
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">付款账号：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control" placeholder="付款账号" value="${professional.rewardAccount}" id="rewardAccount">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12"></label>
					</div>
 
 					<!-- 第三行 -->
					<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">目前公司：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control " placeholder="公司" value="${professional.company}" id="company">
                      </div>
					  <label class=" col-md-1 col-sm-6 col-xs-12"></label>
					  
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">职位：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control" placeholder="职位" value="${professional.title}" id="title">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12"></label>
                      
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">ID：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input disabled="disabled" type="text"  class="form-control" placeholder="付款账号" value="${professional.id}" id="">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12"></label>
					</div>                     

					<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">工作经历：</label>
                      <div class="col-md-10 col-sm-10 col-xs-12  ">
                       		<textarea oncontextmenu="window.event.returnValue=false" oncopy="return false;" oncut="return false;" 
                       		  onpaste="return false"
                       		  class="form-control"rows="9" cols="9" id="workExp">${professional.workExp}</textarea>
                      </div>
					</div>  
                      
                    <!-- 第三行 -->  
                    <div class="form-group">
                        <div class="col-md-12 col-sm-9 col-xs-12 " style="text-align : right;">
                          <button id="submit_buttion" type="button" class="btn btn-primary ">修改并保存</button>
                         <!--  <button type="submit" class="btn btn-success">Submit</button> -->
                        </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              
           <!-- 工作经历 -->
           	<div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>工作经历 
	                    <small style="color: red">
							<button type="button" class="btn btn-success" id="work_exp_modal"  data-toggle="modal">新增工作经历</button>
						</small>
					</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <br />
                   <table id="datatable-buttons" class="table table-striped table-bordered">
                   <thead>
                        <tr>
                          <th>开始时间</th>
                          <th>结束时间</th>
                          <th>公司</th>
                          <th>职位</th>
                          <th>编辑操作</th>
                          <th>删除操作</th>
                        </tr>
                      </thead>
                  </table>
                  </div>
        		</div>
        	</div>
        	
        	<div class="modal fade bs-modal-workexp" id="workexp_add" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close" id="workexp_close">
											<span aria-hidden="true">×</span>
										</button>
									<h4 class="modal-title" id="add_workexp_title"> 新增工作经历 </h4>
									</div>
									<div class="modal-body">
										<fieldset>
										 	<input id="profession_work_id" type="hidden" />
										 	<input id="profession_id" type="hidden" />
										    <div class="control-group">
										          <!-- Text input-->
										          <label class="control-label" for="input01">开始时间</label>
										          <div class="controls">
										            <input id="datetimepicker1" type="text" placeholder="开始时间" class="workexp_begin_time input-xlarge form-control datetime">
										          </div>
										        </div>
										    <div class="control-group">
										          <!-- Text input-->
										          <label class="control-label" for="input01">结束时间</label>
										          <div class="controls">
										            <input id="datetimepicker2" type="text" placeholder="结束时间" class="workexp_end_time input-xlarge form-control datetime">
										          </div>
										    </div> 
										    <div class="control-group">
										          <!-- Text input-->
										          <label class="control-label" for="input01">公司名称</label>
										          <div class="controls">
										            <input type="text" placeholder="公司名称" class="workexp_company input-xlarge form-control">
										          </div>
										    </div>
										    <div class="control-group">
										          <!-- Text input-->
										          <label class="control-label" for="input01">职位</label>
										          <div class="controls">
										            <input type="text" placeholder="职位" class="workexp_title input-xlarge form-control">
										          </div>
										    </div>
										    <div class="control-group">
										       <!-- Textarea -->
										       <label class="control-label">工作内容</label>
										       <div class="controls">
										         <div class="textarea">
										               <textarea placeholder="工作内容" type="" class="workexp_content form-control"> </textarea>
										         </div>
										       </div>
										     </div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">取消</button>
										<button type="button" id="workexp_save" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
        
        
            <!-- 项目相关经历 -->
           	<div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>项目相关经历 <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <br />
                  <table id="datatable-pp" class="table table-striped table-bordered">
                  	<thead>
                        <tr>
                          <th>项目ID</th>
                          <th>公司</th>
                          <th>职位</th>
                          <th>相关经历</th>
                        </tr>
                      </thead>
                  </table>
                  </div>
        		</div>
        	</div>
        	
        	
        	<!-- 备注 -->
           	<div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>备注 <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <br />
					
                  </div>
        		</div>
        	</div>       
        
        
        	</div>
        </div>
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    
    
    <!-- Datatables -->
    <script src="<%=basePath%>/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

    <script src="<%=basePath%>/static/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<%=basePath%>/static/js/sweetalert/sweetalert.min.js"></script>
    <script src="<%=basePath%>/static/js/bootstrap-datetimepicker.js"></script>
    <script>
    $(document).ready(function() {
    	    
          if($("#datatable-buttons").length) {
        	var professionalId = $('#id').val();
        	if(professionalId.length > 0){
	            $("#datatable-buttons").DataTable({
	            	"processing": true,
		            "serverSide": false,
		            "ajax":{
		              	url:'<%=basePath%>/analyst/professional/workexps',
		              	type:"POST",
		              	"data":{
		              		"professionalId":professionalId
		              	}
		              },
	             	 "columns": [
	                          { "data": "startDate" },
	                          { "data": "endDate" },
	                          { "data": "companyName" },
	                          { "data": "title" },
	                          {	"data": "id"},
	                          {	"data": "id"}
	                      ],
		              "columnDefs" : [{
		            	  targets:4,
                       	  render: function (data, type, row, meta) {
                       		 return '<button type="button" onclick="updateWorkExpById('+data+')" class="btn btn-success btn-xs">编辑</button>';
                       	  }
		              },{
		            	  targets:5,
                       	  render: function (data, type, row, meta) {
                       		 return '<button type="button" onclick="deleteWorkExpById('+data+',&quot;'+row.content+'&quot;)" class="btn btn-success btn-xs">删除</button>';
                       	  }
		              }
		              ],
	              	"rowId":"id",
	              	"paging":false,
	              	"searching":false,
	              	"info":false,
	          	  	"ordering":false
	            });
        	}
          }
          
          if($("#datatable-pp").length) {
          	var professionalId = $('#id').val();
          	if(professionalId.length > 0){
	             var projectTab = $("#datatable-pp").DataTable({
	              	"processing": true,
	  	            "serverSide": false,
	  	            "ajax":{
	  	              	url:'<%=basePath%>/analyst/professional/projects',
	  	              	type:"POST",
	  	              	"data":{
	  	              		"professionalId":professionalId
	  	              	}
	  	              },
	               	 "columns": [
	                            { "data": "projectId" },
	                            { "data": "company" },
	                            { "data": "title" },
	                            { "data": "exp" }
	                        ],
	                	"rowId":"id",
	                	"paging":false,
	                	"searching":false,
	                	"info":false,
	            	  	"ordering":false
	              });
	              $('#datatable-pp tbody').on( 'dblclick', 'tr', function () {
	              	var data = projectTab.row( this ).data();
	          		location.href = '<%=basePath%>/analyst/project/'+data.projectId;
	          	 });
          	}
            }
    });
    
    var dateRegularExpression = /^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/;  
    
    $('#datetimepicker1').datetimepicker({
    	todayBtn : "linked",  
        language : 'zh-CN',  
        autoclose : true,  
        startView: 3,  
        minView: 3,  
        format: 'yyyy-mm',  
        startView: 3
    }).on('changeDate',function(e){  
        var time = e.date;  
        $('#datetimepicker2').datetimepicker('setStartDate',time);  
    }).on('hide',function(e){  
        var time = e.date;  
        if( time.toLocaleDateString() == '1899-12-31' || !dateRegularExpression.test(time.toLocaleDateString()) ){  
            $('#datetimepicker1').val("");  
        }  
    });  
    
  //结束时间：  
    $('#datetimepicker2').datetimepicker({  
        todayBtn : "linked",  
        language : 'zh-CN',  
        autoclose : true,  
        startView: 3,  
        minView: 3,  
        format: 'yyyy-mm',  
        startView: 3
    }).on('changeDate',function(e){  
        var time = e.date;  
        $('#datetimepicker1').datetimepicker('setEndDate',time);  
    }).on('hide',function(e){  
        var time = e.date;  
        //乱输入 ime.toLocaleDateString()得到总是'1899-12-31'，选择日期总是得到年月日，使用正则表达式验证手动输入  
        if( time.toLocaleDateString() == '1899-12-31'|| !dateRegularExpression.test(time.toLocaleDateString()) ){  
            $('#datetimepicker2').val("");  
        }  
    })  
    
    $('#submit_buttion').click(function(){

    	var id = $("#id").val();
    	var name = $("#name").val();
    	var phone = $("#phone").val();
    	var email = $("#email").val();
    	var rewardType = $("#rewardType").val();
    	var rewardAccount = $("#rewardAccount").val();
    	var price = $("#price").val();
    	var company = $("#company").val();
    	var title = $("#title").val();
    	var workExp = $("#workExp").val();
    	
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/professional/isexist",
			data:{'id':id,'phone':phone,'email':email},
			dataType:'text',
			success:function(res){
				var res = $.parseJSON(res);
				if(res.isExist){
					
					swal({
				        title: "已存在该专家",
				        text: "<a href='<%=basePath%>/analyst/professional/detail/" + res.id +"'</>已存在专家</a>",
				        type: "warning",
				        html: true,
				        showCancelButton: true,
				        confirmButtonColor: "#DD6B55",
				        confirmButtonText: "忽略",
				        cancelButtonText: "取消",
				        closeOnConfirm: false
				    }, function(){
				    	if(id.length > 0){
					    	$.ajax({
								type:"post",
								url:"<%=basePath%>/analyst/professional/info/save",
								data:{'id':id,'name':name,'phone':phone,'email':email,
									'rewardType':rewardType,'rewardAccount':rewardAccount,
									'price':price,'company':company,'title':title,'workExp':workExp},
								asyn:true,
								dataType:'text',
								success:function(d){
										swal("","基本信息修改成功","success");
						                location.href='<%=basePath%>/analyst/professional/detail/'+id;
								}
							});
				    	}
				    	else{
				    		$.ajax({
								type:"post",
								url:"<%=basePath%>/analyst/professional/singleadd",
								data:{'name':name,'phone':phone,'email':email,
									'rewardType':rewardType,'rewardAccount':rewardAccount,
									'price':price,'company':company,'title':title,'workExp':workExp},
								dataType:'text',
								success:function(d){
										swal("","基本信息增加成功","success");
						                location.href='<%=basePath%>/analyst/professional/detail/'+d;
								}
							});
				    	}
				    });
				}else{
					if(id.length > 0){
				    	$.ajax({
							type:"post",
							url:"<%=basePath%>/analyst/professional/info/save",
							data:{'id':id,'name':name,'phone':phone,'email':email,
								'rewardType':rewardType,'rewardAccount':rewardAccount,
								'price':price,'company':company,'title':title,'workExp':workExp},
							asyn:true,
							dataType:'text',
							success:function(d){
									swal("","基本信息修改成功","success");
					                location.href='<%=basePath%>/analyst/professional/detail/'+id;
							}
						});
			    	}
			    	else{
			    		$.ajax({
							type:"post",
							url:"<%=basePath%>/analyst/professional/singleadd",
							data:{'name':name,'phone':phone,'email':email,
								'rewardType':rewardType,'rewardAccount':rewardAccount,
								'price':price,'company':company,'title':title,'workExp':workExp},
							dataType:'text',
							success:function(d){
									swal("","基本信息增加成功","success");
					                location.href='<%=basePath%>/analyst/professional/detail/'+d;
							}
						});
			    	}
				}
			}
		});
    	
    });
    
    $("#work_exp_modal").click(function(){
    	if($("#id").val() == ''){
    		swal("","请先添加专家基本信息","warning");
    		return false;
    	}
    	$("#add_workexp_title").html("新增工作经历");
    	$("#profession_work_id").val("");
    	$("#profession_id").val($("#id").val());
    	$(".workexp_begin_time").val("");
    	$(".workexp_end_time").val("");
    	$(".workexp_title").val("");
    	$(".workexp_company").val("");
    	$(".workexp_content").val("");
    	$(".bs-modal-workexp").modal('show');
    });
    
    $("#workexp_save").click(function(){
    	var param = {};
    	
    	param.companyName = $(".workexp_company").val();
    	param.startDate = $(".workexp_begin_time").val();
    	param.endDate = $(".workexp_end_time").val();
    	param.title = $(".workexp_title").val();
    	param.content = $(".workexp_content").val();
    	param.professionalId = $("#profession_id").val();
    	var pro_work_id = $("#profession_work_id").val();
    	if(pro_work_id != ''){
    		param.id = $("#profession_work_id").val();
    	}
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/professional/addWorkExp",
			data: param,
			dataType: "text",
			success:function(res){
				if(res==1){
					swal("","保存成功","success");
					$(".bs-modal-workexp").modal('hide');
	                location.href='<%=basePath%>/analyst/professional/detail/'+param.professionalId;
				}else{
					swal("","发生错误,请重试","error");
				}
			}
		});
    	
    	
    });
    
    function updateWorkExpById(professionWorkID){
    	$.ajax({
			type:"get",
			url:"<%=basePath%>/analyst/professional/workexp/" + professionWorkID,
			dataType: "text",
			success:function(res){
				var pressWork = $.parseJSON(res);
				$("#add_workexp_title").html("编辑工作经历");
		    	$("#profession_work_id").val(pressWork.id);
		    	$("#profession_id").val(pressWork.professionalId);
		    	$(".workexp_begin_time").val(pressWork.startDate);
		    	$(".workexp_end_time").val(pressWork.endDate);
		    	$(".workexp_title").val(pressWork.title);
		    	$(".workexp_company").val(pressWork.companyName);
		    	$(".workexp_content").val(pressWork.content);
		    	$(".bs-modal-workexp").modal('show');
			}
		});
    }
    
    function deleteWorkExpById(professionWorkID){
    	swal({
	        title: "您确定要删除这条信息吗",
	        text: "删除后将无法恢复，请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "删除",
	        cancelButtonText: "取消",
	        closeOnConfirm: false
	    }, function(){
	    	$.ajax({
				type:"post",
				url:"<%=basePath%>/analyst/professional/workexp/del",
				data:{"id" : professionWorkID},
				dataType:'text',
				success:function(d){
					if(d == 1){
						swal({
		 					title: "",
		 					text: "删除成功",
		 					type: "success"
		 				});
						setTimeout(function(){
							location.href='<%=basePath%>/analyst/professional/detail/' + $("#id").val();
						},1000);
						
					}
					else{
						swal({
		 					title: "",
		 					text: "出错,请重试",
		 					type: "error"
		 				});
					}
				}
			});
	    });
    }

    	/*  */
    
    
    </script>
  </body>
</html>