<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					需求信息 
					<small style="color: red">
						<button type="button" class="btn btn-success"  data-toggle="modal" data-target=".bs-modal-requirement">新增需求</button>
					</small>
				</h2>
				<div class="modal fade bs-modal-requirement" id="require_addpage" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close" id="requirement_close">
											<span aria-hidden="true">×</span>
										</button>
									<h4 class="modal-title" id="myModalLabel2"> 新增需求 </h4>
									</div>
									<div class="modal-body">
										<textarea class="resizable_textarea form-control" rows="6" placeholder="需求描述" cols="6" id="requirement_info" name="info" value=""></textarea>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">取消</button>
										<button type="button" id="requirement_save" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
					
					<div class="modal fade" id="require_updatepage" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close" id="requirement_close">
											<span aria-hidden="true">×</span>
										</button>
									<h4 class="modal-title" id="updateRequireTitle"> 修改需求 </h4>
									</div>
									<div class="modal-body">
										<input id="require_update_id" type="hidden" />
										<textarea class="resizable_textarea form-control" rows="6" placeholder="需求描述" cols="6" id="update_requirement_info" name="info" value=""></textarea>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">取消</button>
										<button type="button" id="requirement_update" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<table id="datatable-requirement" class="table table-striped table-bordered">
                   <thead>
                        <tr>
                          <th>时间</th>
                          <th>创建者</th>
                          <th>需求描述</th>
                          <th>删除操作</th>
                          <th>编辑操作</th>
                        </tr>
                   </thead>
                </table>
			</div>
		</div>
	</div>
</div>