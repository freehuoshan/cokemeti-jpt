<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
 <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    	
    	.properties_name{
    		    margin-top: 10px;
    			text-align: right;
    	}
    	
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
<!-- 附件信息 -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>文件导入</h3>
              </div>
            </div>
            
            <!-- 基本信息 -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>
               		 <small style="color: red">
						<button type="button" class="btn btn-success"  data-toggle="modal" data-target=".bs-modal-report">上传文件</button>
					</small>
                    </h2>
                    <div id="files">
					</div>
                   <!--  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  				<div class="modal fade bs-modal-report" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close" id="report_close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
				                        
									<div class="modal-body">
 										<div class="dropzone" id="myDropzone">
											<div class="am-text-success dz-message">
												将文件拖拽到此处,或点此打开文件管理器选择文件
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">取消</button>
										<button type="button" id="report_save" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
				<div class="clearfix"></div>
			</div>
                </div>
              </div>
        	</div>
        </div><!-- 附件信息 -->
       	</div>
       </div>
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Dropzone.js -->
    <script src="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    <script src="<%=basePath%>/static/js/sweetalert/sweetalert.min.js"></script>

    <script>
    Dropzone.autoDiscover = false;
    var dropz = new Dropzone("#myDropzone", {
  	  url: "<%=basePath%>/file/upload",
        addRemoveLinks: true,
        dictRemoveLinks: "x",
        dictCancelUpload: "x",
        maxFiles: 10,
        maxFilesize: 50,
		  init: function() {
            this.on("success", function(file, response) {
          	 if(response != "0"){
          		 var string = response;
          		 var arr = string.split('.');
          		 var id = arr[0];
          		 var data = response;
          		 var html = '<input type="hidden" id='+id+' value="'+data+ '" data-filename ="' +file.name+'"/>';
          		 $("#files").append(html);
          	 }
            });
            this.on("removedfile", function(file) {
       		 var string = file.name;
      		 var arr = string.split('.');
      		 var id = arr[0];
               $('#'+id).remove();
            });
        }
    });
  
    $('#report_save').click(function(){
		var files='';
		var filenames='';
    	$("#files input[type='hidden']").each(function(){
    	   files = $(this).val()
    	   filenames = $(this).data("filename");
    	});  
		if(files != ''){
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/analyst/professional/file",
    			data:{"reports":files,"filenames":filenames},
    			dataType:'json',
    			async:false,
    			success:function(d){
    				$("#report_close").click();
    				dropz.removeAllFiles();
    				if(!d){
    					swal({
    						title: "",
    						text: "上传失败",
    						type:"error"
    					});
    					return;
    				}
    				else if(d.repeat && d.fail){
    					swal({
    						title: "部分上传成功",
    						text:"<a href='" + d.fail + "'/>下载失败专家</a>&nbsp;&nbsp;&nbsp;" + "<a href='" + d.repeat + "'/>下载重复专家</a>",
    						html:true,
    					});
    					return;
    				}
    				else if(d.repeat){
    					swal({
    						title: "部分上传成功",
    						text:"<a href='" + d.repeat + "'/>下载重复专家</a>",
    						html:true,
    					});
    					return;
    				}
    				else if(d.fail){
    					swal({
    						title: "部分上传成功",
    						text:"<a href='" + d.fail + "'/>下载失败专家</a>",
    						html:true,
    					});
    					return;
    				}
    				else{
    					swal({
    						title: "",
    						text: "全部上传成功",
    						type:"success"
    					});
    				}
    				
    			}
    		});
		}else{
			alert("请上传附件");
		}
    });
    </script>
  </body>
</html>