<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
    <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/sweetalert/sweetalert.css" rel="stylesheet">
    

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        	
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>项目列表</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li ><a style="color:#5A738E;" class="" href="<%=basePath%>/user/to_add"><i class="fa fa-plus"></i> 新建用户</a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable-users" class="table table-hover table-striped table-bordered ">
                      <thead>
                        <tr>
                          <th>用户名</th>
                          <th>邮箱</th>
                          <th>电话号</th>
                          <th>密码</th>
                          <th>删除操作</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
        </div>
        
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    
    
    <!-- Datatables -->
    <script src="<%=basePath%>/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

    <script src="<%=basePath%>/static/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<%=basePath%>/static/js/sweetalert/sweetalert.min.js"></script>
    <script>
    var atttable;
    $(document).ready(function() {
    	getUserList();
    });
    
    function getUserList(){
    	var url = '<%=request.getSession().getAttribute("url")%>';
    	if ($("#datatable-users").length) {
    		if(typeof(atttable)=='undefined'){
    			atttable = $("#datatable-users").DataTable({
    				  "responsive": true,
    	              "processing": true,
    	              "serverSide": true,
    	              "stateSave": true,
    	              "ajax":{
    	              	url:'<%=basePath%>/user/list',
    	              	type:'POST',
    	              },
    	              "columns": [
    	                          { "data": "username" },
    	                          { "data": "email" },
    	                          { "data": "phone" },
    	                          { "data": "password" },
    	                          {"data" : "id"}
    	                      ],
    	               "columnDefs":[
	    	                   	 {
	    	                        	targets: 4,
	    	                        	render: function (data, type, row, meta) {
	    	                        		return '<button type="button" onclick="deleteUser('+data+')" class="btn btn-success btn-xs">删除</button>';
	    	                        	}
	    	                   	  	}
                   			],  
                   "rowId":"id",
 	              "searching":true,
 	              "searchDelay": -1,
 	              "info":false,
 	          	  "ordering":false
	            });
    			 $('#datatable-users tbody').on( 'dblclick', 'tr', function () {
    	            	var data = atttable.row( this ).data();
    	        		location.href = '<%=basePath%>/user/edit/'+data.id;
    	       	 	});
    		}else{
    			atttable.ajax.reload();
    		}
       }
    	
    }
    
    function deleteUser(id){
    	
    	swal({
	        title: "您确定要删除这条信息吗",
	        text: "删除后将无法恢复，请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "删除",
	        cancelButtonText: "取消",
	        closeOnConfirm: false
	    }, function(){
	    	$.ajax({
				type:"post",
				url:"<%=basePath%>/user/delete",
				data:{"id" : id},
				dataType:'text',
				success:function(d){
					if(d == "成功"){
						swal({
		 					title: "",
		 					text: d,
		 					type: "success"
		 				});
						setTimeout(function(){
							location.href='<%=basePath%>/user/to_list';
						},1000);
						
					}
					else{
						swal({
		 					title: "",
		 					text: d,
		 					type: "error"
		 				});
					}
				}
			});
	    });
    	
    }
    </script>
  </body>
</html>