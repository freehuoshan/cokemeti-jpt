<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
    <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
	<link href="<%=basePath%>/static/css/sweetalert/sweetalert.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->
         <!-- page content -->
        <div class="right_col" role="main">
        	
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="page-title">
		              <div class="title_left">
		              <c:if test="${type==0}">
		                <h3>添加用户</h3>
		              </c:if>
		              <c:if test="${type==1}">
		                <h3>修改信息</h3>
		              </c:if>
		              </div>
		            </div>
                  </div>
                  <div class="x_content text-center"　>
                   	<br />
                  <form id="user_form" class="form-horizontal form-label-left input_mask text-center" method = "post">
					<c:if test="${type==1}">
                  	<input type="hidden" value="${vo.id}" name="id" id="id">
                  	</c:if>
                  	<input type="hidden" value="${type}" id="type">
                  	<!-- 第一行 -->
                  	<div class="form-group">
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">用户姓名：</label>
                      <div class="col-md-2 col-sm-12 col-xs-12  ">
                       <input type="text" class="form-control "  placeholder="用户姓名" id="username" name="username" value="${vo.username}">
                      </div>
					  <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">电话：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                       <input type="text" class="form-control "  placeholder="电话" id="phone" name="phone" value="${vo.phone}">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">电子邮件：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <input type="text" class="form-control"  placeholder="电子邮件" id="email" name="email" value="${vo.email}">
                      </div>
                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">角色：</label>
                      <div class="col-md-2 col-sm-6 col-xs-12  ">
                        <select id="role" name = "role">
	                        <c:choose>
	                        	<c:when test="${vo.role == 1}">
	                        		<option value="1" selected = "selected">管理员</option>
	                        	</c:when>
	                        	<c:otherwise>
	                        		<option value="1">管理员</option>
	                        	</c:otherwise>
	                        </c:choose>
	                        <c:choose>
	                        <c:when test="${vo.role == 2}">
	                        		<option value="2" selected = "selected">销售员</option>
	                        	</c:when>
	                        	<c:otherwise>
	                        		<option value="2">销售员</option>
	                        	</c:otherwise>
	                        </c:choose>
	                        <c:choose>
	                        	<c:when test="${vo.role == 3}">
	                        		<option value="3" selected = "selected">分析员</option>
	                        	</c:when>
	                        	<c:otherwise>
	                        		<option value="3">分析员</option>
	                        	</c:otherwise>
	                        </c:choose>
                        </select>
                      </div>
					</div>
					<div class="form-group">
	                      <label class=" col-md-1 col-sm-6 col-xs-12 properties_name">密码：</label>
	                      <div class="col-md-2 col-sm-6 col-xs-12  ">
	                        <input type="text" class="form-control"  placeholder="密码" id="password" name="password" value="${vo.password}">
	                      </div>
	                </div>
				  	<div class="form-group"></div>
				  	<div class="form-group"></div>
                    <!-- 第三行 -->  
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align : center;">
                        <c:if test="${type==1}">
                          <button id="submit_buttion" type="button" class="btn btn-primary ">修改</button>
                        </c:if>
                        <c:if test="${type==0}">
                          <button id="submit_buttion" type="button" class="btn btn-primary ">添加</button>
                        </c:if>
                        </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
        
        </div>
        
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    
    
    <!-- Datatables -->
    <script src="<%=basePath%>/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

    <script src="<%=basePath%>/static/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/vfs_fonts.js"></script>
     <!-- jQuery Validation plugin javascript-->
    <script src="<%=basePath%>/static/js/jquery.form.js"></script>
    <script src="<%=basePath%>/static/js/validate/jquery.validate.min.js"></script>
    <script src="<%=basePath%>/static/js/validate/messages_zh.min.js"></script>
    <script src="<%=basePath%>/static/js/sweetalert/sweetalert.min.js"></script>
    
    <script>
    $(document).ready(function() {
    	validate();
    	var url = "";
    	var type = $("#type").val();
    	if(type == 0){
    		url = "<%=basePath%>/user/add"
    	}
    	else{
    		url = "<%=basePath%>/user/update"
    	}
    	$("#user_form").attr("action" , url);
    	$("#submit_buttion").on("click",function(){
    	 	   $("#user_form").submit();
    	 });
    	
    	//新增验证
        function validate(){
        	// validate signup form on keyup and submit
        	var icon = "<i class='fa fa-times-circle'></i> ";
        	validator = $("#user_form").validate({
        			    rules: {
        			    	username:{
        			    	 	required:true,
        			    	},
        			    	email:{
        			    	 	required:true,
        						checkEmailRepeat: true
        			    	},
        			    	password:{
        			    		required:true
        			    	}
        			    },
        			    messages: {
        			    	username:{
        			    		required: icon+"为必填值不可为空"
        			    	},
        			    	email:{
        			    		required: icon+"为必填值不可为空"
        			    	},
        			    	password:{
        			    		required: icon+"为必填值不可为空"
        			    	}
        			    },
        			    submitHandler: function(form){
        			    	$(form).ajaxSubmit({
        			    		type: "POST",
        			    		dataType: "json",
        			    		success: function(data){
        			    			if(type == 0){
        		    					if(data){
        		    						swal({
        		    							title: "",
        		    							text: "添加成功",
        		    							type: "success"
        		    						});
        		    						setTimeout(function(){
        		    							location.href='<%=basePath%>/user/to_list';
        		    						},1000);
        		    					}
        		    					else{
        		    						swal({
        		    							title: "",
        		    							text: "添加失败",
        		    							type: "error"
        		    						});
        		    					}
        		    				}
        		    				else{
        		    					if(data){
        		    						swal({
        		    							title: "",
        		    							text: "修改成功",
        		    							type: "success"
        		    						});
        		    						setTimeout(function(){
        		    							location.href='<%=basePath%>/user/to_list';
        		    						},1000);
        		    					}
        		    					else{
        		    						swal({
        		    							title: "",
        		    							text: "修改失败",
        		    							type: "error"
        		    						});
        		    					}
        		    				}
        			    		}
        			    	});
        			    }
        			});

        	$.validator.addMethod("checkEmailRepeat",function(value,element,params){  
        		var param = {};
        		param.email = value;
        		if($("#id").length >0){
        			param.id = $("#id").val();
        		}
        		var result;
        		$.ajax({  
        	       url: "<%=basePath%>/user/checkRepeat",  
        	       dataType: "json", 
        	       data: param,
        	       type: "POST",
        	       async:false,
        	       success: function (data) {  
        	    	   if(typeof data == 'boolean'){
        	    		  result=data;
        	    	   }
        	    	   else{
        	    		   swal({
        	 					title: "",
        	 					text: "发生错误",
        	 					type: "error"
        	 				});
        	    		   return;
        	    	   }
        	       },  
        	       error: function (XMLHttpRequest, textStatus, errorThrown) {  
        	    	   swal({
        					title: "",
        					text: "发生错误",
        					type: "error"
        				});
        	       }  
        	   });  
        		return this.optional(element)||!result;  
            },icon+"已存在该用户");  
        }
    	
    });
    </script>
  </body>
</html>

