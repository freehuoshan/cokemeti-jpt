<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					基本信息 <small style="color: red"></small>
				</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="form" class="form-horizontal form-label-left input_mask">
					<!-- 第一行 -->
					<div class="form-group">
						<label class=" col-md-5 col-sm-6 col-xs-12 properties_name"></label>
						<label class=" col-md-2 col-sm-6 col-xs-12 ">
							<button type="button" class="btn btn-success" data-toggle="modal"
								data-target=".lg-company">选择公司</button>
						</label>

						<div class="modal fade bs-example-modal-lg lg-company" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">×</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">Modal title</h4>
									</div>
									<div class="modal-body">
										<table id="datatable-customers"
											class="table table-striped table-bordered" style="width: 100%;">
											<thead>
												<tr>
													<th>客户名称</th>
													<th>公司名称</th>
													<th>状态</th>
													<th>等级</th>
													<th>账户余额</th>
												</tr>
											</thead>
										</table>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal" id="info_close">关闭</button>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- 第一行 -->
					<div class="form-group">
						<label class=" col-md-1 col-sm-6 col-xs-12 properties_name">项目名称：</label>
						<div class="col-md-3 col-sm-6 col-xs-12  ">
							<input type="text" class="form-control " placeholder="项目名称"
								id="project_name" name="project_name" value="${p.name}">
						</div>

						<label class=" col-md-1 col-sm-6 col-xs-12 properties_name">公司名称：</label>
						<input type="hidden" name="customer_id" id="customer_id" value="${c.id}"/>
						<div class="col-md-3 col-sm-6 col-xs-12  ">
							<input type="text" class="form-control " placeholder="公司名称"
								id="company_name" name="company_name" disabled="disabled" value="${c.name}">
						</div>
					</div>

					<!-- 第二行 -->
					<div class="form-group">
						<label class=" col-md-1 col-sm-6 col-xs-12 properties_name">开始时间：</label>
						<div class="col-md-3 col-sm-6 col-xs-12  ">
								<input type="text" value="${p.startDate}" class="form-control has-feedback-left" id="single_cal_start" placeholder="开始时间" aria-describedby="inputSuccess2Status4">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status4" class="sr-only">(success)</span>
						</div>

						<label class=" col-md-1 col-sm-6 col-xs-12 properties_name">客户姓名：</label>
						<div class="col-md-3 col-sm-6 col-xs-12  ">
							<input type="text" class="form-control" placeholder="客户名称"
								id="customer_name" name="customer_name" disabled="disabled" value="${c.username}">
						</div>
					</div>

					<!-- 第三行 -->
					<div class="form-group">
						<label class=" col-md-1 col-sm-6 col-xs-12 properties_name">结束时间：</label>
						<div class="col-md-3 col-sm-6 col-xs-12  ">
								<input type="text"  value="${p.endDate}" class="form-control has-feedback-left" id="single_cal_end" placeholder="结束时间" aria-describedby="inputSuccess2Status5">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status5" class="sr-only">(success)</span>
						</div>
						<label class=" col-md-1 col-sm-6 col-xs-12 properties_name">账户余额：</label>
						<div class="col-md-3 col-sm-6 col-xs-12  ">
							<input type="text" class="form-control" placeholder="账户余额"
								id="amount" name="amount" disabled="disabled" value="${c.amount}">
						</div>
					</div>

					<!-- 第三行 -->
					<div class="form-group">
						<c:if test="${type==1}">
							<label class=" col-md-1 col-sm-6 col-xs-12 properties_name">创建者：</label>
							<div class="col-md-3 col-sm-6 col-xs-12  ">
								<input type="text" class="form-control" placeholder="创建者"
									id="createUser" name="createUser" disabled="disabled" value="${createUser.username}">
							</div>
						</c:if>
						<div class="col-md-4 col-sm-6 col-xs-8 "
							style="text-align: right;">
							 <c:if test="${type==1}">
							<button id="delete_button" type="button" class="btn btn-primary ">
								删除
							</button>
							</c:if>
							<button id="info_submit" type="button" class="btn btn-primary ">
								保存
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>