<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Cokemeti</title>
  <meta charset="utf-8">
  <style type="text/css">
  *{margin:0;padding:0}
  body{ background: #e3e3e3;font-size: 14px; color: #333;font-family:"微软雅黑","宋体",Arial,Helvetica,Verdana,sans-serif;color:#3c3c3c;line-height:1}
  .header{height:93px;line-height:93px;border-bottom: 1px solid #5086a6;background: #fff;}
  .mc1000{  width:1000px;margin:0 auto}
  .whitebg{ background: #fff;}
  .logo{ font-size: 24px; font-weight: 500;}
  .clearfix:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0}
    .fleft{ float:left;}
    .fright{ float:right;}
    a{ color: #333;text-decoration: none;}
    a:hover{ color: #00b2ff;}
    .ml-20{ margin-left: 20px;}
    .mt-35{ margin-top: 35px;}
    .mt-55{ margin-top: 55px;}
    .mt-15{ margin-top: 15px;}
    .blue2{ color: #5086a6;}
    .f12{ font-size: 12px;}
    .f20{ font-size: 20px;}
    .dashline{ border:1px dashed #c1c9cc;}
    .footer{ border-top:1px solid #4f5262; margin-top: 55px; color: #1d1c1b;}
    .footer-right a{ color: #867e8b; margin-top: 18px; float: left;}
  </style>
</head>
<body>
  <div class="header">
    <div class="mc1000 clearfix">
      <h1 class="logo fleft">Cokemeti</h1>
      <!-- <div class="fright">
        <a href="#" title="" class="">联系</a>
        <a href="#" title="" class="ml-20">主网站</a>
      </div> -->
    </div>
  </div>
  <div class="container mc1000">
	<c:if test="${type==11}">
     <div class="mt-55 f20 blue2">项目确认(接受)</div>                    		
     <p class="f12 mt-15">感谢您确认接受此咨询项目</p>
  	</c:if>
  	<c:if test="${type==-1}">
     <div class="mt-55 f20 blue2">项目确认(不接受)</div>                    		
     <p class="f12 mt-15">感谢您确认不接受此项目咨询</p>
  	</c:if>
    <p class="f12 mt-15">如果您有任何问题或者疑问，请随时与我们取得联系。</p>
 <!--    <p class="dashline mt-15"></p>
    <div class="mt-35 f20 blue2">咨询：安防产品的svac标准</div> -->
  </div>
  <div class="footer clearfix mc1000">
    <span class="f12 fleft mt-15">©2017 版权所有</span>
    <div class="fright footer-right">
      <a href="#" title="" class="">联系我们</a>
      <a href="#" title="" class="ml-20">使用条款</a>
      <a href="#" title="" class="ml-20">隐私政策</a>
    </div>
  </div>
</body>
</html>