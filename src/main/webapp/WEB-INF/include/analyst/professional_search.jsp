<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
 <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<%=basePath%>/static/css/pagination.css"/>
    <style type="text/css">
    	
    	.properties_name{
    		    margin-top: 10px;
    			text-align: right;
    	}
    	
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
        
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
             <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>搜索专家</h3>
              </div>

              <div class="title_right">
                <%-- <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" id="keyword" placeholder="全文搜索" value="${sessionScope.searchWord}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="J_search_button" type="button">Go!</button>
                    </span>
                  </div>
                </div> --%>
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" id="keyword_mohu" placeholder="搜索" value="${sessionScope.searchWord}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="J_mohu_search_button" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            
            <div id ="listbox" class="row-fluid" >
				<div class="search_result_error"></div>
				<div class="widget-box">
					<div class="widget-title bg_lg">
						<span class="icon">
							<i class="icon-list-ul"></i>
						</span>
						<h5></h5>
					</div>
					<div class="widget-content">
						<div class="row-fluid">
							<div class="Searchitems">
									<input type="hidden" name="q" value="q">
									<input type="hidden" name="filter" value="true">
							</div>

							<div id="Pagination" class="pagination"><!-- 这里显示分页 -->
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div id = "list"></div>
				</div>
			</div>
            
        </div>
       	</div>
       </div>
        <!-- footer -->
        <input type="hidden" name="flg" value="0"></input>
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Dropzone.js -->
    <script src="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
	<script src="<%=basePath%>/static/js/jquery.pagination.js"></script>
 	<script type="text/javascript">
 	$(function(){
		$("#listbox").hide();
	/* 	$("#J_search_button").click(function(){
			$("input[name=flg]").val(0);
			searchProfessionals(0);
		});
		
		$("#keyword").bind('keydown',function(event){
			  if(event.keyCode ==13){
				  $("#J_search_button").click();	
			  }
		}); */
		
		$("#J_mohu_search_button").click(function(){
			$("input[name=flg]").val(0);
			searchMohuProfessionals(0);
		});
		
		$("#keyword_mohu").bind('keydown',function(event){
			  if(event.keyCode ==13){
				  $("#J_mohu_search_button").click();	
			  }
		});
		
		function searchMohuProfessionals(page){
			var pageNo= page+1;
			var keyword = $("#keyword_mohu").val();
			var url = "/analyst/professional/search/mohu";
			if(keyword != ''){
				$.ajax({
			        type: 'post',
			        dataType: 'json',
			        url: '<%=basePath%>' + url,
			        data: {'keyword':keyword,'pageNo':pageNo},
			        success: function (data) {
						var result = data;
						console.log(result);
			        	if(result.code > 0){
			        		var d = result.data;
			        		var totalHits = d.recordsTotal;
							$(".widget-title h5").html("共找到约" + totalHits+ "个结果")//总数
							if(totalHits>0){
								var f= $("input[name=flg]").val();
								if(f==0){
									$("input[name=flg]").val(1);
									$(".pagination").pagination(totalHits, {
									    num_edge_entries: 2,
									    num_display_entries: 4,
									    callback:searchMohuProfessionals,
									    items_per_page:10
									});
								}				
					        	var resultHitList = d.data;
					        	var html ="";
					        	$("#list").empty();
					        	console.log(resultHitList);
					            for(var i=0;i<resultHitList.length;i++){
					            	console.log(resultHitList[i]);
					            	var hit = resultHitList[i];
									html += '<table width="97%" border="0" cellspacing="0" cellpadding="0" class="Searchlist"><tbody><tr><td width="80%" colspan="2" class="font14">ID：';
									html += hit.id;
									html += '</td></tr>'
									/*<tr><td width="20%">电话：;
							        html += hit.phone + '</td><td>邮箱：';
							        html += hit.email + '</td></tr> */
							        html += '<tr><td colspan="2"><h4>';
							        html += '<a href="<%=basePath%>/analyst/professional/detail/ ' + hit.id +'" >' + hit.name + ' | '+ hit.company  + ' | ' + hit.title +"</a>" ;
							        html += '</h4></td></tr><tr><td oncopy="return false;" oncut="return false;" oncontextmenu="window.event.returnValue=false" colspan="2">' + hit.workExp +' </td></tr></tbody></table>';
					        		html += '<br/><br/>'
					            }
								$("#list").append(html);
								if(html!="")
									$("#listbox").show();
							}else{
								$("#list").empty();
								$("#Pagination").empty();
							}
			        	}
			        },
			        error:function(XMLHttpRequest, textStatus, errorThrown){
			        	alert("出错了。。。"); //
			        }
		    	});
				
			}
	    }
		
		/* searchProfessionals(0); */
		function searchProfessionals(page){
			var pageNo= page+1;
			var keyword=$('#keyword').val();
			var url = "/analyst/professional/search";
			if(keyword != ''){
				$.ajax({
			        type: 'post',
			        dataType: 'json',
			        url: '<%=basePath%>' + url,
			        data: {'keyword':keyword,'pageNo':pageNo},
			        success: function (data) {
						console.log(data);
			        	var result = data;
			        	if(result.code > 0){
			        		var d = result.data;
			        		var totalHits = d.recordsTotal;
							$(".widget-title h5").html("共找到约" + totalHits+ "个结果")//总数
							if(totalHits>0){
								var f= $("input[name=flg]").val();
								if(f==0){
									$("input[name=flg]").val(1);
									$(".pagination").pagination(totalHits, {
									    num_edge_entries: 2,
									    num_display_entries: 4,
									    callback:searchProfessionals,
									    items_per_page:10
									});
								}				
					        	var resultHitList = d.data;
					        	var html ="";
					        	$("#list").empty();
					        	console.log(resultHitList);
					            for(var i=0;i<resultHitList.length;i++){
					            	/* var hit = JSON.parse(resultHitList[i]); */
					            	var hit = resultHitList[i];
									console.log(hit);
									html += '<table width="97%" border="0" cellspacing="0" cellpadding="0" class="Searchlist"><tbody><tr><td width="80%" colspan="2" class="font14">ID：';
									html += hit.id;
									html += '</td></tr>'
									/*<tr><td width="20%">电话：;
							        html += hit.phone + '</td><td>邮箱：';
							        html += hit.email + '</td></tr> */
							        html += '<tr><td colspan="2"><h4>';
							        html += '<a href="<%=basePath%>/analyst/professional/detail/ ' + hit.id +'" >' + hit.name + ' | '+ hit.company  + ' | ' + hit.title +"</a>" ;
							        html += '</h4></td></tr><tr><td oncopy="return false;" oncut="return false;" oncontextmenu="window.event.returnValue=false" colspan="2">' + hit.workExp +' </td></tr></tbody></table>';
					        		html += '<br/><br/>'
					            }
								$("#list").append(html);
								if(html!="")
									$("#listbox").show();
							}else{
								$("#list").empty();
								$("#Pagination").empty();
							}
			        	}
			        },
			        error:function(XMLHttpRequest, textStatus, errorThrown){
			        	alert("出错了。。。"); //
			        }
		    	});
				
			}
	    }
	});
    </script>
  </body>
</html>