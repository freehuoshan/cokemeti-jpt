<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
	<!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Autosize -->
    <script src="<%=basePath%>/static/vendors/autosize/dist/autosize.min.js"></script>
    
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/jquery.bootstrap-touchspin.css" rel="stylesheet">
    <style type="text/css">
    	.properties_name{
    		    margin-top: 10px;
    			text-align: right;
    	}
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>项目信息</h3>
              </div>
            </div>
            <input type="hidden" id="project_id" value="${p.id}"/>
            <!-- 基本信息 -->
            <div class="clearfix"></div>
			<%@include file="project_add_info.jsp" %>
            <div class="clearfix"></div>
			<%@include file="project_add_requirement.jsp" %>        	
        	<div class="clearfix"></div>
			<%@include file="project_add_professional.jsp" %>    
			<div class="clearfix"></div>
			<%@include file="project_add_report.jsp" %>             	
        </div>
       </div>
       </div>
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- Datatables -->
    <script src="<%=basePath%>/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    
    <!-- iCheck -->
    <script src="<%=basePath%>/static/vendors/iCheck/icheck.min.js"></script>
    <!-- NProgress -->
    <script src="<%=basePath%>/static/vendors/nprogress/nprogress.js"></script>
    <!-- Dropzone.js -->
    <script src="<%=basePath%>/static/vendors/dropzone/dist/min/dropzone.min.js"></script>
    
    <!-- bootstrap-daterangepicker -->
    <script src="<%=basePath%>/static/vendors/moment/min/moment.min.js"></script>
    <script src="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <script src="<%=basePath%>/static/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    
    <!-- bootstrap-wysiwyg -->
    <script src="<%=basePath%>/static/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<%=basePath%>/static/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<%=basePath%>/static/vendors/google-code-prettify/src/prettify.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    <!-- jQuery Validation plugin javascript-->
    <script src="<%=basePath%>/static/js/jquery.form.js"></script>
    <script src="<%=basePath%>/static/js/validate/jquery.validate.min.js"></script>
    <script src="<%=basePath%>/static/js/validate/messages_zh.min.js"></script>
    <script src="<%=basePath%>/static/js/sweetalert/sweetalert.min.js"></script>
    <script src="<%=basePath%>/static/js/jquery.bootstrap-touchspin.min.js"></script>
    <script>
    var requirement_table;
    var report_table;
    var professional_table;
    
    $(document).ready(function() {
    	
    	$("#project_priceTimes").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 1,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '倍'
        });
    	//校验新增专家分类
      validate();
      autosize($('.resizable_textarea'));
      if ($("#datatable-customers").length) {
          var customer_table =  $("#datatable-customers").DataTable({
            	"processing": true,
                "serverSide": true,
                "stateSave": true,
                "ajax":{
                	url:'<%=basePath%>/sales/customer/getlist',
                	type:'POST',
                },
                "columns": [
                            { "data": "username" },
                            { "data": "name" },
                            { "data": "status" },
                            { "data": "level" },
                            { "data": "amount" }
                        ],
           	  	"rowId":"id",
           	  	"lengthChange": false,
              	"searching":true,
              	"searchDelay": -1,
              	"info":false,
           	  	"ordering":false
  			 });
     }
     $('#datatable-customers tbody').on( 'dblclick', 'tr', function () {
	   	    var data =  customer_table.row( this ).data();
	 		var id =data.id;
	 		var companyName = data.name;
	 		var username = data.username;
	 		var amount = data.amount;
	 		$("#customer_id").val(id);
	 		$("#company_name").val(companyName);
	 		$("#customer_name").val(username);
	 		$("#amount").val(amount);
	 		$("#info_close").click();
   	 } );
     getProfessionalList();
     getRequirementInfo();
     getReports();
     Dropzone.autoDiscover = false;
     var dropz = new Dropzone("#myDropzone", {
    	  url: "<%=basePath%>/file/upload",
          addRemoveLinks: true,
          dictRemoveLinks: "x",
          dictCancelUpload: "x",
          maxFiles: 10,
          maxFilesize: 50,
  		  init: function() {
  			 this.on("success", function(file, response) {
  	          	 if(response != "0"){
  	          		 var string = response;
  	          		 var arr = string.split('.');
  	          		 var id = arr[0];
  	          		 var data = response;
  	          		 var html = '<input type="hidden" id='+id+' value="'+data+ '" data-filename ="' +file.name+'"/>';
  	          		 $("#files").append(html);
  	          	 }
  	          });
             this.on("removedfile", function(file) {
	       		 var string = file.name;
	      		 var arr = string.split('.');
	      		 var id = arr[0];
	             $('#'+id).remove();
             });
          }
      });
    });
    
    $("#professional_search").click(function(){
    	var professionalId = $("#professional_id").val();
    	$.ajax({
			type:"get",
			url:'<%=basePath%>/analyst/professional/'+professionalId,
			dataType:'json',
			success:function(d){
				if(d.code != 0){
					$("#professional_name").val(d.data.name);
					$("#professional_title").val(d.data.title);
					$("#professional_company").val(d.data.company);
					$("#professional_price").val(d.data.price);
					$("#professional_email").val(d.data.email);
					
					$("#project_title").val(d.data.title);
					$("#project_company").val(d.data.company);
					$("#project_exp").val("");
				}else{
					alert("无对应专家");
					$("#professional_id").focus();
					$("#project_professional_form")[0].reset();
				}
			}
		});
    });
    
    $("#professional_save").click(function(){
    	//var professionalId = $("#professional_id").val();
    	var projectId = $("#project_id").val();
    	if(projectId == null || projectId==""){
    		alert("请先保存项目基本信息");
    		$("#professional_close").click();
    	}else{
    		$("#p_project_id").val(projectId);
    		$("#project_professional_form")[0].submit();
    	}
    });
    
    $("#professional_email_save").click(function(){
    	//var professionalId = $("#professional_id").val();
    	var projectId = $("#project_id").val();
    	if(projectId == null || projectId==""){
    		alert("请先保存项目基本信息");
    		$("#professional_email_close").click();
    	}else{
    		var content = $('#editor').html();
    		var subject = $('#subject').val();
    		var cc = $('#cc').val();
    		var receiver = $('#receiver').val();
    		var type = $('#email_type').val();
    		var operatype = $("#operat_type").val();
    		var ppId = $('#ppId').val();
    		$.ajax({
    			type:"post",
    			url:'<%=basePath%>/analyst/project/professional_email/add',
    			data:{'receiver':receiver,'cc':cc,'subject':subject,'content':content, 'type':type,"operatype":operatype, 'projectId':projectId,'ppId':ppId},
    			dataType:'json',
    			success:function(d){
    				if(d.code != 0){
						alert("发送成功");
						$('#editor').html('');
						$('#professional_email_form')[0].reset();
						$("#professional_email_close").click();
						
						professional_table.ajax.reload();
    				}else{
    					alert("发送失败，请重试");
    				}
    			}
    		});
    	}
    });
    
    
    $("#delete_button").click(function(){
    	var projectId = $("#project_id").val();
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/project/delete",
			data:{"projectId" : projectId},
			dataType:'text',
			success:function(d){
				if(d == 'success'){
					swal({
	 					title: "",
	 					text: "成功",
	 					type: "success"
	 				});
					setTimeout(function(){
						location.href='<%=basePath%>/analyst/project/'+projectId;
					},1000);
					
				}
				else{
					swal({
	 					title: "",
	 					text: "失败",
	 					type: "error"
	 				});
				}
			}
		});
    });
    
    $("#info_submit").click(function(){
    	var projectId = $("#project_id").val();
    	var projectName = $("#project_name").val();
    	var customerId = $("#customer_id").val();
    	var startDate = $("#single_cal_start").val();
    	var endDate = $("#single_cal_end").val();
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/project/addOrUpdate",
			data:{"customerId":customerId,"id":projectId,"startDate":startDate,"endDate":endDate,"name":projectName},
			dataType:'json',
			success:function(d){
				alert(d.msg);
				if(d.code != 0){
					$("#project_id").val(d.code);
				}
			}
		});
    });
    
    $('#requirement_save').click(function(){
    	var info = $("#requirement_info").val();
    	var projectId = $("#project_id").val();
    	if(projectId == null || projectId==""){
    		alert("请先保存项目基本信息");
    		$("#requirement_close").click();
    	}else{
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/analyst/project/requirement/add",
    			data:{"info":info,"projectId":projectId},
    			dataType:'json',
    			async:false,
    			success:function(d){
    				if( d.code != 0){
    					$("#requirement_close").click();
        				getRequirementInfo();
    				}else{
    					alert(d.msg);
    				}
    			}
    		});
    	}
    });
    
    $("#upload_report_button").click(function(){
    	var projectId = $("#project_id").val();
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/project/professionals",
			data:{"projectId":projectId},
			dataType:'json',
			async:false,
			success:function(professionlist){
				$.each(professionlist, function (key, value) { 
					$.each(value, function (index, prop){
						$("#professionals_selector").append("<option value="+prop.id+">" +prop.professionalName + "</option>");
					});
				});
				$(".bs-modal-report").modal("show");
			}
		});
    	
    });
    
    $('#report_save').click(function(){
        var projectId = $("#project_id").val();
        var ppId = $('#professionals_selector option:selected').val();
        var report_type = $("input[name='report_type']:checked").val();
    	if(projectId == null || projectId==""){
    		alert("请先保存项目基本信息");
    		$("#requirement_close").click();
    	}else{
    		var files='';
    		var filenames='';
        	$("#files input[type='hidden']").each(function(){
        	   files = files+"|"+$(this).val()
        	   filenames = filenames + "|" + $(this).data("filename");
        	});  
    		if(files != ''){
	    		$.ajax({
	    			type:"post",
	    			url:"<%=basePath%>/analyst/project/reports/add",
	    			data:{"reports":files,"filenames":filenames, "projectId":projectId, "ppId":ppId, "reportType":report_type},
	    			dataType:'json',
	    			async:false,
	    			success:function(d){
	    				$("#report_close").click();
	    				getReports();
	    			}
	    		});
    		}
    	}
    });
    
    function getProfessionalList(){
    	var projectId = $("#project_id").val();
    	if(projectId == null || projectId==""){
    	}else{
	    	$.ajax({
				type:"POST",
				url:'<%=basePath%>/analyst/project/professionals',
				dataType:'json',
				data:{"projectId":projectId},
				success:function(professionlist){
					
					$.each(professionlist, function (key, value) { 
						var typArr = key.split("_");
						var protd;
						var profession_type = "<tr>"+
						"<td colspan='10' id='profession_type_td"+typArr[1]+"' class='profession_type_td'>" + typArr[0] + "</td>"+
						"<td><button type='button' onclick='toUpdateTypePage("+typArr[1]+")' class='btn btn-success btn-xs'>编辑</button></td>"+
						"<td><button type='button' onclick='deleteTypeByTypeId("+typArr[1]+")' class='btn btn-success btn-xs'>删除</button></td>"+
						"</tr>";
						$("#datatable-professional-tbody").append(profession_type);
						
						$.each(value, function (index, prop){
							var info;
							switch(prop.status)
                     		{
                     			case 0:
                     		  		info='未发送专家邮件';
                     		  		break;
                     			case 10:
                             		info='专家未回复';
                             		break;
                     			case 11:
                     				info='专家已同意';
                     				break;
                     			case -1:
                     				info='专家已拒绝';
                     				break;
                     			case 12: 
                     				info="已发送专家付款信息";
                     				break;
                     		}
							var customer_info;
							switch(prop.customerStatus)
							{
								case 0:
									customer_info = "未发送客户邮件";
									break;
								case 20:
									customer_info='已发客户邮件';
	                 				break;
	                 			case 21:
	                 				customer_info='被选中';
	                 				break;
	                 			case -2:
	                 				customer_info='未被选中';
	                 				break;
	                 			case 23:
	                 				customer_info="已发送客户专家详情";
	                 				break;
							}
							
							var td8;
							var info8="发送邮件";
							
							td8 = '<button type="button" onclick="sendProEmail('+prop.id+')" class="btn btn-success btn-xs">'+info8+'</button>'+
							'<button type="button" onclick="sendProEmailEnglish('+prop.id+')" class="btn btn-success btn-xs">发送英文邮件</button>'+
             		  		'<button type="button" onclick="sendProPayEmail('+prop.id+')" class="btn btn-success btn-xs">获取付款方式</button>'+
             		  		'<button type="button" onclick="updateStatus('+prop.id+',21,this)" class="btn btn-success btn-xs">选中</button>'+'<button type="button" onclick="updateStatus('+prop.id+',-2,this)" class="btn btn-success btn-xs">未选中</button>';
							
						/* 	switch(prop.status)
                     		{
                     			case 0:
                     		  		info8='发送邮件';
                     		  		td8 = '<button type="button" onclick="sendProEmail('+prop.id+')" class="btn btn-success btn-xs">'+info8+'</button>'+
                     		  		'<button type="button" onclick="sendProPayEmail('+prop.id+')" class="btn btn-success btn-xs">获取付款方式</button>';
                     		  		break;
                     			case 20:
                     				td8= '<button type="button" onclick="updateStatus('+prop.id+',21)" class="btn btn-success btn-xs">选中</button>'+'<button type="button" onclick="updateStatus('+prop.id+',-2)" class="btn btn-danger btn-xs">未选中</button>'
                     				break;
                     		  	default:
                     		  		info8='发送邮件';
                     		  		td8= '<button type="button" onclick="sendProEmail('+prop.id+')" class="btn btn-success btn-xs">'+info8+'</button>' +
                 		  				   '<button type="button" onclick="sendProPayEmail('+prop.id+')" class="btn btn-success btn-xs">获取付款方式</button>';
                     		  		break;
                     		} */
							
							var td9 =  '<button type="button" onclick="deleteByProfessionId('+prop.professionalId +','+projectId+')" class="btn btn-success btn-xs">删除</button>';
							var td10 = '<button type="button" onclick="toProfessionUpdate('+prop.professionalId+','+projectId+')" class="btn btn-success btn-xs">编辑</button>';
							
							protd =	"<tr>"　+
							"<td><input type='checkbox' name='checkItem' class='checkbox-td' value='" +  prop.id + "'/></td>" +
							"<td>" + prop.professionalId + "</td>" +
							"<td>" + prop.professionalName + "</td>" +
							"<td>" + prop.title + "</td>" +
							"<td>" + prop.company + "</td>"+
							"<td>" + prop.exp + "</td>"+
							"<td>" + prop.professionalPrice + "</td>"+
							"<td>" + prop.reportPrice + "</td>"+
							"<td id='"+prop.id+"_customer_info'>" + customer_info + "</td>"+
							"<td>" + info + "</td>"+
							"<td>" + td8 + "</td>"+
							"<td>" + td9 + "</td>"+
							"<td>" + td10 + "</td>" +
							"</tr>";
							$("#datatable-professional-tbody").append(protd);
						});
						
					 });
					
				}
					
			});
    	}
    } 
    
  
    function deleteTypeByTypeId(typeId){
    	var projectId = $("#project_id").val();
    	swal({
	        title: "您确定要删除这条信息吗",
	        text: "删除后将无法恢复，请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "删除",
	        cancelButtonText: "取消",
	        closeOnConfirm: false
	    }, function(){
	    	$.ajax({
				type:"post",
				url:"<%=basePath%>/analyst/project/profession_type/delete",
				data:{"professionType" : typeId, "projectId" : projectId},
				dataType:'text',
				success:function(d){
					if(d == "成功"){
						swal({
		 					title: "",
		 					text: d,
		 					type: "success"
		 				});
						setTimeout(function(){
							location.href='<%=basePath%>/analyst/project/'+projectId;
						},1000);
						
					}
					else{
						swal({
		 					title: "",
		 					text: d,
		 					type: "error"
		 				});
					}
				}
			});
	    });
    	
    	
    }
    
    function toUpdateTypePage(typeId){
    	var typeStr = $("#profession_type_td"+typeId).html();
    	$("#professiontype_add_textfield").val(typeStr);
    	$("#professionadd_title").html("编辑分类");
    	$("#profession_type_add_projectid").remove();
    	if($("#profession_type_id").length <= 0){
    		$("#profession_type_addform").append("<input type='hidden' id='profession_type_id' name='id' value='"+ typeId+"'/>");
    	}
    	else{
    		$("#profession_type_id").val(typeId);
    	}
    	$("#profession_type_addform").attr("action","<%=basePath%>/analyst/project/profession_type/update")
    	
    	$(".bs-modal-add-professiontype").modal("show");
    }
    
 
    //点击新增分类按钮
    $("#addProfessionType").click(function(){
    	$("#professiontype_add_textfield").val("");
    	$("#profession_type_id").remove();
    	$("#professionadd_title").html("新增分类");
    	var projectId = $("#project_id").val();
    	if($("#profession_type_add_projectid").length <= 0){
    		$("#profession_type_addform").append("<input type='hidden' id='profession_type_add_projectid' name='projectId' value='"+ projectId+"'/>");
    	}
    	else{
    		$("#profession_type_addform").val(projectId);
    	}
    	$("#profession_type_addform").attr("action","<%=basePath%>/analyst/project/profession_type/add")
    	$(".bs-modal-add-professiontype").modal("show");
    });
    

    $("#addprofession").click(function(){
    	$.ajax({
			type:"POST",
			url:'<%=basePath%>/analyst/project/profession_type/list',
			data:{"projectId" : $("#project_id").val()},
			dataType:'json',
			success:function(typelist){
				 if(typelist.length  ==0 ){
					 swal({
		 					title: "",
		 					text: "现在没有任何分类，请先添加分类",
		 					type: "warning"
		 				});
					 return;
				 }
				 else{
					 $("#profession_type").html("");
					 $.each(typelist, function (key, value) { 
						 $("#profession_type").append("<option value="+value.id+">" +value.type + "</option>");
					 });
					 $("#professional_name").val("");
						$("#professional_title").val("");
						$("#professional_company").val("");
						$("#professional_price").val("");
						$("#professional_email").val("");
						$("#professional_id").val("");
						$("#professional_name").val("");
						$("#professional_price").val("");
						$("#project_title").val("");
						$("#project_company").val("");
						$("#project_exp").val("");
						$("#report_price").val("")
				    	
				    	$(".lg-professional").modal("show");
				 }
				
			}
		});
    	
    	
    })
    
    function toProfessionUpdate(professionId, projectId){
    
    	$.ajax({
			type:"POST",
			url:'<%=basePath%>/analyst/project/profession_type/list',
			data: {"projectId" : $("#project_id").val()},
			dataType:'json',
			success:function(typelist){
					 if(typelist.length  ==0 ){
						 swal({
			 					title: "",
			 					text: "现在没有任何分类，请先添加分类",
			 					type: "warning"
			 				});
						 return;
					 }
					 else{
						 $("#profession_type").html("");
						 $.each(typelist, function (key, value) {
							 $("#profession_type").append("<option value="+value.id+">" +value.type + "</option>");
						
						 });
						 
						 $.ajax({
								type:"post",
								url:'<%=basePath%>/analyst/professional/toProfessionUpdate/'+projectId+'/'+professionId,
								dataType:'text',
								async: false,
								success:function(res){
									var projectProfession = $.parseJSON(res);
									$.ajax({
										type:"get",
										url:'<%=basePath%>/analyst/professional/'+projectProfession.projectProfession.professionalId,
										dataType:'json',
										success:function(d){
												$("#professional_name").val(d.data.name);
												$("#professional_title").val(d.data.title);
												$("#professional_company").val(d.data.company);
												$("#professional_email").val(d.data.email);
												$("#professional_exp").val(d.data.workExp);
										}
									});
									$("#professional_id").val(projectProfession.projectProfession.professionalId);
									$("#professional_name").val(projectProfession.projectProfession.professionalName);
									$("#professional_price").val(projectProfession.projectProfession.professionalPrice);
									$("#project_title").val(projectProfession.projectProfession.title);
									$("#project_company").val(projectProfession.projectProfession.company);
									$("#project_exp").val(projectProfession.projectProfession.exp);
									$("#report_price").val(projectProfession.projectProfession.reportPrice)
									$("#profession_type").val(projectProfession.projectProfession.professionType)
									$("input[name='status'][value='"+projectProfession.projectProfession.status+"']").attr("checked",true); 
									$("#project_priceTimes").val(projectProfession.projectProfession.priceTimes);
									$(".lg-professional").modal("show");
								}
							});
					 }
				 }
			});
    }
    
    function deleteByProfessionId(professionId,projectId){
    	
    	swal({
	        title: "您确定要删除这条信息吗",
	        text: "删除后将无法恢复，请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "删除",
	        cancelButtonText: "取消",
	        closeOnConfirm: false
	    }, function(){
	    	$.ajax({
				type:"post",
				url:"<%=basePath%>/analyst/professional/delete",
				data:{"professionId" : professionId, "projectId" : projectId},
				dataType:'text',
				success:function(d){
					if(d == "成功"){
						swal({
		 					title: "",
		 					text: d,
		 					type: "success"
		 				});
						setTimeout(function(){
							location.href='<%=basePath%>/analyst/project/'+projectId;
						},1000);
						
					}
					else{
						swal({
		 					title: "",
		 					text: d,
		 					type: "error"
		 				});
					}
				}
			});
	    });
    	
    	
    }
    
    //发送邮件(获取专家支付方式)
    function sendProPayEmail(ppId){
    	if(ppId == null || ppId==""){
    	}else{
    		$('#ppId').val(ppId);
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/analyst/project/professional/email_payinfo",
    			data:{"ppId":ppId},
    			dataType:'text',
    			async:false,
    			success:function(d){
    				var email;
    			 	if (typeof d === 'string') {
    			 		email=eval('(' + d + ')');
    			 		if(email != null)
    			 			sendEmail(1,12,email.receiver,email.cc,email.subject, email.content);
    				}
    			},
    			error:function(XMLHttpRequest, textStatus, errorThrown){
    		        	alert("出错了。。。"); //
    		    }
    		});	
    	}
    }
    
    function sendProEmailEnglish(ppId){
    	if(ppId == null || ppId==""){
    	}else{
    		$('#ppId').val(ppId);
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/analyst/project/professional/email_info_english",
    			data:{"ppId":ppId},
    			dataType:'text',
    			async:false,
    			success:function(d){
    				var email;
    			 	if (typeof d === 'string') {
    			 		email=eval('(' + d + ')');
    			 		if(email != null)
    			 			sendEmail(1,10,email.receiver,email.cc,email.subject, email.content);
    				}
    			},
    			error:function(XMLHttpRequest, textStatus, errorThrown){
    		        	alert("出错了。。。"); //
    		    }
    		});	
    	}
    }
    
    function sendProEmail(ppId){
    	if(ppId == null || ppId==""){
    	}else{
    		$('#ppId').val(ppId);
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/analyst/project/professional/email_info",
    			data:{"ppId":ppId},
    			dataType:'text',
    			async:false,
    			success:function(d){
    				var email;
    			 	if (typeof d === 'string') {
    			 		email=eval('(' + d + ')');
    			 		if(email != null)
    			 			sendEmail(1,10,email.receiver,email.cc,email.subject, email.content);
    				}
    			},
    			error:function(XMLHttpRequest, textStatus, errorThrown){
    		        	alert("出错了。。。"); //
    		    }
    		});	
    	}
    }
    
    function sendProfessionInfo(){
    	var projectId = $("#project_id").val();
    	var professions ="";
    	$("input[name='checkItem']:checked").each(function () {
    		professions += $(this).val() +','
        });
    	if(projectId == null || projectId==""){
    	}else{
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/analyst/project/profession_info/email_info",
    			data:{"projectId":projectId,"ppId":professions},
    			dataType:'text',
    			async:false,
    			success:function(d){
    				var email;
    			 	if (typeof d === 'string') {
    			 		email=eval('(' + d + ')');
    			 		if(email != null){
    			 			sendEmail(2,23,email.receiver,email.cc,email.subject, email.content);
    			 		}else{
    			 			sendEmail(2,23,'','','','');
    			 		}
    				}
    			},
    			error:function(XMLHttpRequest, textStatus, errorThrown){
    		        	alert("出错了。。。"); //
    		    }
    		});	
    	}
    	
    	
    }
    
    function sendCustomerEmail(){
    	var projectId = $("#project_id").val();
    	var professions ="";
    	$("input[name='checkItem']:checked").each(function () {
    		professions += $(this).val() +','
        });
    	
    	if(projectId == null || projectId==""){
    	}else{
    		$.ajax({
    			type:"post",
    			url:"<%=basePath%>/analyst/project/customer/email_info",
    			data:{"projectId":projectId,"ppId" : professions},
    			dataType:'text',
    			async:false,
    			success:function(d){
    				var email;
    			 	if (typeof d === 'string') {
    			 		email=eval('(' + d + ')');
    			 		if(email != null){
    			 			sendEmail(2,20,email.receiver,email.cc,email.subject, email.content);
    			 		}else{
    			 			sendEmail(2,20,'','','','');
    			 		}
    				}
    			},
    			error:function(XMLHttpRequest, textStatus, errorThrown){
    		        	alert("出错了。。。"); //
    		    }
    		});	
    	}
    }
    
    function sendEmail(type,operatType, receiver, cc, subject, content){
    	$('#email_type').val(type);
    	$("#operat_type").val(operatType);
    	$('#receiver').val(receiver);
    	$('#cc').val(cc);
    	$('#subject').val(subject);
    	$('#editor').html(content);
    	
    	$('#sendemail-button').click();
    }
    
    function getRequirementInfo(){
    	var projectId = $("#project_id").val();
    	if(projectId == null || projectId==""){
    	}else{
    		if(typeof(requirement_table)=='undefined'){
		    	if ($("#datatable-requirement").length) {        	  
		    		requirement_table = $("#datatable-requirement").DataTable({
		              "processing": true,
		              "serverSide": false,
		              "ajax":{
		              	"url":'<%=basePath%>/analyst/project/requirements',
		              	"type":"POST",
		              	"data":{
		              		"projectId":projectId
		              	}
		              },
		              "columns": [
		                          { "data": "createTime" },
		                          { "data": "createUserName" },
		                          { "data": "content" },
		                          {"data" : "id"},
		                          {"data" : "id"}
		                      ],
		              "columnDefs" : [{
		            	  targets:3,
                       	  render: function (data, type, row, meta) {
                       		 return '<button type="button" onclick="deleteRequireById('+data+')" class="btn btn-success btn-xs">删除</button>';
                       	  }
		              },{
		            	  targets:4,
                       	  render: function (data, type, row, meta) {
                       		 return '<button type="button" onclick="toUpdatePage('+data+',&quot;'+row.content+'&quot;)" class="btn btn-success btn-xs">编辑</button>';
                       	  }
		              }
		              ],
		              "paging":false,
		         	  "rowId":"id",
		              "searching":false,
		              "info":false,
		          	  "ordering":false
		            });
		          }
    		}else{
    			requirement_table.ajax.reload();
    		}
    	}
    }
    
    function toUpdatePage(requireId,content){
    	
    	
    	$("#update_requirement_info").val(content);
    	$("#require_update_id").val(requireId);
    	$("#require_updatepage").modal("show");
    }
    
    $("#requirement_update").click(function(){
    	var requireId = $("#require_update_id").val();
    	var description = $("#update_requirement_info").val();
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/project/requirement/update",
			data:{"requireId":requireId,"description":description},
			dataType:'text',
			success:function(d){
				if(d == "成功"){
					swal({
	 					title: "",
	 					text: d,
	 					type: "success"
	 				});
					setTimeout(function(){
						location.href='<%=basePath%>/analyst/project/list';
					},1000);
					
				}
				else{
					swal({
	 					title: "",
	 					text: d,
	 					type: "error"
	 				});
				}
			}
		});
    });
    
    function deleteRequireById(requireId){
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/project/requirements/delete",
			data:{"requireId" : requireId},
			dataType:'text',
			success:function(d){
				if(d == "成功"){
					swal({
	 					title: "",
	 					text: d,
	 					type: "success"
	 				});
					setTimeout(function(){
						location.href='<%=basePath%>/analyst/project/list';
					},1000);
					
				}
				else{
					swal({
	 					title: "",
	 					text: d,
	 					type: "error"
	 				});
				}
			}
		});
    }
    
    function getReports(){
    	var url = '<%=request.getSession().getAttribute("url")%>';
    	var projectId = $("#project_id").val();
    	if(projectId == null || projectId==""){
    	}else{
    		if(typeof(report_table)=='undefined'){
		    	if ($("#datatable-report").length) {        	  
		    		report_table = $("#datatable-report").DataTable({
		              "processing": true,
		              "serverSide": false,
		              "ajax":{
		              	"url":'<%=basePath%>/analyst/project/reports',
		              	"type":"POST",
		              	"data":{
		              		"projectId":projectId
		              	}
		              },
		              "columns": [
		                          { "data": "name" },
		                          { "data": "porfessionalName" },
		                          { "data": "typeName" },
		                          { "data": "uploadUserName" },
		                          { "data": "uploadTime"},
		                          { "data": "id"}
		                      ],
                     "columnDefs":[
                              {
                                   	targets: 0,
                                   	render: function (data, type, row, meta) {
                                       return '<a href="'+url+row.path+'">' + data +'</a>';
                                   	}
                              },
                              {
                                 	targets: 5,
                                 	render: function (data, type, row, meta) {
                                 		return '<button type="button" onclick="deleteByReportId('+data+')" class="btn btn-success btn-xs">删除</button>';
                                 	}
                            },
                              
                      ],  
		              "paging":false,
		         	  "rowId":"id",
		              "searching":false,
		              "info":false,
		          	  "ordering":false
		            });
		          }
    		}else{
    			report_table.ajax.reload();
    		}
    	}
    }
    
    function deleteByReportId(reportId){
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/project/reports/delete",
			data:{"reportId" : reportId},
			dataType:'text',
			success:function(d){
				if(d == "成功"){
					swal({
	 					title: "",
	 					text: d,
	 					type: "success"
	 				});
					setTimeout(function(){
						location.href='<%=basePath%>/analyst/project/list';
					},1000);
					
				}
				else{
					swal({
	 					title: "",
	 					text: d,
	 					type: "error"
	 				});
				}
			}
		});
    }
    
    function updateStatus(ppId, status,obj){
    	$.ajax({
			type:"post",
			url:"<%=basePath%>/analyst/project/professional/status",
			data:{"ppId":ppId,'customerStatus':status},
			dataType:'json',
			async:false,
			success:function(d){
				if(d.code>0){
					$(obj).attr("class","btn btn-danger btn-xs");
					if(status == 21){
						$("#"+ppId+"_customer_info").html("被选中");
					}
					if(status == -2){
						$("#"+ppId+"_customer_info").html("未被选中");
					}
					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
		        	alert("出错了。。。"); //
		    }
		});	
    }

	$('#single_cal_start').daterangepicker(
	{
		singleDatePicker : true,
		singleClasses : "picker_4",
			locale: {
		          format: 'YYYY-MM-DD'
		        }
	},
	function(start, end, label) {
		console.log(start.toISOString(), end
				.toISOString(), label);
	});
	$('#single_cal_end').daterangepicker(
	{
		singleDatePicker : true,
		singleClasses : "picker_4",
		locale: {
	          format: 'YYYY-MM-DD'
	        }
	},
	function(start, end, label) {
		console.log(start.toISOString(), end
				.toISOString(), label);
	});
	
	</script>
	
	<!-- bootstrap-wysiwyg -->
    <script>
    $(document).ready(function() {
        function initToolbarBootstrapBindings() {
          var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'
            ],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
          $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
          });
          
          var colors = ['red', 'black', 'gray', 'blue'],
          colorTarget = $('[title=Color]').siblings('.dropdown-menu');
          $.each(colors, function(idx, colorName) {
        	  colorTarget.append($('<li><a data-edit="ForeColor ' + colorName + '" style="color:\'' + colorName + '\'">' + colorName + '</a></li>'));
          });
                   
          $('a[title]').tooltip({
            container: 'body'
          });
          $('.dropdown-menu input').click(function() {
              return false;
            })
            .change(function() {
              $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
            })
            .keydown('esc', function() {
              this.value = '';
              $(this).change();
            });

          $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this),
              target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
          });

          if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $('#editor').offset();

            $('.voiceBtn').css('position', 'absolute').offset({
              top: editorOffset.top,
              left: editorOffset.left + $('#editor').innerWidth() - 35
            });
          } else {
            $('.voiceBtn').hide();
          }
        }

       function showErrorAlert(reason, detail) {
          var msg = '';
          if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
          } else {
            console.log("error uploading file", reason, detail);
          }
          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        initToolbarBootstrapBindings();

        $('#editor').wysiwyg({
          fileUploadError: showErrorAlert
        });

        window.prettyPrint;
        prettyPrint();
      });
    
    $("#professiontype_save").on("click",function(){
 	   $("#profession_type_addform").submit();
    })
    
  //新增分类验证
    function validate(){
    	var projectId = $("#project_id").val();
    	// validate signup form on keyup and submit
    	var icon = "<i class='fa fa-times-circle'></i> ";
    	validator = $("#profession_type_addform").validate({
    			    rules: {
    			    	type:{
    			    	 	required:true,
    						checkTypeRepeat: true
    			    	}
    			    },
    			    messages: {
    			    	type:{
    			    		required: icon+"为必填值不可为空"
    			    	}
    			    },
    			    submitHandler: function(form){
    			    	$(form).ajaxSubmit({
    			    		type: "POST",
    			    		dataType: "json",
    			    		success: function(data){
    			    			 if(typeof data == 'boolean'){
    			    				 if(data){
    			    					 swal({
    	 			    	 					title: "",
    	 			    	 					text: "成功",
    	 			    	 					type: "success"
    	 			    	 				});
    			    						setTimeout(function(){
    			    							location.href='<%=basePath%>/analyst/project/'+ projectId;
    			    						},1000);
    			    				 }
    			    				 else{
    			    					 swal({
 	 			    	 					title: "",
 	 			    	 					text: "失败",
 	 			    	 					type: "error"
 	 			    	 				});
    			    				 }
    			    				 $(".bs-modal-add-professiontype").modal("hide");
    			    	    	   }
    			    	    	   else{
    			    	    		   swal({
    			    	 					title: "",
    			    	 					text: "发生错误",
    			    	 					type: "error"
    			    	 				});
    			    	    		   return;
    			    	    	   }
    			    		}
    			    	});
    			    }
    			});

    	$.validator.addMethod("checkTypeRepeat",function(value,element,params){  
    		var param = {};
    		param.type = value;
    		param.projectId = $("#project_id").val();
    		if($("#professiontype_id").length >0){
    			param.id = $("#professiontype_id").val();
    		}
    		var result;
    		$.ajax({  
    	       url: "<%=basePath%>/analyst/project/profession_type/checkRepeat",  
    	       dataType: "json", 
    	       data: param,
    	       type: "POST",
    	       async:false,
    	       success: function (data) {  
    	    	   if(typeof data == 'boolean'){
    	    		  result=data;
    	    	   }
    	    	   else{
    	    		   swal({
    	 					title: "",
    	 					text: "发生错误",
    	 					type: "error"
    	 				});
    	    		   return;
    	    	   }
    	       },  
    	       error: function (XMLHttpRequest, textStatus, errorThrown) {  
    	    	   swal({
    					title: "",
    					text: "发生错误",
    					type: "error"
    				});
    	       }  
    	   });  
    		return this.optional(element)||!result;  
        },icon+"已存在该分类");  
    }
    
    </script>
  </body>
</html>