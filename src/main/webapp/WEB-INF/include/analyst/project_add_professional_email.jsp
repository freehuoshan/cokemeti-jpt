<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="myModalLabel2">发送专家列表</h4>
		</div>

		<div class="modal-body">
			<form id="professional_email_form" action="" method="post">
				  <input type="hidden" id="operat_type" name="operat_type"/>
				  <input type="hidden" id="email_type" name="email_type">
				  <input type="hidden" id="ppId" name="ppId">
                  <div class="form-group">
                    <label for="ex3">收件人</label>
                    <input type="text" id="receiver" class="form-control" name="receiver" placeholder=" " value="${c.email};">
                  </div>
                  
                  
                  <div class="form-group">
                    <label for="ex4">抄送</label>
                    <input type="email" id="cc" class="form-control" name="cc" placeholder=" ">
                  </div>
 
                  <div class="form-group">
                    <label for="ex4">主题</label>
                    <input type="email" id="subject" class="form-control" name="subject" placeholder=" ">
                  </div>
                                   
				  <div class="form-group">
						<div id="alerts"></div>
						<div class="btn-toolbar editor" data-role="editor-toolbar"
							data-target="#editor">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i
									class="fa fa-font"></i><b class="caret"></b></a>
								<ul class="dropdown-menu">
								</ul>
							</div>

							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" title="Color"><i
									class="fa fa-th-large"></i><b class="caret"></b></a>
								<ul class="dropdown-menu">
								</ul>
							</div>
			
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown"
									title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b
									class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a data-edit="fontSize 5">
											<p style="font-size: 17px">Huge</p>
									</a></li>
									<li><a data-edit="fontSize 3">
											<p style="font-size: 14px">Normal</p>
									</a></li>
									<li><a data-edit="fontSize 1">
											<p style="font-size: 11px">Small</p>
									</a></li>
								</ul>
							</div>
			
							<div class="btn-group">
								<a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i
									class="fa fa-bold"></i></a> <a class="btn" data-edit="italic"
									title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a> <a
									class="btn" data-edit="strikethrough" title="Strikethrough"><i
									class="fa fa-strikethrough"></i></a> <a class="btn"
									data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i
									class="fa fa-underline"></i></a>
							</div>
			
							<div class="btn-group">
								<a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i
									class="fa fa-list-ul"></i></a> <a class="btn"
									data-edit="insertorderedlist" title="Number list"><i
									class="fa fa-list-ol"></i></a> <a class="btn" data-edit="outdent"
									title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
								<a class="btn" data-edit="indent" title="Indent (Tab)"><i
									class="fa fa-indent"></i></a>
							</div>
			
							<div class="btn-group">
								<a class="btn" data-edit="justifyleft"
									title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
								<a class="btn" data-edit="justifycenter"
									title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
								<a class="btn" data-edit="justifyright"
									title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
								<a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i
									class="fa fa-align-justify"></i></a>
							</div>
			 				<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown"
									title="Hyperlink"><i class="fa fa-link"></i></a>
								<div class="dropdown-menu input-append">
									<input class="span2" placeholder="URL" type="text"
										data-edit="createLink" />
									<button class="btn" type="button">Add</button>
								</div>
								<a class="btn" data-edit="unlink" title="Remove Hyperlink"><i
									class="fa fa-cut"></i></a>
							</div>
			 				<div class="btn-group">
								<a class="btn" title="Insert picture (or just drag & drop)"
									id="pictureBtn"><i class="fa fa-picture-o"></i></a> <input
									type="file" data-role="magic-overlay" data-target="#pictureBtn"
									data-edit="insertImage" />
							</div>
			
							<div class="btn-group">
								<a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i
									class="fa fa-undo"></i></a> <a class="btn" data-edit="redo"
									title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
							</div>
						</div>
		
						<div id="editor" class="editor-wrapper" style="text-align:left"></div>
						<input type="hidden" id="email_content" name="content"/>
						<textarea name="descr" id="descr" style="display: none;"></textarea>
				</div>
			
			</form>
		</div>

		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"
				id="professional_email_close">取消</button>
			<button type="button" id="professional_email_save" class="btn btn-primary">发送</button>
		</div>
	</div>
</div>