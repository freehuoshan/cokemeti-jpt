<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false" %>
<%@include file="../common/top.jsp" %>
<html>
	<head>
    <!-- Bootstrap -->
    <link href="<%=basePath%>/static/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=basePath%>/static/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=basePath%>/static/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%=basePath%>/static/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<%=basePath%>/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<%=basePath%>/static/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<%=basePath%>/static/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=basePath%>/static/css/custom.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div style="position:absolute;font-size:20px;bottom:0;left:15px">六度智囊</div>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
			<%@include file="../common/left.jsp" %>
            <!-- sidebar menu -->
			<%@include file="menu.jsp" %>
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
		<%@include file="../common/head.jsp" %>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        	
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>项目列表</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li ><a style="color:#5A738E;" class="" href="<%=basePath%>/analyst/project/add"><i class="fa fa-plus"></i> 新建项目</a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable-projects" class="table table-hover table-striped table-bordered ">
                      <thead>
                        <tr>
                          <th>项目名称</th>
                          <th>客户</th>
                          <th>项目状态</th>
                          <th>开始时间</th>
                          <th>结束时间</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
        
        </div>
        
        <!-- footer -->
		<%@include file="../common/footer.jsp" %>
    </div>

    <!-- jQuery -->
    <script src="<%=basePath%>/static/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=basePath%>/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<%=basePath%>/static/js/custom.min.js"></script>
    
    
    <!-- Datatables -->
    <script src="<%=basePath%>/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<%=basePath%>/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

    <script src="<%=basePath%>/static/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<%=basePath%>/static/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
    $(document).ready(function() {
    	if ($("#datatable-projects").length) {
            var table = $("#datatable-projects").DataTable({
	              "responsive": true,
	              "processing": true,
	              "serverSide": true,
	              "stateSave": true,
	              "ajax":{
	              	url:'<%=basePath%>/analyst/project/list',
	              	type:'POST',
	              },
	              "columns": [
	                          { "data": "name" },
	                          { "data": "customerId" },
	                          { "data": "status" },
	                          { "data": "startDate" },
	                          { "data": "endDate" }
	                      ],
	         	  "rowId":"id",
	              "searching":true,
	              "searchDelay": -1,
	              "info":false,
	          	  "ordering":false
        	});
            
            $('#datatable-projects tbody').on( 'dblclick', 'tr', function () {
            	var data = table.row( this ).data();
        		location.href = '<%=basePath%>/analyst/project/'+data.id;
       	 	});
    	}
    });
    </script>
  </body>
</html>