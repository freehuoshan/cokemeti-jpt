<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					QA报告 
					<small style="color: red">
						<button type="button" class="btn btn-success"  data-toggle="modal" id="upload_report_button">上传报告</button>
					</small>
				</h2>
				<div id="files">
				</div>
				
				<div class="modal fade bs-modal-report" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close" id="report_close">
											<span aria-hidden="true">×</span>
										</button>
										<h4 class="modal-title" id="myModalLabel3">
					                        	样本报告:
					                        	<input type="radio" class="flat" name="report_type" id="report_type_s" value="1" checked="checked" required /> 
					                        	&nbsp;&nbsp;&nbsp;
					                        	完整报告:
					                        	<input type="radio" class="flat" name="report_type" id="report_type_l" value="2" />
										</h4>
									</div>
				                        
									<div class="modal-body">
									<!-- <div id="mydropzone"></div> -->
 									<!--<form action="" method="post" enctype="multipart/form-data">
											<input type="file" name="file" />
										</form> -->
				                        <div class="">
				                          <select class="form-control" id="professionals_selector">
				                          </select>
				                        </div>
 										<div class="dropzone" id="myDropzone">
											<div class="am-text-success dz-message">
												将文件拖拽到此处<br>或点此打开文件管理器选择文件
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal" id="report_close">取消</button>
										<button type="button" id="report_save" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br/>
				<table id="datatable-report" class="table table-striped table-bordered">
<!--                    <thead>
                        <tr>
                          <th>时间</th>
                          <th>创建者</th>
                          <th>需求描述</th>
                        </tr>
                   </thead> -->
                </table>
			</div>
		</div>
	</div>
</div>