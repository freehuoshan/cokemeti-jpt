<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <style>
    .stepContainer{ height:auto!important;}
    </style>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="col-md-12 col-sm-12 col-xs-12">
					专家列表 
					<small style="color: red">
						<button type="button" class="btn btn-success"  data-toggle="modal" id="addProfessionType">新增分类</button>
						<button type="button" class="btn btn-success"  data-toggle="modal" id="addprofession">添加专家</button>
						<button type="button" id="sendemail-button" class="btn btn-dark" data-toggle="modal" data-target=".lg-professional_email" style="display:none;">发送专家列表</button>
						<button type="button" id="sendProfessionList-button" class="btn btn-dark" onclick="sendCustomerEmail()">发送专家列表</button>
						<!-- <button type="button" id="sendProfessionInfo-button" class="btn btn-dark" onclick="sendProfessionInfo()">发送专家详情</button> -->
					</small>
				</h2>
					<div class="modal fade bs-example-modal-lg lg-professional" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
										<c:choose>
											<c:when test="${type==1}">
												<h4 class="modal-title" id="myModalLabel1">编辑专家</h4>
											</c:when>
											<c:otherwise>
											<h4 class="modal-title" id="myModalLabel2">添加专家</h4>
											</c:otherwise>
										</c:choose>
										
									</div>
									
									<div class="modal-body">
									<form class="form-horizontal form-label-left" id="project_professional_form" action="<%=basePath%>/analyst/project/professional/add" method="POST">
			                           <input type="hidden" name="profession_project_id" id="profession_project_id"/>
			                          <input type="hidden" name="projectId" id="p_project_id"/>
			                          <div class="form-group">
			                            <label class="control-label col-md-3 col-sm-3" for="first-name">专家编号
			                            </label>
			                            <div class="col-md-3 col-sm-6">
			                              <input type="text" id="professional_id" value="${projectProfession.professionalId}" name="professionalId" required="required" class="form-control col-md-7 col-xs-12">
			                            </div>
			                            <label class=" col-md-3 col-sm-6 col-xs-12 ">
											<button type="button" class="btn btn-success" id="professional_search">确定</button>
										</label>
			                          </div>
			                          
			                          <div class="form-group">
			                            <label class="control-label col-md-3 col-sm-3" for="name">姓名
			                            </label>
			                            <div class="col-md-5 col-sm-6">
			                              <input type="text" id="professional_name" name="name" required="required" disabled="disabled" class="form-control col-md-7 col-xs-12">
			                            </div>
			                          </div>
			                          <div class="form-group">
			                            <label class="control-label col-md-3 col-sm-3" for="name">邮箱
			                            </label>
			                            <div class="col-md-5 col-sm-6">
			                              <input type="text" id="professional_email" name="email" required="required" disabled="disabled" class="form-control col-md-7 col-xs-12">
			                            </div>
			                          </div>			                          
			                          <div class="form-group">
			                            <label for="title" class="control-label col-md-3 col-sm-3">目前职位</label>
			                            <div class="col-md-5 col-sm-6">
			                              <input id="professional_title" class="form-control col-md-7 col-xs-12" type="text" disabled="disabled" />
			                            </div>
			                          </div>
			                          <div class="form-group">
			                            <label for="company" class="control-label col-md-3 col-sm-3">目前公司</label>
			                            <div class="col-md-5 col-sm-6">
			                              <input id="professional_company" class="form-control col-md-7 col-xs-12" type="text"  disabled="disabled" />
			                            </div>
			                          </div>
			                          <div class="form-group">
			                            <label for="project_exp" class="control-label col-md-3 col-sm-3">相关经历</label>
			                            <div class="col-md-5 col-sm-6">
			                              <textarea oncontextmenu="window.event.returnValue=false" oncopy="return false;" oncut="return false;"  id="professional_exp" disabled="disabled" class="form-control col-md-7 col-xs-12"  name="exp" rows="5"></textarea>
			                            </div>
			                          </div>
			                          <div class="form-group">
			                            <label for="professional_price" class="control-label col-md-3 col-sm-3">专家报酬</label>
			                            <div class="col-md-5 col-sm-6">
			                              <input id="professional_price" class="form-control col-md-7 col-xs-12" type="text" name="professionalPrice" />
			                            </div>
			                          </div>
			                            <div class="form-group">
			                            <label class="control-label col-md-3 col-sm-3" for="name">咨询分类
			                            </label>
			                            <div class="col-md-5 col-sm-6">
			                            	<select id="profession_type" name="professionType" required="required" class="form-control col-md-7 col-xs-12">
			                            		
			                            	</select>
			                            </div>
			                          </div>
			                          <h4 class="modal-title" id="myModalLabel2">项目相关经历</h4>
			                          <div class="form-group">
			                            <label for="project_title" class="control-label col-md-3 col-sm-3">职位</label>
			                            <div class="col-md-5 col-sm-6">
			                              <input id="project_title" class="form-control col-md-7 col-xs-12" type="text" name="title" />
			                            </div>
			                          </div>
			                          <div class="form-group">
			                            <label for="project_company" class="control-label col-md-3 col-sm-3">公司</label>
			                            <div class="col-md-5 col-sm-6">
			                              <input id="project_company" class="form-control col-md-7 col-xs-12" type="text" name="company" />
			                            </div>
			                          </div>			                        
			                          <div class="form-group">
			                            <label for="project_exp" class="control-label col-md-3 col-sm-3">相关经历</label>
			                            <div class="col-md-5 col-sm-6">
			                              <textarea id="project_exp" class="form-control col-md-7 col-xs-12"  name="exp" rows="5"></textarea>
			                            </div>
			                          </div>
			                          
			                          <h4 class="modal-title">其他信息</h4>
			                          <div class="form-group">
			                            <label for="project_priceTimes" class="control-label col-md-3 col-sm-3">客户付费倍数</label>
			                            <div class="col-md-5 col-sm-6">
			                              <input id="project_priceTimes" value="1" class="form-control col-md-7 col-xs-12" type="text" name="priceTimes" />
			                            </div>
			                          </div>	
			                          <div class="form-group">
			                            <label for="project_company" class="control-label col-md-3 col-sm-3">是否选中</label>
										<div class="col-md-5 col-sm-6">
			                              <div id="gender" class="btn-group" data-toggle="buttons">
			                                <label class="btn btn-default active" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
			                                  <input type="radio" name="customerStatus" value="-2" checked="checked"> &nbsp;否  &nbsp;
			                                </label>
			                                <label class="btn btn-default" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
			                                  <input type="radio" name="customerStatus" value="21"> 是
			                                </label>
			                              </div>
			                            </div>
			                          </div>
			                          
			                          <div class="form-group">
			                            <label for="report_price" class="control-label col-md-3 col-sm-3">报告价格</label>
			                            <div class="col-md-5 col-sm-6">
			                              <input id="report_price" class="form-control col-md-7 col-xs-12" type="text" name="reportPrice" />
			                            </div>
			                          </div>       
			                        </form>
									</div>
									
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal" id="professional_close">取消</button>
										<button type="button" id="professional_save" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
					<div class="modal fade bs-modal-add-professiontype" id="professiontype_add" tabindex="-1"
							role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close" id="professiontype_add_close">
											<span aria-hidden="true">×</span>
										</button>
									<h4 class="modal-title" id="professionadd_title"> 新增分类 </h4>
									</div>
									<div class="modal-body">
									<form id="profession_type_addform" action="<%=basePath%>/analyst/project/profession_type/add">
										<input type="text"  placeholder="分类名称" name="type" id="professiontype_add_textfield"/>
									</form>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">取消</button>
										<button type="button" id="professiontype_save" class="btn btn-primary">保存</button>
									</div>
								</div>
							</div>
					</div>
					
					<div class="modal fade bs-example-modal-lg lg-professional_email" tabindex="-1"role="dialog" aria-hidden="true">
						<%@include file="project_add_professional_email.jsp" %>
					</div>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix">
                           
				</div>
			</div>
			<div class="x_content">
				<br />
				<table id="datatable-professional" class="table table-striped table-bordered">
                   <thead>
                        <tr>
                         <!-- <th><input type="checkbox" id="checkAll" name="checkAll" /></th> -->
                         <th>选择专家</th>
                          <th>专家编号</th>
                          <th>专家姓名</th>
                          <th class="col-md-2 col-sm-2 col-xs-3 ">职位</th>
                          <th class="col-md-2 col-sm-2 col-xs-3 ">公司</th>
                          <th>相关背景</th>
                          <th>专家报酬</th>
                          <th>报告价格</th>
                          <th>客户阶段</th>
                          <th>专家阶段</th>
                          <th>邮件操作</th>
                          <th>删除操作</th>
                          <th>编辑操作</th>
                        </tr>
                   </thead>
                   <tbody id="datatable-professional-tbody"></tbody>
                </table>
			</div>
		</div>
	</div>
</div>
