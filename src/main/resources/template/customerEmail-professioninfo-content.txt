<br style="font-family:Helvetica,arial,sans-serif;font-size:13px;text-align:justify">
	<table cellpadding="0" cellspacing="0" class="m_-76039934011966351cke_show_border" style="border:none;color:gray;font-family:Helvetica,arial,sans-serif;font-size:13px;text-align:justify">
		<tbody>
			<tr>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:22px">	<b>
					专家编号: $pid$--现任 $company$ $title$
				   </b>
				</td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" class="m_-76039934011966351cke_show_border" style="border:none;color:gray;font-family:Helvetica,arial,sans-serif;font-size:13px;text-align:justify">
		<tbody>
			<tr>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:22px">	Name:
				</td>
				<td width="10" style="border:1px dotted gray">
				   <br>
				</td>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:22px">	$professionName$
				</td>
			</tr>
			<tr>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:22px">	Email:
				</td>
				<td width="10" style="border:1px dotted gray">
					<br>
				</td>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:22px">	<a href="mailto:$professionEmail$" target="_blank">$professionEmail$</a>
				</td>
			</tr>
			<tr>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:22px">	Phone:
				</td>
				<td width="10" style="border:1px dotted gray">
					<br>
				</td>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:22px">	
					$professionPhone$
				</td>
			</tr>
		</tbody>
	</table>
	<br style="font-family:Helvetica,arial,sans-serif;font-size:13px;text-align:justify">
	<table cellpadding="0" cellspacing="0" class="m_-76039934011966351cke_show_border" style="border:none;color:gray;font-family:Helvetica,arial,sans-serif;font-size:13px;text-align:justify">
		<tbody>
			<tr>
				<td class="m_-76039934011966351fontincrease" style="border:1px dotted gray;line-height:16px">
					 $re$
				</td>
			</tr>
		</tbody>
	</table>

