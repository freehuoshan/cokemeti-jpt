/**
 * author: wangxj
 * create time: 下午2:20:37
 */
package com.cokemeti.jpt.service.professional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cokemeti.jpt.domain.professional.ProfessionType;
import com.cokemeti.jpt.domain.project.ProjectProfessional;
import com.cokemeti.jpt.idao.professional.ProfessionTypeMapper;
import com.cokemeti.jpt.idao.project.ProjectProfessionalMapper;
import com.cokemeti.jpt.util.DateUtil;

/**
 * @author huoshan
 * created by 2017年4月15日 下午2:20:37
 * 
 */
@Service
public class ProfessionTypeService {
	
	@Autowired
	private ProfessionTypeMapper professionTypeMapper;
	
	@Autowired
	private ProjectProfessionalMapper projectProfessionalMapper;

	/**
	 * 校验专家类型是否重复
	 * @param professionType
	 * @return true: 重复 false :不重复
	 */
	public Boolean checkTypeRepeat(ProfessionType professionType) {
		Integer id = professionType.getId();
		professionType.setId(null);
		List<ProfessionType> conditionList = professionTypeMapper.selectByCondition(professionType);
		//查询结果为0
		if(conditionList.size() == 0){
			return false;
		}//当编辑时,如果查询结果只有一个而且id与当前被编辑的相同则为同一个,不重复
		else if(conditionList.size() == 1 && conditionList.get(0).getId() == id){
			return false;
		}
		return true;
	}

	/**
	 * @param professionType
	 * @return
	 */
	public Boolean addProfessionType(ProfessionType professionType) {
		professionType.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
		Integer count = professionTypeMapper.insert(professionType);
		return count > 0;
	}

	/**专家分类列表
	 * @return
	 */
	public List<ProfessionType> getProfessionList(ProfessionType professionType) {
		return professionTypeMapper.selectByCondition(professionType);
	}

	/**
	 * @param professionType
	 * @return
	 */
	public Boolean updateProfessionType(ProfessionType professionType) {
		professionType.setUpdateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
		Integer count = professionTypeMapper.updateByUuid(professionType);
		return count > 0;
	}

	/**
	 * @param projectProfession
	 * @return
	 */
	public Boolean deleteProfessionByType(ProjectProfessional projectProfession) {
		int count = projectProfessionalMapper.deleteProfessionByTypeAndProject(projectProfession);
		ProfessionType professiontype = new ProfessionType();
		professiontype.setId(projectProfession.getProfessionType());
		int countType = professionTypeMapper.deleteById(professiontype);
		return true;
	}

}
