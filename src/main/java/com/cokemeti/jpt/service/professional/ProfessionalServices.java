package com.cokemeti.jpt.service.professional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.resource.spi.work.WorkException;

import org.HdrHistogram.SingleWriterDoubleRecorder;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.Histogram;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.common.ExcelVo;
import com.cokemeti.jpt.domain.professional.Professional;
import com.cokemeti.jpt.domain.professional.ProfessionalWithBLOBs;
import com.cokemeti.jpt.domain.professional.ProfessionalWork;
import com.cokemeti.jpt.domain.project.ProjectInfoVo;
import com.cokemeti.jpt.domain.project.ProjectProfessional;
import com.cokemeti.jpt.idao.professional.ProfessionalMapper;
import com.cokemeti.jpt.idao.professional.ProfessionalWorkMapper;
import com.cokemeti.jpt.idao.project.ProjectMapper;
import com.cokemeti.jpt.idao.project.ProjectProfessionalMapper;
import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.DataTablesVoUtil;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.ExcelUtil;
import com.cokemeti.jpt.util.UUIDUtil;
import com.cokemeti.jpt.util.es.ClientF;
import com.cokemeti.jpt.util.es.ESClient;

@Service
public class ProfessionalServices {

	@Autowired
	private ProfessionalMapper professionalMapper;
	@Autowired
	private ProjectProfessionalMapper ppMapper;
	@Autowired
	private ProjectMapper projectMapper;
	@Autowired
	private ProfessionalWorkMapper professionWorkMapper;
	
	public ProfessionalWithBLOBs getProfessionalById(int id){
		return professionalMapper.selectByPrimaryKey(id);
	}

	public DataTablesVo getProjectsList(int id){
		List<ProjectProfessional> list =  ppMapper.getAllPpByProfessionalId(id);
		
		return  DataTablesVoUtil.getDataTablesVo(1, list.size(), list.size(), list);
	}
	
	public void updateProfessionalInfo(ProfessionalWithBLOBs professional) throws Exception{
		if (professional != null && professional.getId() != null) {
			int i = professionalMapper.updateByPrimaryKeySelective(professional);
			if(i > 0){
				try{
					ESClient client = ClientF.getInstance();
					client.updateDoc(Constants.ES_DB, Constants.ES_TABLE, String.valueOf(professional.getId()),JSONObject.parseObject(JSON.toJSONString(professional)));
					return ;
				}catch(Exception e){
					throw e;
				}
			}
		}
	}
	
	public Integer insertProfessional(ProfessionalWithBLOBs pro) throws Exception{
		if (pro != null){
			int i = professionalMapper.insertSelective(pro);
			if (i > 0) {
				ESClient client = ClientF.getInstance();
				client.addDoc(Constants.ES_DB, Constants.ES_TABLE, String.valueOf(pro.getId()),
						JSONObject.parseObject(JSON.toJSONString(pro)));
			}
			return pro.getId();
		}
		else{
			return null;
		}
	}
	
	public Map<String, String> uploadProfessionals(String filename, String path,String url)throws Exception{
		
			Map<String,String> resMap = new HashMap<>(); 
			if (!StringUtils.isEmpty(filename)) {
				//创建重复,失败专家excel
				String ext = "xls"; // 后缀
				String failName = UUIDUtil.newGUID()+ "."+ext;
				String repeatName = UUIDUtil.newGUID()+ "."+ext;
				String failDir = path + "/" + failName;
				String repeatDir = path + "/" + repeatName;
				String repeatSheet = "重复专家";
				String failShee = "失败专家";
				String[] titleRow = { "专家名字" , "职位" , "公司" , "邮件" , "电话" , "费用","工作经历" , "专家简介" };
				List<Map> repeatData = new ArrayList<>();
				List<Map> failData = new ArrayList<>();
				ExcelUtil.createExcel(repeatDir,repeatSheet , titleRow);
				ExcelUtil.createExcel(failDir, failShee, titleRow);
				List<ExcelVo> list = new ArrayList<>();
				if(filename.endsWith("csv")){
					list = ExcelUtil.readCsv(path+"/"+filename);
				}else{
					list = ExcelUtil.readXml(path+"/"+filename);
				}
				
				for (ExcelVo vo : list) {
					if (!StringUtils.isEmpty(vo.getName())) {
						ProfessionalWithBLOBs pb = new ProfessionalWithBLOBs();
						pb.setPhone(vo.getTel());
						pb.setEmail(vo.getEmail());
//						Map<String, Object> exist = isExist(pb);
//						Object isExist = exist.get("isExist");
						String workexps = vo.getWorkexp();
//						if((boolean)isExist){
//							Map<String,Object> repeatMap = new HashMap<>();
//							repeatMap.put("专家名字", vo.getName());
//							repeatMap.put("职位", vo.getTitle());
//							repeatMap.put("公司", vo.getCompany());
//							repeatMap.put("邮件", vo.getEmail());
//							repeatMap.put("电话", vo.getTel());
//							repeatMap.put("费用", vo.getPrice());
//							repeatMap.put("工作经历", workexps);
//							repeatMap.put("专家简介", vo.getDesc());
//							repeatData.add(repeatMap);
//							continue;
//						}
						pb.setName(vo.getName());
						pb.setCompany(vo.getCompany());
						pb.setTitle(vo.getTitle());
						pb.setWorkExp(vo.getDesc());
						pb.setPrice(Double.valueOf("".equals(vo.getPrice()) || vo.getPrice() == null ? "0" : vo.getPrice()));
						pb.setSource("1");
						try {
							Integer professioinId = insertProfessional(pb);
							if(workexps != null && !"".equals(workexps)){
								String[] workexpArr = workexps.split("\n");
								for (String workexp : workexpArr) {
									ProfessionalWork professionWork = new ProfessionalWork();
									professionWork.setProfessionalId(professioinId);
									String[] workExpSingArr = workexp.split("  ");
									if(workExpSingArr != null && workExpSingArr.length > 0){
										String[] timeArr = workExpSingArr[0].split("-");
										if(timeArr != null && timeArr.length > 0){
											professionWork.setStartDate(timeArr[0]);
										}
										if(timeArr.length == 2){
											professionWork.setEndDate(timeArr[1]);
										}
										if(workExpSingArr.length > 1){
											professionWork.setCompanyName(workExpSingArr[1]);
										}
										if(workExpSingArr.length > 2){
											professionWork.setTitle(workExpSingArr[2]);
										}
									}
									professionWork.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
									professionWorkMapper.insertSelective(professionWork);
								}
							}
						} catch (Exception e) {
							Map<String,Object> failMap = new HashMap<>();
							failMap.put("专家名字", vo.getName());
							failMap.put("职位", vo.getTitle());
							failMap.put("公司", vo.getCompany());
							failMap.put("邮件", vo.getEmail());
							failMap.put("电话", vo.getTel());
							failMap.put("费用", vo.getPrice());
							failMap.put("工作经历", workexps);
							failMap.put("专家简介", vo.getDesc());
							failData.add(failMap);
							e.printStackTrace();
							continue;
						}
					}
				}
				if(repeatData.size() > 0){
					ExcelUtil.writeToExcel(repeatDir, repeatSheet, repeatData);
					resMap.put("repeat", url+repeatName);
				}
				if(failData.size() > 0){
					ExcelUtil.writeToExcel(failDir, failShee, failData);
					resMap.put("fail", url+failName);
				}
			}
			return resMap;
	}
	
	public DataTablesVo MoHuSearchProfessional(String keyword, int from, int size){
		String[] keywords = keyword.split("\\s+");
		Map<String, Object> params = new HashMap<>();
		List<String> keyList = new ArrayList<>();
		for (String singleWord : keywords) {
			keyList.add(singleWord);
		}
		params.put("list", keyList);
		params.put("start", from);
		params.put("limit", size);
		int count = professionalMapper.mohuCount(params);
		List<ProfessionalWithBLOBs> professions = professionalMapper.mohuQuery(params);
		for (ProfessionalWithBLOBs professionalWithBLOBs : professions) {
			for (String singleWord : keywords) {
				String company = "<mark>" + singleWord + "</mark>";
				company =professionalWithBLOBs.getCompany().replace(singleWord, company);
				professionalWithBLOBs.setCompany(company);
				String name = "<mark>" + singleWord + "</mark>";
				name = professionalWithBLOBs.getName().replace(singleWord, name);
				professionalWithBLOBs.setName(name);
				String title = "<mark>" + singleWord + "</mark>";
				title = professionalWithBLOBs.getTitle().replace(singleWord, title);
				professionalWithBLOBs.setTitle(title);
				String worExp = "<mark>" + singleWord + "</mark>";
				worExp = professionalWithBLOBs.getWorkExp().replace(singleWord, worExp);
				worExp = worExp.replace("\t", "\\t");
				professionalWithBLOBs.setWorkExp(worExp);
				
				String email = "<mark>" + singleWord + "</mark>";
				email = professionalWithBLOBs.getEmail().replace(singleWord, email);
				email = email.replace("\t", "\\t");
				professionalWithBLOBs.setEmail(email);
				
				String phone = "<mark>" + singleWord + "</mark>";
				phone = professionalWithBLOBs.getPhone().replace(singleWord, phone);
				phone = phone.replace("\t", "\\t");
				professionalWithBLOBs.setPhone(phone);
			}
		}
		
		return DataTablesVoUtil.getDataTablesVo(1, count, 1, professions);
	}
	
	public DataTablesVo searchProfessional(String keyword, int from, int size){
		ESClient client = ClientF.getInstance();
		SearchHits hits = client.search(keyword, from, size);
		List<String> list = new ArrayList<>();
		List<ProfessionalWithBLOBs> listProfessions = new ArrayList<>();
		int total_hits= 0;
		if (hits != null) {
			total_hits = (int)hits.getTotalHits();
			for (SearchHit hit : hits) {
				Map<String, HighlightField> highlightFields = hit.getHighlightFields();
				HighlightField nameField = highlightFields.get("name");
				HighlightField companyField = highlightFields.get("company");
				HighlightField title = highlightFields.get("title");
				HighlightField workExp = highlightFields.get("workExp");
				
				System.out.println("name:"+ nameField);
				System.out.println("company:" + companyField);
				System.out.println("title:"+ title);
				System.out.println("workExp:" + workExp);
				
				ProfessionalWithBLOBs profession = JSONObject.parseObject(hit.getSourceAsString(), ProfessionalWithBLOBs.class);
				System.out.println("pro:" + profession);
				if(nameField != null){
					String name = "";
					for(Text text : nameField.fragments()){
						name = name.concat(text.string());
					}
					profession.setName(name);
				}
				System.out.println("profession:" + profession);
				if(companyField != null){
					String company = "";
					for(Text text : companyField.fragments()){
						company = company.concat(text.string());
					}
					profession.setCompany(company);
				}
				System.out.println("profession:" + profession);
				if(title != null){
					String t = "";
					for(Text text : title.fragments()){
						t = t.concat(text.string());
					}
					profession.setTitle(t);
				}
				System.out.println("profession:" + profession);
				if(workExp != null){
					String t = "";
					for(Text text : workExp.fragments()){
						t = t.concat(text.string());
					}
					profession.setWorkExp(t);
				}
				System.out.println("profession:" + profession);
				listProfessions.add(profession);
//				list.add(hit.getSourceAsString());
			}
		}
		return DataTablesVoUtil.getDataTablesVo(1, total_hits, 1, listProfessions);
	}
	
	
	public void test(){
		List<Professional> list = professionalMapper.getProfessionals();
		if (list != null) {
			System.out.println("需同步"+list.size()+"条数据");
			for (Professional p : list) {
				System.out.println(JSON.toJSONString(p));
				ESClient client = ClientF.getInstance();
				client.addDoc(Constants.ES_DB, Constants.ES_TABLE, String.valueOf(p.getId()),
						JSONObject.parseObject(JSON.toJSONString(p)));
				
			}
		}
	}

	/**
	 * @param professionId
	 * @return
	 */
	public Boolean deleteById(String professionId,String projectId) {
		
		Integer id = Integer.valueOf(professionId);
		Integer projId = Integer.valueOf(projectId);
		Map<String , Integer> paramMap = new HashMap<>();
		paramMap.put("projectId", projId);
		paramMap.put("professionId", id);
		return ppMapper.deleteByProfessProjectId(paramMap) >= 0;
	}

	/**
	 * @param projectId
	 * @param professionId
	 * @return
	 */
	public ProjectProfessional getProfessionInfoByProjectIdAndProfessionId(String projectId, String professionId) {
	
		return ppMapper.getPPByProjectIdAndProfessionalId(Integer.valueOf(projectId), Integer.valueOf(professionId));
	}

	/**
	 * @param professionBak
	 * @return
	 */
	public Map<String,Object> isExist(ProfessionalWithBLOBs profession) {
		ProfessionalWithBLOBs professionPhone = new ProfessionalWithBLOBs();
		professionPhone.setPhone(profession.getPhone());
		ProfessionalWithBLOBs professionEmail = new ProfessionalWithBLOBs();
		professionEmail.setEmail(profession.getEmail());
		List<Professional> listProfessionByPhone = professionalMapper.selectByCondition(professionPhone);
		List<Professional> listProfessionByEmail = professionalMapper.selectByCondition(professionEmail);
		List<Professional> allList = professionalMapper.selectByCondition(new ProfessionalWithBLOBs());
		Integer id = profession.getId();
		Map<String,Object> resMap = new HashMap<>();
		
		if(listProfessionByPhone.size() == 0 && listProfessionByEmail.size() == 0){
			resMap.put("isExist", false);
			return resMap;
		}
		else if(listProfessionByPhone.size() == 1 && (listProfessionByPhone.get(0).getId() == profession.getId()) && listProfessionByEmail.size() == 1 && (listProfessionByEmail.get(0).getId() == id)){
			resMap.put("isExist", false);
			return resMap;
		}
		else if((listProfessionByPhone.size() == allList.size() || listProfessionByPhone.size() == 0) && listProfessionByEmail.size() == 0 ){
			resMap.put("isExist", false);
			return resMap;
		}
		else if((listProfessionByEmail.size() == allList.size() || listProfessionByEmail.size() == 0) && listProfessionByPhone.size() == 0){
			resMap.put("isExist", false);
			return resMap;
		}
		else if(listProfessionByEmail.size() == allList.size() && listProfessionByPhone.size() == allList.size()){
			resMap.put("isExist", false);
			return resMap;
		}
		else {
			resMap.put("isExist", true);
			if(listProfessionByPhone.size() > 0 && listProfessionByPhone.size() < allList.size()){
				Integer phoneId = listProfessionByPhone.get(0).getId();
				resMap.put("id", phoneId);
				return resMap;
			}else if(listProfessionByEmail.size() > 0 && listProfessionByEmail.size() < allList.size()){
				Integer emailId = listProfessionByEmail.get(0).getId();
				resMap.put("id", emailId);
				return resMap;
			}
			return resMap;
		}
	}
}
