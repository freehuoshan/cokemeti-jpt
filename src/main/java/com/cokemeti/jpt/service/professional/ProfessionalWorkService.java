/**
 * 
 */
package com.cokemeti.jpt.service.professional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cokemeti.jpt.domain.professional.ProfessionalWork;
import com.cokemeti.jpt.idao.professional.ProfessionalWorkMapper;

/**
 * @author Administrator
 *
 */
@Service
public class ProfessionalWorkService {

	@Autowired
	private ProfessionalWorkMapper mapper;
	
	public List<ProfessionalWork> getProfessionalWorks(int professional_id){
		return mapper.getProfessionalWorksByProfessionalId(professional_id);
	}
	
	public int addProfessionWorks(ProfessionalWork professionWork){
		return mapper.insertSelective(professionWork);
	}
	
	
	public int updateProfessionWork(ProfessionalWork professionWork){
		return mapper.updateByPrimaryKeySelective(professionWork);
	}
	
	public ProfessionalWork getProfessionWork(Integer professionWorkId){
		return mapper.selectByPrimaryKey(professionWorkId);
	}
	
	public int deleteById(Integer professionWorkId){
		return mapper.deleteByPrimaryKey(professionWorkId);
	}
}
