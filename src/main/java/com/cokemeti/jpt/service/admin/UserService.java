/**
 * 
 */
package com.cokemeti.jpt.service.admin;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cokemeti.jpt.domain.admin.User;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.project.Project;
import com.cokemeti.jpt.idao.admin.UserLoginMapper;
import com.cokemeti.jpt.idao.admin.UserMapper;
import com.cokemeti.jpt.util.DataTablesVoUtil;
import com.cokemeti.jpt.util.EncryptUtil;

/**
 * @author Administrator
 *
 */
@Service("userService")
public class UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserLoginMapper userLoginMapper;
	
	public User getUserByUsername(String email){
		return userMapper.selectByEmail(email);
	}
	
	public User getUserById(Integer id){
		return userMapper.selectByPrimaryKey(id);
	}

	/**
	 * @param id
	 * @param search
	 * @param start
	 * @param length
	 * @param draw
	 * @return
	 */
	public DataTablesVo getUsers(String id, String search, String start, String length, int draw) {
		List<User> list =  userMapper.getUsers(id==null?null:Integer.valueOf(id),search,Integer.valueOf(start), Integer.valueOf(length));
		int count = userMapper.getUserTotal(id==null?null:Integer.valueOf(id),search);
		List<User> decodedUserList = new ArrayList<>();
		for(User user : list){
			user.setPassword(EncryptUtil.PBEDecrypt(user.getPassword()));
		}
		return DataTablesVoUtil.getDataTablesVo(draw, count, count, list);
	}

	/**
	 * @param user
	 * @return
	 */
	public Integer addUser(User user) {
		return userMapper.insert(user);
	}

	/**
	 * @param user
	 * @return
	 */
	public Integer updateUser(User user) {
		
		return userMapper.updateByPrimaryKeySelective(user);
	}

	/**
	 * @param user
	 * @return
	 */
	public Boolean checkRepeat(User user) {
		User existUser = userMapper.selectByEmail(user.getEmail());
		if(existUser == null){
			return false;
		}
		else if(existUser.getId() == user.getId()){
			return false;
		}
		else{
			return true;
		}
	}

	/**
	 * @param user
	 * @return
	 */
	public Integer delete(User user) {
		int count = userMapper.deleteByPrimaryKey(user.getId());
		return count;
	}
	
}
