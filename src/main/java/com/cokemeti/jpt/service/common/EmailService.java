/**
 * 
 */
package com.cokemeti.jpt.service.common;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cokemeti.jpt.domain.common.Email;
import com.cokemeti.jpt.domain.project.ProjectProfessional;
import com.cokemeti.jpt.idao.common.EmailMapper;
import com.cokemeti.jpt.idao.project.ProjectProfessionalMapper;
import com.cokemeti.jpt.service.project.ProjectService;
import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.MailUtils;

/**
 * @author Administrator
 *
 */
@Service
public class EmailService {

	@Value("${email.account}")
	private String account;

	@Value("${email.password}")
	private String password;
	
	@Value("${email.smtphost}")
	private String smtphost;
	@Value("${email.smtpport}")
	private String smtpPort;
	
	@Autowired
	private EmailMapper emailMapper;
	@Autowired
	private ProjectProfessionalMapper ppMapper;
	@Autowired
	private ProjectService ps;
	
	/**
	 * 
	 * @param email
	 * @param userid
	 * @param type 1:专家邮件   2：客户邮件
	 */
	public void sendEmail(Email email , int userid, int type,int operatype ,String projectId, String ppId){
		
		if (email != null) {
			List<String> accounts = Arrays.asList(email.getReceiver().split(";"));
			List<String> ccs =null;
			if(!StringUtils.isEmpty(email.getCc())){
				ccs = Arrays.asList(email.getCc().split(";"));				
			}
			
			if(accounts != null){
				email.setSendUser(userid);
				email.setSendTime(DateUtil.getFormatDate(System.currentTimeMillis()));
				try {
					
					MailUtils.sendEmail(accounts, ccs, null, 
							"六度智囊", email.getSubject(), email.getContent(), null, 
							smtphost, account, password,smtpPort,false
					);
					email.setStatus(1);
					
					switch (type) {
						case 1:
							//专家邮件
							ps.updatePpStatus(Integer.valueOf(ppId), operatype);
							break;
						case 2:
							//List<ProjectProfessional> list = ppMapper.getAllPpByProjectIdWithStatus(Integer.valueOf(projectId), Constants.PROJECT_PROFESSINOAL_STATUS_AGREE);
							List<ProjectProfessional> list = ppMapper.getAllPpByProjectId(Integer.valueOf(projectId));
							for (ProjectProfessional pp : list) {
								ps.updatePpCustomerStatus(pp.getId(),operatype);
							}
							break;
						default:
							break;
					}
				} catch (Exception e) {
					email.setStatus(0);
				}
				emailMapper.insert(email);
			}
		}
	}
}
