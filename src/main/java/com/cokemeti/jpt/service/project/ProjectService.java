/**
 * 
 */
package com.cokemeti.jpt.service.project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.javassist.bytecode.LineNumberAttribute.Pc;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.common.Email;
import com.cokemeti.jpt.domain.professional.ProfessionType;
import com.cokemeti.jpt.domain.professional.Professional;
import com.cokemeti.jpt.domain.professional.ProfessionalWithBLOBs;
import com.cokemeti.jpt.domain.professional.ProfessionalWork;
import com.cokemeti.jpt.domain.project.Project;
import com.cokemeti.jpt.domain.project.ProjectInfo;
import com.cokemeti.jpt.domain.project.ProjectInfoVo;
import com.cokemeti.jpt.domain.project.ProjectProfessional;
import com.cokemeti.jpt.domain.project.ProjectReport;
import com.cokemeti.jpt.domain.project.ProjectReportVo;
import com.cokemeti.jpt.domain.project.ProjectVo;
import com.cokemeti.jpt.domain.sales.Customer;
import com.cokemeti.jpt.idao.professional.ProfessionTypeMapper;
import com.cokemeti.jpt.idao.professional.ProfessionalMapper;
import com.cokemeti.jpt.idao.professional.ProfessionalWorkMapper;
import com.cokemeti.jpt.idao.project.ProjectInfoMapper;
import com.cokemeti.jpt.idao.project.ProjectMapper;
import com.cokemeti.jpt.idao.project.ProjectProfessionalMapper;
import com.cokemeti.jpt.idao.project.ProjectReportMapper;
import com.cokemeti.jpt.idao.sales.CustomerMapper;
import com.cokemeti.jpt.service.professional.ProfessionalWorkService;
import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.DataTablesVoUtil;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.template.EmailContentTemplateUtil;
import com.cokemeti.jpt.util.EncryptUtil;

/**
 * @author Administrator
 *
 */
@Service
public class ProjectService {

	private Logger log = Logger.getLogger(ProjectService.class);
	@Autowired
	private ProjectMapper projectMapper;
	@Autowired
	private ProjectProfessionalMapper ppMapper;
	@Autowired
	private ProjectInfoMapper infoMapper;
	@Autowired
	private ProjectReportMapper prMapper;
	@Autowired
	private ProfessionalMapper proMapper;
	@Autowired
	private CustomerMapper customerMapper;
	@Autowired
	private ProfessionTypeMapper professionTypeMapper;
	@Autowired
	private ProfessionalWorkMapper professionWorkMapper;
	
	
	public DataTablesVo getProjects(String id,String search,String start, String length, int draw){
		if(StringUtils.isEmpty(id)){
			id=null;
		}
		List<Project> list =  projectMapper.getProjects(id==null?null:Integer.valueOf(id),search,Integer.valueOf(start), Integer.valueOf(length));
		int count = projectMapper.getProjectsTotal(id==null?null:Integer.valueOf(id),search);
		
		return DataTablesVoUtil.getDataTablesVo(draw, count, count, list);
	}
	
	public Project getProject(String projectId){
		return projectMapper.selectByPrimaryKey(Integer.valueOf(projectId));
	}
	
	public int addOrUpdateProject(Project project, Integer userId) throws Exception{
		if (project != null) {
			if (project.getId() != null) {
				log.info("update project ");
				project.setUpdateUser(Integer.valueOf(userId));
				project.setUpdateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
				projectMapper.updateByPrimaryKeySelective(project);
			}else{
				log.info("add project ");
				project.setStatus(Constants.PROJECT_STATUS_ONE);
				project.setCreateUser(Integer.valueOf(userId));
				project.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
				projectMapper.insertSelective(project);
			}
		}
		return project.getId();
	}
	
	public Map<Object, Object> getProfessionalList(String projectId){
		List<Integer> professionTypeList = ppMapper.getAllProfessionTypeByProjectId(Integer.valueOf(projectId));
		Map<Object, Object> professionMap = new HashMap<>();
		for (Integer typeId : professionTypeList) {
			List<ProjectProfessional> projectProfessionList = new ArrayList<>();
			
			ProfessionType professionType = new ProfessionType();
			professionType.setId(typeId);
			ProfessionType profession = professionTypeMapper.selectByCondition(professionType).get(0);
			List<ProjectProfessional> professionList = ppMapper.getDataByProjectIdAndProfessionTypeId(Integer.valueOf(projectId),typeId);
			for (ProjectProfessional projectProfessional : professionList) {
				ProfessionalWithBLOBs professionBlobs = proMapper.selectByPrimaryKey(projectProfessional.getProfessionalId());
				projectProfessional.setProfessionalName(professionBlobs.getName());
				projectProfessionList.add(projectProfessional);
			}
			professionMap.put(profession.getType()+"_"+profession.getId(), projectProfessionList);
		}
		return professionMap;
	}
	
	public void addProjectInfo(ProjectInfo info) throws Exception{
		infoMapper.insert(info);
	}
	
	public DataTablesVo getProjectInfos(int projectId){
		List<ProjectInfoVo> list =  infoMapper.getProjectInfos(projectId);
		for (ProjectInfoVo projectInfoVo : list) {
			String content = projectInfoVo.getContent();
			content = content.replace("\t", "");
			content = content.replace("\n", "");
			projectInfoVo.setContent(content);
		}
		return  DataTablesVoUtil.getDataTablesVo(1, list.size(), list.size(), list);
	}
	
	public List<Project> getProjectInfosByCustomerId(int id){
		return projectMapper.getProjectInfosByCustomerId(id);
	}
	
	public void addProjectReports(String reports,String filenames, String projectId,String ppId, String reportType, Integer uploadUserId){
		String[] s = reports.split("\\|");
		String[] filenameArray = filenames.split("\\|");
		for (int i=0; i<s.length ; i++) {
			String path = s[i];
			if (!StringUtils.isEmpty(path)) {
				ProjectReport pr = new ProjectReport();
				pr.setName(filenameArray[i]);
				pr.setPath(path);
				pr.setType(Integer.valueOf(reportType));
				pr.setProjectId(Integer.valueOf(projectId));
				pr.setUploadUser(Integer.valueOf(uploadUserId));
				pr.setUploadTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
				pr.setPpId(Integer.valueOf(ppId));
				pr.setStatus(Constants.PROJECT_REPORT_STATUS_UPLOAD);
				prMapper.insert(pr);
			}
		}
	}
	
	public DataTablesVo getReports(int projectId){
		List<ProjectReportVo> list = prMapper.getProjectReportsByProjectId(projectId);
		return  DataTablesVoUtil.getDataTablesVo(1, list.size(), list.size(), list);
	}
	
	public void addOrUpdateProjectProfessional(ProjectProfessional pp, Integer userId) throws Exception{
		ProjectProfessional p = ppMapper.getPPByProjectIdAndProfessionalId(pp.getProjectId(), pp.getProfessionalId());
		if (p == null) {
			//add
			pp.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
			pp.setCreateUser(userId);
			
			ppMapper.insert(pp);
		}else{
			pp.setId(p.getId());
			pp.setUpdateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
			pp.setUpdateUser(userId);
			
			ppMapper.updateByPrimaryKeySelective(pp);
		}
	}

	public void updateProjectProfessionalStatus(String type,String token){
		String id = EncryptUtil.PBEDecrypt(token);
		if(id==null) return;
		ProjectProfessional pp = new ProjectProfessional();
		pp.setId(Integer.valueOf(id).intValue());
		pp.setStatus(Integer.valueOf(type).intValue());
		ppMapper.updateByPrimaryKeySelective(pp);
	}
	
	public Email getPpEmailPayInfo(Integer ppId, String senderEmail, String senderUserName){
		ProjectProfessional pp = ppMapper.selectByPrimaryKey(ppId);
		if(pp != null){
			Email email = new Email();
			Professional pro = proMapper.selectByPrimaryKey(pp.getProfessionalId());
			Project project = projectMapper.selectByPrimaryKey(pp.getProjectId());
			if (pro != null && project != null) {
				email.setReceiver(pro.getEmail());
				String content = EmailContentTemplateUtil.getProPayInfoTemplate();
				if (content != null) {
					content = content.replace("$professional_name$", pro.getName());
					content = content.replace("$login_user_name$", senderUserName);
				}
				email.setContent(content);
				email.setCc(senderEmail);
				email.setSubject(pro.getName()+"-付款方式");
			}
			return email;
		}
		return null;
	}
	
	public Email getPpEmailInfoEnglish(Integer ppId, String senderEmail, String projectUrl){
		ProjectProfessional pp = ppMapper.selectByPrimaryKey(ppId);
		if(pp != null){
			Email email = new Email();
			Professional pro = proMapper.selectByPrimaryKey(pp.getProfessionalId());
			Project project = projectMapper.selectByPrimaryKey(pp.getProjectId());
			if (pro != null && project != null) {
				email.setReceiver(pro.getEmail());
				String content = EmailContentTemplateUtil.getProEnglishEmailContentTemplate();
				if (content != null) {
					content = content.replace("$professional_name$", pro.getName());
					content = content.replace("$project_name$", project.getName());
					content = content.replace("$professional_company$", pro.getCompany());
					content = content.replace("$professional_title$", pro.getTitle());
					content = content.replace("$professional_price$", pp.getProfessionalPrice() != null ? String.valueOf(pp.getProfessionalPrice().intValue()) : "0");
					
					String condition = projectUrl + "/analyst/project/condition";
					
					String term_url = condition;
					content = content.replace("$sender_email$", senderEmail);
					content = content.replace("$terms_url$",term_url);
					
					String url_yes = projectUrl+"/analyst/project/mailcheck/"+Constants.PROJECT_PROFESSINOAL_STATUS_AGREE+"/"+EncryptUtil.PBEEncrypt(String.valueOf(ppId));
					String url_no = projectUrl+"/analyst/project/mailcheck/"+Constants.PROJECT_PROFESSINOAL_STATUS_DISAGREE+"/"+EncryptUtil.PBEEncrypt(String.valueOf(ppId));
					
					int i =content.indexOf("$url_yes$");
					content = content.replace("$url_yes$", url_yes);
					content = content.replace("$url_no$", url_no); 
				}
				email.setContent(content);
				email.setCc(senderEmail);
				email.setSubject("Six Degress-Confirmation of Consultation Engagement");
			}
			return email;
		}
		return null;
	}
	
	public Email getPpEmailInfo(Integer ppId, String senderEmail, String projectUrl){
		ProjectProfessional pp = ppMapper.selectByPrimaryKey(ppId);
		if(pp != null){
			Email email = new Email();
			Professional pro = proMapper.selectByPrimaryKey(pp.getProfessionalId());
			Project project = projectMapper.selectByPrimaryKey(pp.getProjectId());
			if (pro != null && project != null) {
				email.setReceiver(pro.getEmail());
				String content = EmailContentTemplateUtil.getProTemplate();
				if (content != null) {
					content = content.replace("$professional_name$", pro.getName());
					content = content.replace("$project_name$", project.getName());
					content = content.replace("$professional_company$", pro.getCompany());
					content = content.replace("$professional_title$", pro.getTitle());
					content = content.replace("$professional_price$", pp.getProfessionalPrice() != null ? String.valueOf(pp.getProfessionalPrice().intValue()) : "0");
					
					String condition = projectUrl + "/analyst/project/condition";
					
					String term_url = condition;
					content = content.replace("$sender_email$", senderEmail);
					content = content.replace("$terms_url$",term_url);
					
					String url_yes = projectUrl+"/analyst/project/mailcheck/"+Constants.PROJECT_PROFESSINOAL_STATUS_AGREE+"/"+EncryptUtil.PBEEncrypt(String.valueOf(ppId));
					String url_no = projectUrl+"/analyst/project/mailcheck/"+Constants.PROJECT_PROFESSINOAL_STATUS_DISAGREE+"/"+EncryptUtil.PBEEncrypt(String.valueOf(ppId));
					
					int i =content.indexOf("$url_yes$");
					content = content.replace("$url_yes$", url_yes);
					content = content.replace("$url_no$", url_no); 
				}
				email.setContent(content);
				email.setCc(senderEmail);
				email.setSubject(project.getName()+"-咨询意向");
			}
			return email;
		}
		return null;
	}
	
	//type: profession_list(专家发送列表)   profession_info(专家发送详情)
	public Email getCustomerEmailInfo(Integer projectId, String senderEmail, String type,String ppid){
		//List<ProjectProfessional> list = ppMapper.getAllPpByProjectIdWithStatus(projectId,Constants.PROJECT_PROFESSINOAL_STATUS_AGREE);
		List<Integer> professionTypeList = new ArrayList<>();
		List<Integer> ppidList = new ArrayList<>();
		if("".equals(ppid) || ppid == null){
			//获取该项目的所有专家分类
			professionTypeList = ppMapper.getAllProfessionTypeByProjectId(Integer.valueOf(projectId));
			List<ProjectProfessional> allProjectprofession = ppMapper.getAllPpByProjectId(Integer.valueOf(projectId));
			for (ProjectProfessional projectProfessional : allProjectprofession) {
				ppidList.add(projectProfessional.getId());
			}
		}
		else{
			String[] ppids = ppid.split(",");
			for (String id : ppids) {
				ppidList.add(Integer.valueOf(id));
			}
			professionTypeList = ppMapper.getProfessionTypesByProjectIdAndPpids(Integer.valueOf(projectId),ppidList);
		}
		
		if (professionTypeList != null && professionTypeList.size() > 0) {
			Project pv = projectMapper.selectByPrimaryKey(projectId);
			Customer c = customerMapper.selectByPrimaryKey(pv.getCustomerId());
			if (pv != null && c != null) {
				Email email = new Email();
				String head = EmailContentTemplateUtil.getCustomerHead();
				String content = head;
				content = content.replace("$customer_name$", c.getUsername());
				content = content.replace("$project_name$", pv.getName());
				//遍历分类
				int classIndex = 0;
				for (Integer professionTypeId : professionTypeList) {
					classIndex++;
					ProfessionType professionType = new ProfessionType();
					professionType.setId(professionTypeId);
					ProfessionType professionTypePo = professionTypeMapper.selectByCondition(professionType).get(0);
					//添加分类字符串
					String typeStr = EmailContentTemplateUtil.getCustomerProTypeTemplate();
					typeStr = typeStr.replace("$pro_type$", professionTypePo.getType());
					content += typeStr;
					List<ProjectProfessional> list = new ArrayList<>();
					list = ppMapper.getDataByPPidsAndProfessionType(ppidList,professionTypeId);
					//遍历专家
					int professionIndex = 0;
					for (ProjectProfessional projectProfessional : list) {
						professionIndex++;
						Professional profession = proMapper.selectByPrimaryKey(projectProfessional.getProfessionalId());
						
						if (projectProfessional.getStatus() != Constants.PROJECT_PROFESSINOAL_STATUS_DISAGREE) {
							String cc = "";
							if("profession_list".equals(type)){
								List<ProfessionalWork> professionWorkList = professionWorkMapper.getProfessionalWorksByProfessionalId(profession.getId());
								String workExps="";
								String expTitle = "<tr>" + 
												  	"<td colspan=\"2\" align=\"left\" style=\"align:left;font-family:Microsoft YaHei;font-size:10pt\">工作经历	</td>" +
												  "</tr>";
								if(professionWorkList != null && professionWorkList.size() > 0){
									workExps = expTitle;
								}
								for (ProfessionalWork professionalWork : professionWorkList) {
									String startDate = professionalWork.getStartDate();
									String endDate = professionalWork.getEndDate();
									String workExp = "<tr style=\"align:left;font-family:Microsoft YaHei;font-size:10pt\">"+
														"<td align=\"left\" style=\"align:left;font-family:Microsoft YaHei;font-size:10pt\">"+startDate.replaceAll("-", ".")+"-"+ endDate.replaceAll("-", ".") +"</td>" +
														"<td style=\"align:left;font-family:Microsoft YaHei;font-size:10pt\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"+ professionalWork.getCompanyName() +" "+ professionalWork.getTitle() +"</td>"+
													"</tr>";
									workExps += workExp;
								}
								
								if(projectProfessional.getPriceTimes().doubleValue() > new Double("1").doubleValue()){
									String priceTimes = "<tr>" + 
											"<td colspan=\"2\" align=\"left\" style=\"align:left;font-family:Microsoft YaHei;font-size:10pt\">" +
												"我们按照<b> "+ projectProfessional.getPriceTimes() +" </b>倍的价格向您收取咨询费用" + 
											"</td>" +
										"</tr>";
									workExps += priceTimes;
								}
								cc = EmailContentTemplateUtil.getCustomerCenter();
								cc = cc.replace("$workexp$", workExps);
							}
							if("profession_info".equals(type)){
								cc = EmailContentTemplateUtil.getCustomProfessionInfoCenter();
								cc = cc.replace("$professionName$", profession.getName());
								cc = cc.replace("$professionEmail$", profession.getEmail());
								String phoneLinks = "";
								String phoneStr = profession.getPhone();
								String[] phoneArr = phoneStr.split("/");
								for (int i = 0; i < phoneArr.length; i++) {
									String phoneALink = "<a href=\"tel:"+phoneArr[i]+"\" value=\""+phoneArr[i]+"\" target=\"_blank\">"+phoneArr[i]+"</a>";
									if(i != phoneArr.length - 1){
										phoneALink += "/";
									}
									phoneLinks += phoneALink;
								}
								cc = cc.replace("$professionPhone$",phoneLinks);
							}
							cc = cc.replace("$pid$", classIndex+"."+professionIndex);
							cc = cc.replace("$company$", projectProfessional.getCompany());
							cc = cc.replace("$title$", projectProfessional.getTitle());
							cc = cc.replace("$re$", String.valueOf(projectProfessional.getExp()));
							
							content += cc;
						}
					}
				}
				content += EmailContentTemplateUtil.getCustomerBottom().replace("$sender_email$", senderEmail);
				email.setSubject(c.getUsername()+"-"+pv.getName()+"-备选专家列表");
				email.setContent(content);
				email.setReceiver(c.getEmail());
				email.setCc(senderEmail);
				return email;
			}
				
			}
		return null;
	}
	
	
	
	public void updatePpStatus(int ppId, int status) throws Exception{
		ProjectProfessional pp = new ProjectProfessional();
		pp.setId(ppId);
		pp.setStatus(status);
		ppMapper.updateByPrimaryKeySelective(pp);
	}

	/**
	 * @param projectId
	 * @return
	 */
	public boolean deleteById(String projectId) {
		int deleteCount = projectMapper.deleteByPrimaryKey(Integer.valueOf(projectId));
		return deleteCount >= 0;
	}

	public Boolean deleteReportByReportId(String reportId) {
		Integer id  = Integer.valueOf(reportId);
		int deleteCount = prMapper.deleteByPrimaryKey(id);
		return deleteCount >= 0;
	}

	
	public Boolean deleteRequireById(String requireId) {
		int deleteCount = infoMapper.deleteByPrimaryKey(Integer.valueOf(requireId));
		return deleteCount >= 0;
	}

	public Boolean updateRequireById(String requireId, String description) {
		ProjectInfo projectInfo = new ProjectInfo();
		projectInfo.setContent(description);
		projectInfo.setId(Integer.valueOf(requireId));
		int updateCount = infoMapper.updateByPrimaryKeySelective(projectInfo);
		
		return updateCount == 1;
	}

	/**
	 * @param id
	 * @param pROJECT_PROFESSINOAL_STATUS_SENDCUSTOMEREMAIL
	 * @return 
	 */
	public int updatePpCustomerStatus(Integer id, Integer customerStatus) {
		ProjectProfessional projectPro = new ProjectProfessional();
		projectPro.setId(id);
		projectPro.setCustomerStatus(customerStatus);
		int updateCount = ppMapper.updateByPrimaryKeySelective(projectPro);
		return updateCount;
	}
}
