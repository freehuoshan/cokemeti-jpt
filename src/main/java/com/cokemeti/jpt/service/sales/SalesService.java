/**
 * 
 */
package com.cokemeti.jpt.service.sales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.cokemeti.jpt.domain.attachement.Attachement;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.sales.Customer;
import com.cokemeti.jpt.domain.sales.CustomerAccount;
import com.cokemeti.jpt.domain.sales.CustomerActivity;
import com.cokemeti.jpt.domain.sales.CustomerCompany;
import com.cokemeti.jpt.domain.sales.vo.ActivityVo;
import com.cokemeti.jpt.domain.sales.vo.CustomerVo;
import com.cokemeti.jpt.idao.attachement.AttachementMapper;
import com.cokemeti.jpt.idao.sales.CustomerAccountLogMapper;
import com.cokemeti.jpt.idao.sales.CustomerAccountMapper;
import com.cokemeti.jpt.idao.sales.CustomerActivityMapper;
import com.cokemeti.jpt.idao.sales.CustomerCompanyMapper;
import com.cokemeti.jpt.idao.sales.CustomerMapper;
import com.cokemeti.jpt.util.DataTablesVoUtil;
import com.cokemeti.jpt.util.JsonUtil;
import com.cokemeti.jpt.util.ResultBean;

/**
 * @author Administrator
 *
 */
@Service
public class SalesService {

	@Autowired
	private CustomerMapper customerMapper;

	@Autowired
	private CustomerCompanyMapper customerCompanyMapper;
	
	@Autowired
	private CustomerAccountMapper customerAccountMapper;

	@Autowired
	private CustomerActivityMapper customerActivityMapper;
	
	@Autowired
	private CustomerAccountLogMapper customerAccountLogMapper;
	
	@Autowired
	private AttachementMapper attachementMapper;
	
	public Customer getCustomerByid(int id){
		return customerMapper.selectByPrimaryKey(id);
	}
	
	public CustomerVo getCustomerInfoByid(int id){
		CustomerVo vo = new CustomerVo();
		Customer c= customerMapper.selectByPrimaryKey(id);
		if(c!=null){
			CustomerCompany cc = customerCompanyMapper.selectByPrimaryKey(c.getCompanyId());
			BeanUtils.copyProperties(cc, vo);
			CustomerAccount ca = customerAccountMapper.selectAccountByUserid((int)id);
			BeanUtils.copyProperties(ca, vo);
			BeanUtils.copyProperties(c, vo);
		}
		return vo;
	}
	
	public DataTablesVo getCustomers(String search,String start, String length,int draw){
		List<CustomerVo> list = customerMapper.getCustomers(search,Integer.valueOf(start), Integer.valueOf(length));
		int count = customerMapper.getCustomersTotal(search);
		return DataTablesVoUtil.getDataTablesVo(draw, count, count, list);
	}
	
	public DataTablesVo getActivities(String customerid,String search,String start, String length,int draw){
		List<ActivityVo> list = customerActivityMapper.getActivities(customerid,search,Integer.valueOf(start), Integer.valueOf(length));
		int count = customerActivityMapper.getActivitiesTotal(customerid,search);
		return DataTablesVoUtil.getDataTablesVo(draw, count, count, list);
	}
	
	public ResultBean saveCustomer(Customer ct,CustomerCompany company,CustomerAccount ca,int type){
		ResultBean result=null;
		if(type==0){			
			int count = customerCompanyMapper.insertSelective(company);
			if(count==1){
				ct.setCompanyId(company.getId());
				int c = customerMapper.insertSelective(ct);
				if(c==1){
					ca.setCustomerId(ct.getId());
					customerAccountMapper.insertSelective(ca);
				}else{
					result = JsonUtil.getJson(400, "新增失败");
				}
				result = JsonUtil.getJson(100, "新增成功");
			} else{
				result = JsonUtil.getJson(400, "新增失败");
			}
		}else if(type==1){
			customerMapper.updateByPrimaryKeySelective(ct);
			Customer ct2 = customerMapper.selectByPrimaryKey(ct.getId());
			company.setId(ct2.getCompanyId());
			customerCompanyMapper.updateByPrimaryKeySelective(company);
			ca.setCustomerId(ct.getId());
			customerAccountMapper.updateByCustomId(ca);
			result = JsonUtil.getJson(100, "修改成功");
		}
		return result;
	}
	
	public ResultBean saveActivite(CustomerActivity ca,int type){
		ResultBean result=null;
		if(type==0){			
			int count = customerActivityMapper.insertSelective(ca);
			if(count==1){
				result = JsonUtil.getJson(100, "新增成功");
			} else{
				result = JsonUtil.getJson(400, "新增失败");
			}
		}else if(type==1){
			customerActivityMapper.updateByPrimaryKey(ca);
			result = JsonUtil.getJson(100, "修改成功");
		}
		return result;
	}
	
	public ArrayList<Attachement> saveAttachement(ArrayList<Attachement> list){
		attachementMapper.insertByList(list);
		return list;
	}
	
	public void saveFileIdByCustomerId(Customer record){
		customerMapper.updateByPrimaryKeySelective(record);
	}
	
	public DataTablesVo getAttachementList(String fids,String search,String start, String length,int draw){
		List<Attachement> list = new ArrayList<Attachement>();
		if(!StringUtils.isEmpty(fids)){
			String[] ids = fids.split(",");
			Map<String, Object> params = new HashMap<String, Object>(4);
			params.put("ids", ids);
			params.put("search", search);
			int count = attachementMapper.getAttachementsTotal(params);
			params.put("bgrow", Integer.valueOf(start));
			params.put("rows", Integer.valueOf(length));
			
			list = attachementMapper.getAttachements(params);
			return DataTablesVoUtil.getDataTablesVo(draw, count, count, list);
		}else{
			return DataTablesVoUtil.getDataTablesVo(draw, 0, 0, list);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	public boolean deleteByCustomId(String id) {
		Integer customId = Integer.valueOf(id);
		Customer custom = this.getCustomerByid(customId);
		int deleteCount = customerMapper.deleteByPrimaryKey(customId);
		int companyCount = customerCompanyMapper.deleteByPrimaryKey(custom.getCompanyId());
		int acountCount = customerAccountMapper.deleteByCustomId(customId);
		int activeCount = customerActivityMapper.deleteByCustomId(customId);
		int acountLogCount = customerAccountLogMapper.deleteByCustomId(customId);
		
		return true;
	}

	public Boolean deleteAttachementById(String attachementId) {
		int deleteCount = attachementMapper.deleteByPrimaryKey(Integer.valueOf(attachementId));
		return deleteCount >= 0;
	}

	public Boolean deleteActivityById(String activityId) {
		int deleteCount = customerActivityMapper.deleteByPrimaryKey(Integer.valueOf(activityId));
		return deleteCount >= 0;
	}

	public CustomerActivity getActivityById(String activityId) {
		
		CustomerActivity customActivity = customerActivityMapper.selectByPrimaryKey(Integer.valueOf(activityId));
		
		return customActivity;
	}

	
	public Boolean updateActivityById(CustomerActivity ca) {
		int count = customerActivityMapper.updateByPrimaryKeySelective(ca);
		return count >= 0;
	}
	
	
}
