package com.cokemeti.jpt.util;

import java.util.List;

import com.cokemeti.jpt.domain.common.EasyUiVo;

public class EasyUiUtil {

	public static EasyUiVo getEasyUIJson(long page,long total,List<?> rows){
		EasyUiVo json = new EasyUiVo();
		json.setRows(rows);
		json.setTotal(total);
		return json;
	}
}
