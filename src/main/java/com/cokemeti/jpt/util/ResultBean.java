package com.cokemeti.jpt.util;

/**
 * 
 * @author hz_joe
 * 
 */
public class ResultBean {

	private Integer code; // 状态码 
	private String msg; // 错误信息
	private Object data; // 返回数据

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
