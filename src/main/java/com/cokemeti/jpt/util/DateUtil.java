/**
 * 
 */
package com.cokemeti.jpt.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * @author Administrator
 * 平台默认统一格式  
 * 时间： yyyy-MM-dd HH:mm:ss
 * 日期： yyyy-MM-dd
 * 月份： yyyy-MM
 */
public class DateUtil {
	private static java.util.Calendar calendar = java.util.Calendar.getInstance();

	  /**
	   * 将Date根据指定格式(yyyy-MM-dd HH:mm:ss)转化成字符串
	   * @param date Date
	   * @param format String
	   * @return String
	   */
	  public static final String baseFormatDateTime(Date date, String format) {
	    SimpleDateFormat mydate = new SimpleDateFormat(format);
	    return mydate.format(date);
	  }

	  /**
	   * 将String根据指定类型（yyyyMMdd） 转化为Date
	   * @param date
	   * @param format
	   * @return
	   */
	  public static Date formatStringToDate(String date, String format){
		  SimpleDateFormat sdf = new SimpleDateFormat(format);
		  try {
			  return sdf.parse(date);
		  } catch (ParseException e) {
			  e.printStackTrace();
		  }
		  return null;
	  }
	    /**
	     * 将long类型时间根据指定格式(yyyy-MM-dd HH:mm:ss)转化成字符串
	     * @param millis long
	     * @param format String
	     * @return String
	     */
	    public static final String baseFormatDateTime(long millis, String format) {
	      return baseFormatDateTime(new Date(millis), format);
	    }

	    /**
	     * 取得格式化日期时间的函数 2004-10-25 05:34:23
	     * @param date Date
	     * @return String
	     */
	    public static final String getFormatDateTime(java.util.Date date) {
	      return baseFormatDateTime(date, "yyyy-MM-dd HH:mm:ss");

	    }

	    /**
	     * 取得格式化日期时间的函数 2004-10-25 05:34:23
	     * @param millis long
	     * @return String
	     */
	    public static final String getFormatDateTime(long millis) {
	      return getFormatDateTime(new Date(millis));
	    }

	    /**
	     * 取得格式化日期的函数 2004-10-25
	     * @param date Date
	     * @return String
	     */
	    public static final String getFormatDate(java.util.Date date) {
	      return baseFormatDateTime(date, "yyyy-MM-dd");
	    }

	    /**
	     * 取得格式化日期的函数 2004-10-25
	     * @param millis long
	     * @return String
	     */
	    public static final String getFormatDate(long millis) {
	      return getFormatDate(new Date(millis));
	    }


	  /**
	   * 根据日期得到年份字符串
	   * @param date Date
	   * @return String
	   */
	  public static final String getYear(java.util.Date date) {
	    String tempstr;
	    int i;
	    SimpleDateFormat mydate = new SimpleDateFormat(" MMM dd H mm ss,yyyy");
	    tempstr = "";
	    mydate.applyPattern("yyyy");
	    i = Integer.parseInt(mydate.format(date));
	    tempstr = tempstr + String.valueOf(i);
	    return tempstr;
	  }

	  /**
	   * 根据日期得到月份字符串
	   * @param date Date
	   * @return String
	   */
	  public static final String getMonth(java.util.Date date) {
	    String tempstr;
	    int i;
	    SimpleDateFormat mydate = new SimpleDateFormat(" MM dd H mm ss,yyyy");
	    tempstr = "";
	    mydate.applyPattern("MM");
	    i = Integer.parseInt(mydate.format(date));
	    tempstr += String.valueOf(i);
	    return tempstr;
	  }

	  /**
	   * 根据日期得到日期字符串
	   * @param date Date
	   * @return String
	   */
	  public static final String getDay(java.util.Date date) {
	    String Tempstr;
	    int i;
	    SimpleDateFormat mydate = new SimpleDateFormat(" MM dd H mm ss,yyyy");
	    Tempstr = "";
	    mydate.applyPattern("dd");
	    i = Integer.parseInt(mydate.format(date));
	    Tempstr = Tempstr + String.valueOf(i);
	    return Tempstr;
	  }

	  /**
	   * 根据日期得到小时字符串
	   * @param date Date
	   * @return String
	   */
	  public static final String getHour(java.util.Date date) {
	    String Tempstr;
	    int i;
	    SimpleDateFormat mydate = new SimpleDateFormat(" MM dd H mm ss,yyyy");
	    Tempstr = "";
	    mydate.applyPattern("H");
	    i = Integer.parseInt(mydate.format(date));
	    Tempstr = Tempstr + String.valueOf(i);
	    return Tempstr;
	  }
	  public static final String getFormatDateTime(Long lngObj) {
	    return getFormatDateTime(lngObj.longValue());
	  }

	  /**
	   * 根据日期得到格式化小时、分 例如  02:32
	   * @param date Date
	   * @return String
	   */
	  public static final String getFormatTime(java.util.Date date) {
	    String Tempstr;
	    int i;
	    SimpleDateFormat mydate = new SimpleDateFormat(" MMM dd H mm ss,yyyy");
	    Tempstr = "";

	    mydate.applyPattern("H");
	    i = Integer.parseInt(mydate.format(date));
	    if(i < 10) {
	      Tempstr = Tempstr + "0" + String.valueOf(i) + ":";
	    } else {
	      Tempstr = Tempstr + String.valueOf(i) + ":";
	    }

	    mydate.applyPattern("mm");
	    i = Integer.parseInt(mydate.format(date));
	    if(i < 10) {
	      Tempstr = Tempstr + "0" + String.valueOf(i);
	    } else {
	      Tempstr = Tempstr + String.valueOf(i);
	    }
	    return Tempstr;
	  }

	  /**
	   * 根据日期得到小时 格式例如  02
	   * @param date Date
	   * @return String
	   */
	  public static final String getFormatHour(java.util.Date date) {
	    String Tempstr;
	    int i;
	    SimpleDateFormat mydate = new SimpleDateFormat(" MMM dd H mm ss,yyyy");
	    Tempstr = "";

	    mydate.applyPattern("H");
	    i = Integer.parseInt(mydate.format(date));
	    Tempstr = Tempstr + String.valueOf(i);
	    return Tempstr;
	  }

	  /**
	   * 根据日期得到分钟的格式例如  28
	   * @param date Date
	   * @return String
	   */
	  public static final String getFormatMinute(java.util.Date date) {
	    String Tempstr;
	    int i;
	    SimpleDateFormat mydate = new SimpleDateFormat(" MMM dd H mm ss,yyyy");
	    Tempstr = "";

	    mydate.applyPattern("mm");
	    i = Integer.parseInt(mydate.format(date));
	    Tempstr = Tempstr + String.valueOf(i);
	    return Tempstr;
	  }

	  /**
	   * 用于只输入年月日生成long型的时间格式
	   * @param yy int
	   * @param mm int
	   * @param dd int
	   * @return long
	   */
	  public static final long getTimeLong(int yy, int mm, int dd) {
	    calendar.clear();
	    calendar.set(yy, mm - 1, dd, 0, 0, 0);
	    return calendar.getTimeInMillis();
	  }

	  /**
	   * 用于输入年月日小时分生成long型的时间格式
	   * @param yy int
	   * @param mm int
	   * @param dd int
	   * @param h int
	   * @param m int
	   * @return long
	   */
	  public static final long getTimeLong(int yy, int mm, int dd, int h, int m) {
	    calendar.clear();
	    calendar.set(yy, mm - 1, dd, h, m, 0);
	    return calendar.getTimeInMillis();
	  }

	  /**
	   * 用于只输入年，月生成long型的时间格式
	   * @param yy int
	   * @param mm int
	   * @return long
	   */
	  public static final long getTimeLong(int yy, int mm) {
	    calendar.clear();
	    calendar.set(yy, mm - 1, 0, 0, 0, 0);
	    return calendar.getTimeInMillis();
	  }

	  /**
	   * 根据输入的初始日期和新增的月份到新增月份后的总时间
	   * @param startTime Date
	   * @param month long
	   * @return long
	   */
	  public static final long getTotalTime(java.util.Date startTime, long month) {
	    calendar.setTime(startTime);
	    calendar.add(Calendar.MONTH, (int)month);
	    return calendar.getTimeInMillis();
	  }

	  /**
	   * 用于输入年月日小时分秒生成long型的时间格式
	   * @param yy int
	   * @param mm int
	   * @param dd int
	   * @param h int
	   * @param m int
	   * @param sec int
	   * @return long
	   */
	  public static final long getTimeLong(int yy, int mm, int dd, int h, int m,
	      int sec) {
	    calendar.clear();
	    calendar.set(yy, mm - 1, dd, h, m, sec);
	    return calendar.getTimeInMillis();
	  }
	  /**
	   * 从日期时间字符串中取得时间的long型
	   * @param datetime String 格式: 2006-12-31 00:00:00
	   * @return long
	   */
	  public static long getLongTimefromString(String datetime) {
	    String args[] = datetime.split(" ");
	    String date = args[0];
	    String time = args[1];

	    String dateArgs[] = date.split("-");
	    String timeArgs[] = time.split(":");
	    String mm = "00";
	    if(timeArgs.length == 3){
	    	mm = timeArgs[2];
	    }

	    long ret = getTimeLong(Integer.parseInt(dateArgs[0])
	                           , Integer.parseInt(dateArgs[1])
	                           , Integer.parseInt(dateArgs[2])
	                           , Integer.parseInt(timeArgs[0])
	                           , Integer.parseInt(timeArgs[1])
	                           , Integer.parseInt(mm));

	    return ret;

	  }
	  /**
	   * 将2005-10-19类型转换成long型
	   *
	   * @param date String
	   * @return long
	   */
	  public static long getLongDateFromString(String date){
	    String[] args = date.split("-");
	    return DateUtil.getTimeLong(Integer.parseInt(args[0]),Integer.parseInt(args[1]),Integer.parseInt(args[2]));
	  }

	  /**
	   * 返回相应格式的时间字符串，iFlag == 0，返回年月日时分秒
	   * iFlag == 1，返回年－月－日 时：分：秒
	   * iFlag == 2，返回年月日
	   * iFlag == 3，返回年－月－日
	   * iFlag == 4，返回年－月
	   * @param lngDate long  要转化的时间
	   * @param iFlag int  要转化的时间格式
	   * @return String    相应格式的时间字符串
	   */
	  public static String getFormatDate(long lngDate, int iFlag) {
	    return getFormatDate(new Date(lngDate), iFlag);
	  }
	  /**
	   * 返回相应格式的时间字符串，iFlag == 0，返回年月日时分秒
	   * iFlag == 1，返回年－月－日 时：分：秒
	   * iFlag == 2，返回年月日
	   * iFlag == 3，返回年－月－日
	   * iFlag == 4，返回年－月
	   * @param date Date  要转化的时间
	   * @param iFlag int  要转化的时间格式
	   * @return String    相应格式的时间字符串
	   */
	  public static String getFormatDate(Date date, int iFlag) {
	    SimpleDateFormat simpleDateFormat;
	    if(iFlag == 0) {
	      simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	    } else if(iFlag == 1) {
	      simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    } else if(iFlag == 2) {
	      simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
	    } else if(iFlag == 3) {
	      simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    } else if(iFlag == 4) {
	      simpleDateFormat = new SimpleDateFormat("yyyy-MM");
	    } else if(iFlag == 5) {
	      simpleDateFormat = new SimpleDateFormat("yyyy-MM HH");
	    } else {
	      simpleDateFormat = new SimpleDateFormat("yyyy-MM HH:mm");
	    }
	    return simpleDateFormat.format(date);
	  }

	  /**
	   * 转化当前的时间，返回相应格式的时间字符串，iFlag == 0，返回年月日时分秒
	   * @param iFlag int
	   * @return String
	   */
	  public static String getFormatDate(int iFlag) {
	    return getFormatDate(new Date(), iFlag);
	  }

	  /**
	   * 根据输入一个时间得到和现在的时间差
	   * @param tagTime long
	   * @return String
	   */
	  public static final String getLeaveTime(long tagTime) {
	    long nowTime = System.currentTimeMillis();
	    long leaveTime = 0;
	    if(nowTime > tagTime)
	      leaveTime = (nowTime - tagTime) / 1000;
	    else
	      leaveTime=(tagTime - nowTime) / 1000;;
	    long date = 0;
	    long hour = 0;
	    long minute = 0;
	    long dateTime = 0;
	    long hourTime = 0;
	    String strDate = "";
	    String strHour = "";
	    String strMinute = "";
	    date = leaveTime / 86400;
	    dateTime = date*86400;
	    hour = (leaveTime - dateTime)/3600;
	    hourTime = hour*3600;
	    minute = (leaveTime - dateTime - hourTime)/60;
	    if(date > 0)
	      strDate = date + "天";
	    if(hour > 0||(minute > 0&&date>0))
	      strHour = hour + "小时";
	    if(minute > 0)
	      strMinute = minute + "分";
	    return strDate + strHour + strMinute;
	  }
	  /**
	   * 根据输入一个时间得到和现在的时间差(天数)
	   * @param tagTime long
	   * @return String
	   */
	  public static final String getLeaveDate(long tagTime) {
	    long nowTime = System.currentTimeMillis();
	    long leaveTime = 0;
	    if(nowTime > tagTime)
	      leaveTime = (nowTime - tagTime) / (1000 * 24 * 3600);
	    else
	      leaveTime = (tagTime - nowTime) / (1000 * 24 * 3600);
	    return leaveTime+"";
	  }

	  /**
	   * 查询公元某年某月某日为星期几,0代表星期天
	   * @param year int
	   * @param month int
	   * @param day int
	   * @return int
	   * @author meigen 2005-10-18
	   */
	  public static int getWeekDay(int year, int month, int day) {
	    if (month < 3) {
	      month += 13;
	      year--;
	    }
	    else month++;
	    return (day + 26 * month / 10 + year + year / 4 - year / 100 + year / 400 +
	            6) % 7;
	  }
	  
	  
		/**
		 * 获取年月日 
		 * i为0时返回当日，i不为零时返回前后i日 日期
		 * @param i 
		 * @return
		 */
		public static Date getDate(Date date, int i){
			Calendar cal = Calendar.getInstance();
			Calendar ca2 = Calendar.getInstance();
			cal.setTime(date);
			ca2.set(Calendar.YEAR, cal.get(Calendar.YEAR));
			ca2.set(Calendar.MONTH, cal.get(Calendar.MONTH));
			ca2.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + i);
			return ca2.getTime();
		}

		/**
		 * 根据date获取相应天数间隔space的时间
		 * @param date      日期
		 * @param format    格式
		 * @param space     间隔天数
		 * @return
		 */
		public static String getFormatStringBySpace(Date date, String format, int space){
			return baseFormatDateTime(date.getTime()+(long)space*24*60*60*1000,format);
		}

		/**
		 * 获取本月某号的formatString
		 * @param day        某号
		 * @param format     格式
		 * @return
		 */
		public static String getFormatStringDayOfMonth(int day, String format){
	        Calendar calendar = Calendar.getInstance();
	        calendar.set(Calendar.DAY_OF_MONTH, day);
	        calendar.getTime();
	        return DateUtil.baseFormatDateTime(calendar.getTime(), format);
		}
		
		public static String getWeekByLongtime(Long time){
			  Calendar c = Calendar.getInstance();
			  c.setTime(new Date(time));
			  int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			  switch (dayOfWeek) {
			  case 1:
				  return "星期日";
			  case 2:
				  return "星期一";
			  case 3:
				  return "星期二";
			  case 4:
				   return "星期三";
			  case 5:
				   return "星期四";
			  case 6:
				   return "星期五";
			  case 7:
				   return "星期六";

			  }
			  return "";
		}
		
		//getFormatStringBySpace
		public static void main(String[] args) {
			System.out.println(System.currentTimeMillis());
			System.out.println(System.currentTimeMillis()-30*24*60*60*1000);
			System.out.println(getFormatStringBySpace(new Date(),"yyyy-MM-dd", -30));
		}
		
		
}
