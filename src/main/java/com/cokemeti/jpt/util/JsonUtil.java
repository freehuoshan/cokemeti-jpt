package com.cokemeti.jpt.util;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

public class JsonUtil {

	/**
	 * 创建返回的json字符串
	 * @param code
	 * @param msg
	 * @param data
	 * @return
	 */
	public static String getJsonStr(int code, String msg, Object data) {
		ResultBean result = new ResultBean();
		result.setCode(code);
		if (!StringUtils.isBlank(msg)) {
			result.setMsg(msg);
		}
		if (data != null) {
			result.setData(data);
		}
		if (StringUtils.isBlank(msg)) {
		}
		return JSON.toJSONString(result);
	}
	
	/**
	 * 创建返回的json字符串
	 * @param code
	 * @param msg
	 * @param data
	 * @return
	 */
	public static ResultBean getJson(int code, String msg, Object data) {
		ResultBean result = new ResultBean();
		result.setCode(code);
		if (!StringUtils.isBlank(msg)) {
			result.setMsg(msg);
		}
		if (data != null) {
			result.setData(data);
		}
		if (StringUtils.isBlank(msg)) {
		}
		return result;
	}
	
	public static String getJsonStr(int code, String msg) {
		return getJsonStr(code, msg, null);
	}
	
	public static String getJsonStr(int code) {
		return getJsonStr(code, null, null);
	}
	
	public static ResultBean getJson(int code, String msg) {
		return getJson(code, msg, null);
	}
	
	public static ResultBean getJson(int code) {
		return getJson(code, null, null);
	}
}
