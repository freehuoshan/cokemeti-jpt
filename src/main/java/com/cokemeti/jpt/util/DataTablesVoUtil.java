package com.cokemeti.jpt.util;

import com.cokemeti.jpt.domain.common.DataTablesVo;

public class DataTablesVoUtil {

	public static DataTablesVo getDataTablesVo(int draw, int recordsTotal, int recordsFiltered,
			Object data){
		DataTablesVo vo = new DataTablesVo(draw,recordsTotal,recordsFiltered,data);
		return vo;
	}
}
