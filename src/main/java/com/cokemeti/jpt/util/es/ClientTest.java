package com.cokemeti.jpt.util.es;
import java.util.ArrayList;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;

import com.alibaba.fastjson.JSONObject;



import com.cokemeti.jpt.util.Constants;

import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

public class ClientTest {
	public static void main(String args[]){
		search();

	}
	
	public static void esTest(){
		ESClient client = ClientF.getInstance();
		//Map<String, Object> i= client.getDocById("resume", "detail", "1");
		//System.out.println(i.get("major"));
	}
	
	public static void get(){
		TransportClient client = ClientF.getTransportClient();
		GetResponse response = client.prepareGet("resume", "detail", "1").get();
		String major = (String) response.getSource().get("major");
		System.out.print(major);
	}
	
	public static void search(){
		TransportClient client = ClientF.getTransportClient();
		QueryBuilder qb2 = QueryBuilders.boolQuery()
				.must(QueryBuilders.matchQuery("_all", "母婴"));
		
		SearchResponse response = client.prepareSearch(Constants.ES_DB)
				.setTypes(Constants.ES_TABLE)
				.setQuery(qb2).setFrom(0).setSize(20)
				.execute().actionGet();
		
		SearchHits sh = response.getHits();
		for (SearchHit hit : sh) {
			System.out.println(hit.getId()+":"+hit.getScore()+":"+hit.getSourceAsString());
		}
		System.out.print(response.getHits().getTotalHits());
		
	}
	

}
