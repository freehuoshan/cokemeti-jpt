package com.cokemeti.jpt.util.es;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import com.cokemeti.jpt.util.Constants;

public class ClientF {
	private static final String NAME = "cluster.name";
	private static final String SNIFF = "client.transport.sniff";
	static Map<String, String> m = new HashMap<String, String>();
	private static String address ;
	private static int port ;
	private static String clusterName ;
	
	 static {
		 InputStream is = null;
		 try {
			 	is = ClientF.class.getClassLoader().getResourceAsStream("properties/ckmtdata.properties");
			 	if (is != null) {
			 		Properties pro = new Properties();
			 		pro.load(is);
			 		
			 		clusterName = pro.getProperty("es.clusterName");
			 		address = pro.getProperty("es.address");
			 		port = Integer.valueOf(pro.getProperty("es.port"));
				}else{
					clusterName = Constants.ES_clusterName;
			 		address = Constants.ES_address;
			 		port = Constants.ES_port;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	 }
	 
	 private static class SingletonHolder{ 
	        private static final ESClient INSTANCE=new ESClient(clusterName,address,port); 
	    } 
	 
	 public static final ESClient getInstance(){ 
	        return SingletonHolder.INSTANCE; 
	    } 
	 
	 
	 //
	 private static class Holder{ 
		 static Settings settings = Settings.settingsBuilder().put(m).
					put(NAME,clusterName).put(SNIFF, true).build();
		 private static TransportClient init(){
			 TransportClient client = null;
		        try {
		         	client = TransportClient
		    					.builder()
		    					.settings(settings)
		    					.build()
		    					.addTransportAddress(
		    							new InetSocketTransportAddress(
		    									new InetSocketAddress(address, port)));
		         } catch (Exception e) {
		             e.printStackTrace();
		         } 
		        return client;
		 }
	        private static final TransportClient INSTANCE= init(); 
	    } 

	public static final TransportClient getTransportClient() {
	        return Holder.INSTANCE;
	    }

}
