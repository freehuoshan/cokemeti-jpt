package com.cokemeti.jpt.util.es;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;

import org.apache.lucene.queryparser.xml.builders.BooleanQueryBuilder;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.highlight.HighlightBuilder;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.cokemeti.jpt.util.Constants;

public class ESClient {
	public final String NAME = "cluster.name";
	public final String SNIFF = "client.transport.sniff";
	
	// 缓冲池容量
    private  final int MAX_BULK_COUNT = 0;
    // 最大提交间隔（秒）
    private  final int MAX_COMMIT_INTERVAL = 20;

//	public String index;
//	public String type;
	public String clusterName; // 集群名称
	public String address;
	public int port;

	Lock commitLock = new ReentrantLock();
	BulkRequestBuilder bulkRequestBuilder = null;
	Client client = null;

	public ESClient(String clusterName, String address, int port) {
		this.clusterName = clusterName;
		this.address = address;
		this.port = port;
		init();
	}
	
	

	public void init() {
		Settings settings = Settings.settingsBuilder().put(NAME, clusterName)
				.put(SNIFF, true).build();
		try {
			client = TransportClient
					.builder()
					.settings(settings)
					.build()
					.addTransportAddress(
							new InetSocketTransportAddress(
									new InetSocketAddress(address, port)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bulkRequestBuilder = client.prepareBulk();
        bulkRequestBuilder.setRefresh(true);
//        Timer timer = new Timer();
//        timer.schedule(new CommitTimer(), 10 * 1000, MAX_COMMIT_INTERVAL * 1000);
	}

	public void close() {
		client.close();
	}
	
	//判断缓存池是否已满，批量提交
	public void bulkRequest(int threshold){
		 if (bulkRequestBuilder.numberOfActions() > threshold) {
			 System.out.println("start bulk......");
	            BulkResponse bulkResponse = bulkRequestBuilder.execute().actionGet();
	            if (!bulkResponse.hasFailures()) {
	                bulkRequestBuilder = client.prepareBulk();
	            }
	        }else{
	        	 System.out.println(bulkRequestBuilder.numberOfActions());
	        }
	}
	
	//加入索引请求到缓冲池
	public  void addUpdateBuilderToBulk(String index,String type,JSONObject json,String id) {
		 IndexRequestBuilder indexRequest = client.prepareIndex(index, type)  
		            //指定不重复的ID        
		            .setSource(json).setId(id);
//		UpdateRequestBuilder builder = client.prepareUpdate(index, type,id);
        commitLock.lock();
        try {
            bulkRequestBuilder.add(indexRequest);
            bulkRequest(MAX_BULK_COUNT);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            commitLock.unlock();
        }
    }

	//加入删除请求到缓冲池
	public  void addDeleteBuilderToBulk(String index,String type,List<String> list) {
        commitLock.lock();
        for(String id : list){
        	 DeleteRequestBuilder builder = client.prepareDelete(index, type, id);
        	 bulkRequestBuilder.add(builder);
        }
        try {
            bulkRequest(MAX_BULK_COUNT);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            commitLock.unlock();
        }
    }

	
	// 删除文档
	public boolean deleteDoc(String index,String type,String id) {
		DeleteResponse dresponse = client.prepareDelete(index, type, id).get();
		boolean isFound = dresponse.isFound(); // 文档存在返回true,不存在返回false；
		return isFound;
	}

	// 增加文档
	public boolean addDoc(String index,String type,String id, JSONObject json) {
		IndexResponse response = client.prepareIndex(index, type, id)
				.setSource(json.toJSONString()).get();
		return response.isCreated();
	}

	// 更新文档，没有现存文档则新建一份文档
	public boolean updateDoc(String index,String type,String id, JSONObject json) {
		UpdateResponse res = null;
		IndexRequest indexRequest = new IndexRequest(index, type, id)
				.source(json.toJSONString());
		UpdateRequest updateRequest = new UpdateRequest(index, type, id).doc(
				json).upsert(indexRequest);
		try {
			res = client.update(updateRequest).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if(null != res){
			return res.isCreated();
		}else{
			return false;
		}
	}
	
	//按ID查询单个文档
	public Map<String, Object> getDocById(String index,String type,String id){
		GetResponse response = client.prepareGet(index, type, id).get();
		return response.getSource();
	}
	
	
	public SearchHits search(String keyword , Integer from, Integer size){
		if (!StringUtils.isEmpty(keyword)) {
			BoolQueryBuilder qb2 =  QueryBuilders.boolQuery();
			String[] keywords = keyword.split("\\s+");
			for (String k : keywords) {
				qb2 = qb2.must(QueryBuilders.matchQuery("title", k));
			}
			SearchResponse response = client.prepareSearch(Constants.ES_DB)
					.setTypes(Constants.ES_TABLE)
					.setQuery(qb2)
					.addHighlightedField("workExp")
					.addHighlightedField("name")
					.addHighlightedField("company")
					.addHighlightedField("title")
					.setHighlighterRequireFieldMatch(false)
					.setHighlighterPreTags("<mark>")
					.setHighlighterPostTags("</mark>")
					.setFrom(from).setSize(size)
					.execute().actionGet();
			
			return  response.getHits();
		}
		return null;
	}
	
	public SearchHits MoHusearch(String keyword , Integer from, Integer size){
		if (!StringUtils.isEmpty(keyword)) {
			
			WildcardQueryBuilder query = QueryBuilders.wildcardQuery("_all", "*" + keyword + "*");
			
			SearchResponse response = client.prepareSearch(Constants.ES_DB)
					.setTypes(Constants.ES_TABLE)
					.setQuery(query).setFrom(from).setSize(size)
					.execute().actionGet();
			
			return  response.getHits();
		}
		return null;
	}

	class CommitTimer extends TimerTask {
        @Override
        public void run() {
            commitLock.lock();
            try {
                bulkRequest(MAX_BULK_COUNT);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                commitLock.unlock();
            }
        }
    }
	
}
