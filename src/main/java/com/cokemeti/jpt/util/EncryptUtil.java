package com.cokemeti.jpt.util;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import org.apache.commons.codec.binary.Hex;

/**
 * PBE 对称加解密
 *
 */
public class EncryptUtil {
	private static String ARITHMETIC = "PBEWithSHA1AndDESede";
	private static String SALTSTRING = "cokemeti";
	private static String PASSWORD = "easyfans_platform";
	private static int ITERATION_COUNT = 100;

	public static void main(String[] args) {
		
		System.out.println("Wang666 ----->"+EncryptUtil.PBEEncrypt("Wang666"));
		System.out.println("Shen666 ----->"+EncryptUtil.PBEEncrypt("Shen666"));
		System.out.println("Yong666 ----->"+EncryptUtil.PBEEncrypt("Yong666"));
		
		
		System.out.println(EncryptUtil.PBEDecrypt("2cc42965ff61100c"));
		System.out.println(EncryptUtil.PBEDecrypt("52ca6c7ee24afc35"));
		System.out.println(EncryptUtil.PBEDecrypt("cef933f9cd181497"));
	}

	public static String PBEEncrypt(String src) {

		try {
			byte[] salt = SALTSTRING.getBytes();

			PBEKeySpec pbeKeySpec = new PBEKeySpec(PASSWORD.toCharArray());
			SecretKeyFactory factory = SecretKeyFactory.getInstance(ARITHMETIC);
			Key key = factory.generateSecret(pbeKeySpec);

			// 加密
			PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, ITERATION_COUNT);
			Cipher cipher = Cipher.getInstance(ARITHMETIC);
			cipher.init(Cipher.ENCRYPT_MODE, key, pbeParameterSpec);
			byte[] result = cipher.doFinal(src.getBytes());
			return new String(Hex.encodeHex(result));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String PBEDecrypt(String src) {
		byte[] salt = SALTSTRING.getBytes();
		PBEKeySpec pbeKeySpec = new PBEKeySpec(PASSWORD.toCharArray());
		SecretKeyFactory factory;
		try {
			byte[] result = Hex.decodeHex(src.toCharArray());
			factory = SecretKeyFactory.getInstance(ARITHMETIC);
			Key key = factory.generateSecret(pbeKeySpec);
			PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, ITERATION_COUNT);
			Cipher cipher = Cipher.getInstance(ARITHMETIC);
			cipher.init(Cipher.ENCRYPT_MODE, key, pbeParameterSpec);
			// 解密
			cipher.init(Cipher.DECRYPT_MODE, key, pbeParameterSpec);
			result = cipher.doFinal(result);
			return new String(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
