package com.cokemeti.jpt.util;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import com.sun.mail.util.MailSSLSocketFactory;

public class MailUtils {
	// 发件人的 邮箱 和 密码（替换为自己的邮箱和密码）
/*    public static String myEmailAccount="";
    public static String myEmailPassword="12345678Aa";
    public static String smtpHost="smtp.exmail.qq.com";*/

    // 发件人邮箱的 SMTP 服务器地址, 必须准确, 不同邮件服务器地址不同, 一般格式为: smtp.xxx.com
    static Properties props =null;
    
    @SuppressWarnings("unchecked")
	public static void sendMail(Map<String,Object> Map, String myEmailAccount, String myEmailPassword,String smtpPort ,boolean isSsl){
    	Transport transport = null;
    	try{
    		// 1. 创建参数配置, 用于连接邮件服务器的参数配置
    		Properties props = new Properties();      
    		props.setProperty("mail.smtp.port", smtpPort);// 参数配置
    		props.setProperty("mail.transport.protocol", "smtp");   // 使用的协议（JavaMail规范要求）
    		props.setProperty("mail.host",(String)Map.get("stmpHost"));        // 发件人的邮箱的 SMTP 服务器地址
    		props.setProperty("mail.smtp.auth", "false");            // 请求认证，参数名称与具体实现有关
    		props.setProperty("mail.smtp.socketFactory.port", smtpPort);
    		if(isSsl){
    			MailSSLSocketFactory msf= new MailSSLSocketFactory();
                msf.setTrustAllHosts(true);
                props.put("mail.smtp.ssl.enable", "true");
                props.put("mail.smtp.ssl.socketFactory",msf);
    		}
    		
    		// 2. 根据配置创建会话对象, 用于和邮件服务器交互
    		Session session = Session.getDefaultInstance(props);
    		session.setDebug(true);      // 设置为debug模式, 可以查看详细的发送 log
    		
			List<String> accountList = (List<String>) Map.get("accountList");
			List<String> ccList = (List<String>) Map.get("ccList");
			List<String> bccList = (List<String>) Map.get("bccList");
			List<String> filepath = (List<String>) Map.get("filepath");
    		Map<String,String> cmap = (Map<String,String>) Map.get("content");
    		// 4. 根据 Session 获取邮件传输对象
    		transport = session.getTransport();
			// 3. 创建一封邮件
			MimeMessage message = createMimeMessage(session, myEmailAccount, accountList,ccList,bccList,filepath,cmap);
			// 5. 使用 邮箱账号 和 密码 连接邮件服务器
			// 这里认证的邮箱必须与 message 中的发件人邮箱一致，否则报错
			transport.connect(myEmailAccount, myEmailPassword);
			// 6. 发送邮件, 发到所有的收件地址, message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人, 抄送人, 密送人
			transport.sendMessage(message, message.getAllRecipients());
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}finally{
    		if(transport!=null){
    	        // 7. 关闭连接
    	        try {
					transport.close();
				} catch (MessagingException e) {
					e.printStackTrace();
				}
    		}
    	}
    }

    /**
     * 创建一封只包含文本的简单邮件
     *
     * @param session 和服务器交互的会话
     * @param sendMail 发件人邮箱
     * @param receiveMail 收件人邮箱
     * @return
     * @throws Exception
     */
    public static MimeMessage createMimeMessage(Session session, String sendMail, 
    		List<String> maillist,
    		List<String> cclist,
    		List<String> bcclist,
    		List<String> filepath,
    		Map<String,String> cmap) throws Exception {
        // 1. 创建一封邮件
        MimeMessage message = new MimeMessage(session);

        // 2. From: 发件人
        message.setFrom(new InternetAddress(sendMail, cmap.get("from")==null?"六度智囊":cmap.get("from"), "UTF-8"));

        // 3. To: 收件人（可以增加多个收件人、抄送、密送）
        message.setRecipients(MimeMessage.RecipientType.TO, getAddress(maillist));
        if(cclist!=null&&cclist.size() > 0){
        	message.setRecipients(MimeMessage.RecipientType.CC, getAddress(cclist));
        }
        if(bcclist!=null&&bcclist.size()>0){
        	message.setRecipients(MimeMessage.RecipientType.BCC, getAddress(bcclist));
        }
        // 4. Subject: 邮件主题
        message.setSubject(cmap.get("subject"), "UTF-8");

        // 5. Content: 邮件正文（可以使用html标签）
//        message.setContent(cmap.get("content"), "text/html;charset=UTF-8");

        // 邮件正文、附件
     // 后面的BodyPart将加入到此处创建的Multipart中  
        Multipart mp = new MimeMultipart();  
        // 附件操作  
        if (filepath != null && filepath.size() > 0) {  
            for (String filename : filepath) {  
                MimeBodyPart mbp = new MimeBodyPart();  
                // 得到数据源  
                FileDataSource fds = new FileDataSource(filename);  
                // 得到附件本身并至入BodyPart  
                mbp.setDataHandler(new DataHandler(fds));  
                // 得到文件名同样至入BodyPart  
                mbp.setFileName(MimeUtility.encodeWord(fds.getName()));
                mp.addBodyPart(mbp);  
            }  
            MimeBodyPart mbp = new MimeBodyPart();  
            mbp.setText(cmap.get("content"),"UTF-8");
            mp.addBodyPart(mbp);  
            // 移走集合中的所有元素  
            filepath.clear();  
            // Multipart加入到信件  
            message.setContent(mp);  
        }else{
        	message.setContent(cmap.get("content"), "text/html;charset=UTF-8");
        }
        // 6. 设置发件时间
        message.setSentDate(new Date());

        // 7. 保存设置
        message.saveChanges();

        return message;
    }
    
    /**
     * 
     * @param accountList 收件人列表
     * @param ccList	  抄送列表
     * @param bccList    密送列表
     * @param from		 来源名称
     * @param subject    主题
     * @param content    正文
     * @param filepath   附件列表
     */
    public static void sendEmail(List<String> accountList, 
    								List<String> ccList,
    								List<String> bccList,
    								String from,
    								String subject,
    								String content,
    								List<String> filepath, String smtpHost, String sender_account, String sender_password,
    								String smtpPort,boolean isSsl){
    	Map<String,Object> Map = new HashMap<String,Object>();
    	Map<String,String> contentMap = new HashMap<String,String>();
    	Map.put("smtp", "smtp");
    	Map.put("stmpHost",smtpHost);
    	Map.put("accountList", accountList);
    	Map.put("ccList", ccList);
    	Map.put("bccList", bccList);
    	contentMap.put("from", from);
    	contentMap.put("subject", subject);
    	contentMap.put("content", content);
    	Map.put("content", contentMap);
    	Map.put("filepath", filepath);
    	MailUtils.sendMail(Map,sender_account,sender_password, smtpPort,isSsl);
    }
    
    public static void main(String[] args) {
    	
	}
    
    private static InternetAddress[] getAddress(List<String> maillist) throws AddressException{
    	if (maillist.size() > 0 ) {
    		InternetAddress[] address = new InternetAddress[maillist.size()];
            for(int i = 0 ; i < maillist.size(); i++){
            	address[i] = new InternetAddress(maillist.get(i));
            }
            return address;
		}
    	return null;
    }
}
