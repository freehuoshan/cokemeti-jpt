/**
 * 
 */
package com.cokemeti.jpt.util.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * @author Administrator
 *
 */
public class EmailContentTemplateUtil {

	private static String proEmailContentName = "template/proEmail.txt";
	private static String proEmailEnglishContentName = "template/proEmail-english.txt";
	private static String customerEmailHeadName="template/customerEmail-head.txt";
	private static String customerEmailCenterName="template/customerEmail-center.txt";
	private static String customerEmailBottomName="template/customerEmail-bottom.txt";
	private static String customerEmailProfessionInfoCenterName = "template/customerEmail-professioninfo-content.txt";
	private static String proEmailPayInfoName = "template/professioin_pay_info.txt";
	private static String customerEmailProTypeName = "template/customerEmail-protype.txt";
	
	private static String proEmailContent;
	private static String proEmailEnglishContent;
	private static String customerEmailHead;
	private static String customerEmailCenter;
	private static String customerEmailBottom;
	private static String customerEmailProfessionInfoCenter;
	private static String proEmailPayInfo;
	private static String customerEmailProType;
	
	static{
		proEmailEnglishContent = readTemplate(proEmailEnglishContentName);
		proEmailContent = readTemplate(proEmailContentName);
		customerEmailHead = readTemplate(customerEmailHeadName);
		customerEmailCenter = readTemplate(customerEmailCenterName);
		customerEmailBottom = readTemplate(customerEmailBottomName);
		customerEmailProfessionInfoCenter = readTemplate(customerEmailProfessionInfoCenterName);
		proEmailPayInfo = readTemplate(proEmailPayInfoName);
		customerEmailProType = readTemplate(customerEmailProTypeName);
	}
	
	public static String getProEnglishEmailContentTemplate(){
		if(proEmailEnglishContent == null){
			proEmailEnglishContent = readTemplate(proEmailEnglishContentName);
		}
		return proEmailEnglishContent;
	}
	
	public static String getCustomerProTypeTemplate(){
		if(customerEmailProType == null){
			customerEmailProType = readTemplate(customerEmailProTypeName);
		}
		return customerEmailProType;
	}
	
	public static String getProPayInfoTemplate(){
		if(proEmailPayInfo == null){
			proEmailPayInfo = readTemplate(proEmailPayInfoName);
		}
		return proEmailPayInfo;
	}

	public static String getProTemplate(){
		if (proEmailContent == null) {
			proEmailContent = readTemplate(proEmailContentName);
		}
		return proEmailContent;
	}
	
	public static String getCustomerHead(){
		if (customerEmailHead == null) {
			customerEmailHead = readTemplate(customerEmailHeadName);
		}
		return customerEmailHead;
	}

	
	public static String getCustomerCenter(){
		if (customerEmailCenter == null) {
			customerEmailCenter = readTemplate(customerEmailCenterName);
		}
		return customerEmailCenter;
	}
	
	public static String getCustomProfessionInfoCenter(){
		if(customerEmailProfessionInfoCenter == null){
			customerEmailProfessionInfoCenter = readTemplate(customerEmailProfessionInfoCenterName);
		}
		return customerEmailProfessionInfoCenter;
	}
	
	public static String getCustomerBottom(){
		if (customerEmailBottom == null) {
			customerEmailBottom = readTemplate(customerEmailBottomName);
		}
		return customerEmailBottom;
	}
	
	private static String readTemplate(String filename){
        int len=0;
        StringBuffer str=new StringBuffer();
        try {
            InputStream is= EmailContentTemplateUtil.class.getClassLoader().getResourceAsStream(filename);
            if (is == null) {
				
			}else{
	            InputStreamReader isr= new InputStreamReader(is,"utf-8");
	            BufferedReader in= new BufferedReader(isr);
	            String line=null;
	            while( (line=in.readLine())!=null )
	            {
	                if(len != 0)  // 处理换行符的问题
	                {
	                    str.append("\r\n"+line);
	                }
	                else
	                {
	                    str.append(line);
	                }
	                len++;
	            }
	            in.close();
	            is.close();
			}
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return str.toString();
	}
	
	public static void main(String[] args) {
		String a = "abdjkadbsss$123$asdasd";
		a = a.replace("$123$", "111");
		System.out.println(a);
	}
}
