/**
 * 
 */
package com.cokemeti.jpt.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.alibaba.druid.util.StringUtils;
import com.cokemeti.jpt.domain.common.ExcelVo;
import com.cokemeti.jpt.domain.professional.ProfessionalWithBLOBs;
import com.cokemeti.jpt.service.professional.ProfessionalServices;
import com.csvreader.CsvReader;

/**
 * @author Administrator
 *
 */
public class ExcelUtil {
	static HSSFWorkbook workbook = null;
	
	 /** 
     * 创建新excel. 
     * @param fileDir  excel的输出路径 
     * @param sheetName 要创建的表格索引 
     * @param titleRow excel的第一行即表格头 
     */  
    public static void createExcel(String fileDir,String sheetName,String titleRow[]) throws Exception{  
        //创建workbook  
    	workbook = new HSSFWorkbook();  
        //添加Worksheet（不添加sheet时生成的xls文件打开时会报错)  
        HSSFSheet sheet1 = workbook.createSheet(sheetName);    
        //新建文件  
        FileOutputStream out = null;  
        try {  
            //添加表头  
            HSSFRow row = workbook.getSheet(sheetName).createRow(0);    //创建第一行    
            for(short i = 0;i < titleRow.length;i++){  
                HSSFCell cell = row.createCell(i);
                cell.setCellValue(titleRow[i]);  
            }  
            out = new FileOutputStream(fileDir);  
            workbook.write(out);  
        } catch (Exception e) {  
            throw e;
        } finally {    
            try {    
            	out.flush();
                out.close();    
                workbook = null;
            } catch (IOException e) {    
                e.printStackTrace();  
            }    
        }    
    }  
    
    /** 
     * 往excel中写入(已存在的数据无法写入). 
     * @param fileDir    文件路径 
     * @param sheetName  表格索引 
     * @param object 
     * @throws Exception 
     */  
    public static void writeToExcel(String fileDir,String sheetName,List<Map> mapList) throws Exception{  
    	 //创建workbook  
        File file = new File(fileDir);  
        byte[] buf = org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(file));//ex
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf); 
        try {  
        	workbook = new HSSFWorkbook(byteArrayInputStream);  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        //流  
        FileOutputStream out = new FileOutputStream(fileDir); 
        HSSFSheet sheet = workbook.getSheet(sheetName);  
        // 获取表格的总行数  
        // int rowCount = sheet.getLastRowNum() + 1; // 需要加一  
        // 获取表头的列数  
        int columnCount = sheet.getRow(0).getLastCellNum();  
        try {  
            // 获得表头行对象  
            HSSFRow titleRow = sheet.getRow(0);  
            if(titleRow!=null){ 
                for(int rowId=0;rowId<mapList.size();rowId++){
                    Map map = mapList.get(rowId);
                    HSSFRow newRow=sheet.createRow(rowId+1);
                    for (short columnIndex = 0; columnIndex < columnCount; columnIndex++) {  //遍历表头  
                        String mapKey = titleRow.getCell(columnIndex).toString().trim().toString().trim();  
                        HSSFCell cell = newRow.createCell(columnIndex);  
                        cell.setCellValue(map.get(mapKey)==null ? null : map.get(mapKey).toString());  
                    } 
                }
            }  
            workbook.write(out);  
        } catch (Exception e) {  
            throw e;
        } finally {    
            try {    
            	out.flush();
                out.close();
                workbook = null;
            } catch (IOException e) {    
                e.printStackTrace();  
            }    
        }    
    }  
	
	
    public static List<ExcelVo> readXml(String fileName){  
    	List<ExcelVo> list = new ArrayList<ExcelVo>();
        boolean isE2007 = false;    //判断是否是excel2007格式  
        if(fileName.endsWith("xlsx"))  
            isE2007 = true;  
        try {  
            InputStream input = new FileInputStream(fileName);  //建立输入流  
            Workbook wb  = null;  
            //根据文件格式(2003或者2007)来初始化  
            if(isE2007)  
                wb = new XSSFWorkbook(input);
            else  
                wb = new HSSFWorkbook(input);  
            Sheet sheet = wb.getSheetAt(0);     //获得第一个表单  
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.rowIterator(); //获得第一个表单的迭代器  
            while (rows.hasNext()) {  
                Row row = rows.next();  //获得行数据  
//                System.out.println("Row #" + row.getRowNum());  //获得行号从0开始  
                if(row.getRowNum()>0){
                	Iterator<Cell> cells = row.cellIterator();    //获得第一行的迭代器  
                    ExcelVo excelVo = new ExcelVo();
                    while (cells.hasNext()) {  
                        Cell cell = cells.next();  
                        switch(cell.getColumnIndex()){
                        case 0:
                        	excelVo.setName(formatter.formatCellValue(cell));
                        	break;
                        case 1:
                        	excelVo.setTitle(formatter.formatCellValue(cell));
                        	break;
                        case 2:
                        	excelVo.setCompany(formatter.formatCellValue(cell));
                        	break;
                        case 3:
                        	excelVo.setEmail(formatter.formatCellValue(cell));
                        	break;
                        case 4:
                        	excelVo.setTel(formatter.formatCellValue(cell));
                        	break;
                        case 5:
                        	excelVo.setPrice(formatter.formatCellValue(cell));
                        	//excelVo.setPrice(String.valueOf(cell.getNumericCellValue()));
                        	break;  
                        case 6:
                        	excelVo.setWorkexp(formatter.formatCellValue(cell));
                        	break;
                        case 7:
                        	excelVo.setDesc(formatter.formatCellValue(cell));
                        	break;
                        default:
                        	break;
                        }
                    } 
                    System.out.println(excelVo);
                    list.add(excelVo);
                }
            }  
        } catch (IOException ex) {  
            ex.printStackTrace();  
        }
		return list;  
    }  
    
    
    public static List<ExcelVo> readCsv(String fileName){  
    	List<ExcelVo> list = new ArrayList<ExcelVo>();  
    	CsvReader reader = null;  
    	try   
    	{  
	    	//初始化CsvReader并指定列分隔符和字符编码  
	    	reader = new CsvReader(fileName, ',', Charset.forName("UTF-8"));
	    	int i = 0;
	    	while (reader.readRecord())   
	    	{  
		    	//读取每行数据以数组形式返回  
		    	String[] str = reader.getValues();  
		    	if (str != null && str.length > 0 && i > 0)   
		    	{  
		    		if (str[0] != null && !"".equals(str[0].trim()))   
		    		{  
		    			if (str.length == 8) {
		    				ExcelVo vo = new ExcelVo(str[0],str[1],str[2],str[3],str[4],str[5],str[6],str[7]);
		    				list.add(vo);
						}
		    		}  
		    	} 
		    	i++;
	    	}  
    	}catch (FileNotFoundException e) {  
    	}   
    	catch (IOException e)   
    	{  
    	}  
    	finally 
    	{  
	    	if(reader != null)  
	    		reader.close();  
    	}  
    	return list;
    }
    
    public static void main(String[] args) {
    	String[] totalRow = {"专家名字" , "职位" , "公司" , "邮件" , "电话" , "费用" , "专家简介" };
    	List<Map> repeatData = new ArrayList<>();
    	Map<String,Object> repeatMap = new HashMap<>();
		repeatMap.put("专家名字", "123");
		repeatMap.put("职位", "123");
		repeatMap.put("公司", "123");
		repeatMap.put("邮件", "123");
		repeatMap.put("电话", "123");
		repeatMap.put("费用", "123");
		repeatMap.put("专家简介", "123");
		repeatData.add(repeatMap);
     	try {
			ExcelUtil.createExcel("/home/huoshan/jpt_img/ces4.xls", "重复", totalRow);
			ExcelUtil.writeToExcel("/home/huoshan/jpt_img/ces4.xls", "重复", repeatData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
