/**
 * 
 */
package com.cokemeti.jpt.util;

/**
 * @author Administrator
 *
 */
public class Constants {

	public static String SESSION_USERNAME = "username";
	public static String SESSION_ID = "uid";
	public static String SESSION_EMAIL = "email";
	public static String SESSION_URL = "url";
	public static String SESSION_SEARCH_WORD = "searchWord";
	public static String SESSION_SEARCH_PAGENO = "pageNo";
	
	
	public static String ANALYST_INDEX = "/analyst/project/list";
	
	public static String ES_DB = "jpt";
	public static String ES_TABLE = "professional";
	public static String ES_clusterName="ckmt-elk";
	public static String ES_address="172.19.45.110";
	public static int ES_port=9300;
	
	public static String PROJECT_REPORT_DIR = "/report";
	
	public static int PROJECT_STATUS_ONE = 1;

	public static int PROJECT_REPORT_STATUS_UPLOAD = 1;
	
	public static int PROJECT_PROFESSINOAL_STATUS_SENDPROEMAIL_NO=0; //未发送专家邮件
	public static int PROJECT_PROFESSINOAL_STATUS_SENDPROEMAIL=10;	 //已发专家邮件
	public static int PROJECT_PROFESSINOAL_STATUS_AGREE=11;			 //专家同意
	public static int PROJECT_PROFESSINOAL_STATUS_DISAGREE=-1;		//专家拒绝
	public static int PROJECT_PROFESSINOAL_STATUS_SENDCUSTOMEREMAIL = 20; //发送客户邮件
	
//	public static String TERMS_URL = "http://www.cokemeti.com/conditions.htm";
	public static String TERMS_URL = "http://qingli.website/test/condition";
}
