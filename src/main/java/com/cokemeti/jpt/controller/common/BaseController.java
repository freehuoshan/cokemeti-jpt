package com.cokemeti.jpt.controller.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @author xujunj
 *
 */
public class BaseController {
	
	@Value("${img.url}")
	protected String COKEMETI_IMG_URL;
	
	@Value("${project.url}")
	protected String COKEMETI_URL;
	
	@Value("${sixdegree.url}")
	protected String SIXDEGREE_URL;
	
	@Autowired
	public HttpServletRequest request; 
	@Autowired
	private HttpServletResponse response;
	
	protected static final String ERRORPAGE = "error/msg";
	
	public SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:m:s");
	/**
	 * http response输出json
	 * @param obj
	 */
	public void outHttpJson(HttpServletResponse response, Object obj) {
		String outMsg = JSONObject.toJSONString(obj,SerializerFeature.WriteMapNullValue);
		response.setContentType("application/json;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		outHttpString(response, outMsg);
	}
	
	/**
	 * http response输出String
	 * @param obj
	 */
	public void outHttpString(HttpServletResponse response, Object obj) {
		try {
			response.setCharacterEncoding("utf-8");
			PrintWriter out = response.getWriter();
			out.write(String.valueOf(obj).toCharArray());
			out.flush();
		} catch (IOException e) {
		}
	}
	
	/**
	 * get值 request
	 * 
	 * @param key
	 * @return
	 */
	public String get(String key) {
		return request.getParameter(key);
	}
	
	/**
	 * 获取session
	 * 
	 * @return
	 */
	public HttpSession getSession() {
		return request.getSession();
	}
}
