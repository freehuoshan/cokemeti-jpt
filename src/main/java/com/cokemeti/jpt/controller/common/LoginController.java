package com.cokemeti.jpt.controller.common;


import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cokemeti.jpt.domain.admin.User;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.service.admin.UserService;
import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.EncryptUtil;

//登录功能（写请求头）
@Controller
@RequestMapping("/")
public class LoginController extends BaseController{
	//logger
	private Logger logger = Logger.getLogger(LoginController.class);
	
	@Value("${file.url}")
	private String url;
	
	@Autowired
	private UserService userService;
	
	/**
	 * 登陆页
	 * @return
	 */
	@RequestMapping(value = "login",method = RequestMethod.GET)
	public ModelAndView loginGet() {
		return new ModelAndView("common/login");
	}
	
	@RequestMapping(value = "user/to_list")
	public String toUserList(){
		return "analyst/user_list";
	}
	
	@RequestMapping(value = "user/to_add")
	public String toUserAdd(Model model){
		model.addAttribute("type", 0);
		return "analyst/user_add";
	}
	
	@RequestMapping(value = "user/add" , method = RequestMethod.POST)
	public void add(User user , HttpServletRequest request , HttpServletResponse response){
		user.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
		user.setStatus(1);
		user.setPassword(EncryptUtil.PBEEncrypt(user.getPassword()));
		Integer count = userService.addUser(user);
		outHttpString(response, count > 0 ? true : false);
	}
	@RequestMapping(value = "user/update",method = RequestMethod.POST)
	public void update(User user , HttpServletRequest request , HttpServletResponse response){
		user.setUpdateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
		user.setPassword(EncryptUtil.PBEEncrypt(user.getPassword()));
		Integer count = userService.updateUser(user);
		outHttpString(response, count > 0 ? true : false);
	}
	
	@RequestMapping(value = "/user/checkRepeat")
	public void checkRepeat(User user , HttpServletRequest request , HttpServletResponse response){
		Boolean isExist = userService.checkRepeat(user);
		outHttpString(response, isExist);
	}
	
	
	@RequestMapping(value = "user/edit/{id}")
	public String toEditUser(Model model,@PathVariable(value = "id") String id){
		model.addAttribute("type", 1);
		User user = userService.getUserById(Integer.valueOf(id));
		String userPassword = EncryptUtil.PBEDecrypt(user.getPassword());
		user.setPassword(userPassword);
		model.addAttribute("vo", user);
		return "analyst/user_add";
	}
	
	@RequestMapping(value = "user/delete")
	public void deleteById(User user , HttpServletResponse response){
		Integer count = userService.delete(user);
		outHttpString(response, count > 0 ? "成功" : "失败");
	}
	
	@RequestMapping(value = "user/list",method = RequestMethod.POST)
	public void userList(@RequestParam(value="search[value]" ,defaultValue="") String search,
			@RequestParam(value="start",defaultValue = "0") String start,
			@RequestParam(value="length",defaultValue = "10") String length,
			@RequestParam(value="id",required=false) String id,
			@RequestParam(value="draw") int draw,
			HttpServletRequest request,
			HttpServletResponse response){
		
		DataTablesVo vo = userService.getUsers(id,search, start, length, draw);
		outHttpJson(response, vo);
		
	}
	
	/**
	 * 登陆
	 * @return
	 */
	@RequestMapping(value = "login",method = RequestMethod.POST)
	public void login(@RequestParam(required=true)String password , 
			@RequestParam(required=true)String username, 
			HttpServletRequest request,
			HttpServletResponse response,
			Model model
			) {
		logger.info("login in : " + username + " : " + password);
		User user = userService.getUserByUsername(username);
		String redirect_url = "/login";
		if (user != null) {
			if (password.equals(EncryptUtil.PBEDecrypt(user.getPassword()))) {
				HttpSession session = request.getSession();
				session.setAttribute(Constants.SESSION_EMAIL, user.getEmail());
				session.setAttribute(Constants.SESSION_ID, user.getId());
				session.setAttribute(Constants.SESSION_USERNAME, user.getUsername());
				session.setAttribute(Constants.SESSION_URL, url);
				session.setAttribute("role", user.getRole());
				
				switch (user.getRole().intValue()) {
				case 1:
					redirect_url = Constants.ANALYST_INDEX;
				case 2:
					redirect_url = "/sales/index";
				case 3:
					redirect_url = Constants.ANALYST_INDEX;
				default:
					break;
				}
			}else{
				model.addAttribute("msg", "用户名或密码不正确");
			}
		}else{
			model.addAttribute("msg", "用户不存在");
		}
		try {
			response.sendRedirect(request.getContextPath()+redirect_url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "index",method = RequestMethod.GET)
	public ModelAndView index() {
		return new ModelAndView("sales/index");
	}

	/**
	 * 登出页面
	 * @param request
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute(Constants.SESSION_ID);
	//	session.removeAttribute("imgurl");
		session.invalidate();
		return new ModelAndView("common/login");
	}
	
	/*@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request,
			@RequestParam(value = "userName", required = true) String username,
			@RequestParam(value = "password", required = true) String password)
			throws IOException {
		HttpSession session = request.getSession();
		// 此处调用业务层根据用户名判断数据库是否存在数据这里检查用户名密码没问题接着看下一步
		User user1 = userService.checkInfo(username, password);
		if (user1 != null) {
			logger.info("用户id:" + user1.getId() +"登陆。");
			session.setAttribute("uid", user1.getId());
			session.setAttribute("imgurl", imgurl);
			return new ModelAndView("page/back/news/newsList");
		} else {
			return new ModelAndView("page/login").addObject("error", "用户名或者密码不正确");
		}
	}*/
}
