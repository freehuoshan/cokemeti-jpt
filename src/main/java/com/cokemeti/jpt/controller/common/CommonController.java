package com.cokemeti.jpt.controller.common;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CommonController extends BaseController{
	//logger
	private Logger logger = Logger.getLogger(CommonController.class);

	@RequestMapping(value = "/error/{code}",method = RequestMethod.GET)
	public ModelAndView error(@PathVariable String code) {
		return new ModelAndView("error/"+code);
	}
}
