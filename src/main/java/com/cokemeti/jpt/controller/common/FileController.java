/**
 * 
 */
package com.cokemeti.jpt.controller.common;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.UUIDUtil;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/file")
public class FileController extends BaseController {

	@Value("${file.path}")
	private String filePath;
	
	@Value("${file.url}")
	private String fileUrl;
	
	@RequestMapping(value="/upload", method = RequestMethod.POST)
	public void upload(MultipartHttpServletRequest request, HttpServletResponse response){
		String msg = "0";
		/** 文件信息 **/
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartRequest.getFile("file");
		String fileName = file.getOriginalFilename(); // 获得文件名
		
		String ext = fileName.substring(fileName.lastIndexOf(".") + 1); // 后缀
		String name = fileName.substring(0, fileName.lastIndexOf("."));
		
		String newFileName = UUIDUtil.getUUID()+ "."+ext;
		
		//创建目录
		File pathDir = new File(filePath);
		if (!pathDir.exists()) {
			pathDir.mkdirs();
		}
		
		File newFile = new File(pathDir + "/"+ newFileName);
		try {
			FileCopyUtils.copy(file.getBytes(), newFile);
//			msg = Constants.PROJECT_REPORT_DIR + "/" + newFileName;
			msg = newFileName;
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(msg);
		outHttpString(response, msg);
	}
	
}
