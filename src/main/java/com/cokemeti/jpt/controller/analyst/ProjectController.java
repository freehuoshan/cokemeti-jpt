/**
 * 
 */
package com.cokemeti.jpt.controller.analyst;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.cokemeti.jpt.controller.common.BaseController;
import com.cokemeti.jpt.domain.admin.User;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.common.Email;
import com.cokemeti.jpt.domain.professional.ProfessionType;
import com.cokemeti.jpt.domain.project.Project;
import com.cokemeti.jpt.domain.project.ProjectInfo;
import com.cokemeti.jpt.domain.project.ProjectProfessional;
import com.cokemeti.jpt.domain.sales.vo.CustomerVo;
import com.cokemeti.jpt.service.admin.UserService;
import com.cokemeti.jpt.service.common.EmailService;
import com.cokemeti.jpt.service.professional.ProfessionTypeService;
import com.cokemeti.jpt.service.project.ProjectService;
import com.cokemeti.jpt.service.sales.SalesService;
import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.JsonUtil;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/analyst/project")
public class ProjectController extends BaseController {
	
	@Autowired
	private ProjectService pjs;
	@Autowired
	private SalesService ss;
	@Autowired
	private EmailService es;
	@Autowired
	private UserService userService;
	@Autowired
	private ProfessionTypeService professionTypeService;
	
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public String toProjectList(Model model){
		//List<Project> list = pjs.getProjects();
		//model.addAttribute("list", list);
		return "analyst/project_list";
	}
	
	@RequestMapping(value="/list", method = RequestMethod.POST)
	public void getProjectList(@RequestParam(value="search[value]" ,defaultValue="") String search,
			@RequestParam(value="start",defaultValue = "0") String start,
			@RequestParam(value="length",defaultValue = "10") String length,
			@RequestParam(value="customerid",required=false) String id,
			@RequestParam(value="draw") int draw,
			HttpServletRequest request,
			HttpServletResponse response){
		
		DataTablesVo vo = pjs.getProjects(id,search, start, length, draw);
		outHttpJson(response, vo);
	}
	
	@RequestMapping(value="/{id}",method = RequestMethod.GET)
	public String projectDetail(@PathVariable String id, Model model, HttpServletRequest request){
		if (!StringUtils.isEmpty(id)) {
			Project project = pjs.getProject(id);
			User createUser = userService.getUserById(project.getCreateUser());
			if (project != null) {
				CustomerVo customer = ss.getCustomerInfoByid(project.getCustomerId());
				model.addAttribute("c", customer);
			}
			model.addAttribute("p", project);
			model.addAttribute("createUser", createUser);
			model.addAttribute("type", 1);
		}
		return "analyst/project_add";
	}
	
	@RequestMapping(value="/delete" , method = RequestMethod.POST)
	public void deleteById(@RequestParam(value="projectId") String projectId, HttpServletResponse response){
		
		boolean isDelete = pjs.deleteById(projectId);
		
		outHttpString(response, isDelete ? "success" : "fail");
	}
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public String toProjectAdd(Model model){
		model.addAttribute("type", 0);
		return "analyst/project_add";
	}
	
	
	@RequestMapping(value="/addOrUpdate", method=RequestMethod.POST)
	public void addOrUpdatepProject(Project project,HttpServletRequest request, HttpServletResponse response){
		String msg = "";
		int code = 0;
		try {
			code = pjs.addOrUpdateProject(project,(Integer)request.getSession().getAttribute(Constants.SESSION_ID));
			msg = "保存成功";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败，请重试";
		}
		outHttpJson(response, JsonUtil.getJson(code, msg));
	}
	
	@RequestMapping(value="/professionals", method=RequestMethod.POST)
	public void getProfessionalList(
			@RequestParam(value="projectId") String projectId,
			HttpServletRequest request, 
			HttpServletResponse response,
			Model model){
		outHttpJson(response, pjs.getProfessionalList(projectId));
	}

	@RequestMapping(value="/professional/add", method=RequestMethod.POST)
	public void addProfessional(ProjectProfessional pp,
			HttpServletRequest request, 
			HttpServletResponse response,
			Model model){
		try {
			pp.setStatus(Constants.PROJECT_PROFESSINOAL_STATUS_SENDPROEMAIL_NO);
			pjs.addOrUpdateProjectProfessional(pp, (Integer)request.getSession().getAttribute(Constants.SESSION_ID));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			response.sendRedirect(request.getContextPath()+"/analyst/project/"+pp.getProjectId());
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	@RequestMapping(value="/mailcheck/{type}/{token}", method=RequestMethod.GET)
	public String returnMailResult(
			@PathVariable String type,
			@PathVariable String token,
			HttpServletRequest request, 
			HttpServletResponse response,
			Model model){
		try {
			pjs.updateProjectProfessionalStatus(type, token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(Constants.PROJECT_PROFESSINOAL_STATUS_AGREE == Integer.valueOf(type)){
			return "redirect:" + SIXDEGREE_URL+"/agree";
		}
		else{
			return "redirect:" + SIXDEGREE_URL + "/reject";
		}
		
	}
	
	@RequestMapping(value="/condition")
	public String toConditioin(){
		return "redirect:" + SIXDEGREE_URL + "/condition";
	}
	
	@RequestMapping(value="/requirement/add" , method = RequestMethod.POST)
	public void addRequirement(@RequestParam(value="info")String info, 
				@RequestParam(value="projectId") String projectId,
				HttpServletResponse response,
				HttpServletRequest request, Model model){
		String msg = "";
		int code = 0;
		if (!StringUtils.isEmpty(info)) {
			if (!StringUtils.isEmpty(projectId)) {
				ProjectInfo pi = new ProjectInfo();
				pi.setContent(info);
				pi.setProjectId(Integer.valueOf(projectId));
				pi.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
				pi.setCreateUser((Integer)request.getSession().getAttribute(Constants.SESSION_ID));
				
				try {
					pjs.addProjectInfo(pi);
					code = 1;
					msg = "保存成功";
				} catch (Exception e) {
					e.printStackTrace();
					msg = "保存失败，请重试";
				}
			}else {
				msg = "请先保存项目基本信息";
			}
		}else{
			msg = "请填写需求描述";
		}
		outHttpJson(response, JsonUtil.getJson(code, msg));
	}
	
	@RequestMapping(value="/requirements", method = RequestMethod.POST)
	public void getRequirements(@RequestParam(value="projectId") String projectId,
			//@RequestParam(value="draw") int draw,
			HttpServletResponse response,
			HttpServletRequest request, Model model){
		DataTablesVo vo = null;
		if (!StringUtils.isEmpty(projectId)) {
			vo =  pjs.getProjectInfos(Integer.valueOf(projectId));
		}
		outHttpJson(response, vo);
	}
	
	@RequestMapping("/requirements/delete")
	public void deleteRequireById(@RequestParam(value="requireId") String requireId , 
			HttpServletResponse response){
		Boolean isDelete = pjs.deleteRequireById(requireId);
		outHttpString(response, isDelete ? "成功" : "失败"); 
	}
	
	@RequestMapping(value = "/requirement/update", method=RequestMethod.POST)
	public void updateRequireById(@RequestParam(value="requireId") String requireId ,
			@RequestParam(value="description") String description , HttpServletResponse response){
		Boolean isUpdate = pjs.updateRequireById(requireId,description);
		outHttpString(response, isUpdate ? "成功" : "失败"); 
	}
	
	@RequestMapping(value="/reports/add", method=RequestMethod.POST)
	public void saveReports(@RequestParam(value="projectId") String projectId,
			@RequestParam(value="ppId") String ppId,
			@RequestParam(value="reports") String reports,
			@RequestParam(value="filenames") String filenames,
			@RequestParam(value="reportType") String reportType,
			HttpServletResponse response,
			HttpServletRequest request,
			Model model){
		
		String msg = "";
		int code = 0;
		if(!StringUtils.isEmpty(reports) && !StringUtils.isEmpty(projectId)){
			try {
				pjs.addProjectReports(reports, filenames, projectId, ppId, reportType, (Integer)request.getSession().getAttribute(Constants.SESSION_ID));	
				code = 1;
				msg = "保存成功";
			} catch (Exception e) {
				e.printStackTrace();
				msg = "保存失败，请重试";
			}
			outHttpJson(response, JsonUtil.getJson(code, msg));
		}
	}
	
	@RequestMapping(value="/reports", method=RequestMethod.POST)
	public void getReports(@RequestParam(value="projectId") String projectId,
			HttpServletResponse response,
			HttpServletRequest request, Model model){
		DataTablesVo vo = null;
		if (!StringUtils.isEmpty(projectId)) {
			vo =  pjs.getReports(Integer.valueOf(projectId));
		}
		outHttpJson(response, vo);
	}
	
	@RequestMapping(value="reports/delete", method=RequestMethod.POST)
	public void deleteReportByReportId(@RequestParam(value="reportId") String reportId,
			HttpServletResponse response){
		Boolean isDelete = pjs.deleteReportByReportId(reportId);
		outHttpString(response, isDelete ? "成功" : "失败"); 
	}
	
	//获取给单个专家发送邮件的模板
	@RequestMapping(value="/professional/email_info", method=RequestMethod.POST)
	public void getPpEmailInfo(@RequestParam(value="ppId") String ppId,
			HttpServletResponse response,
			HttpServletRequest request){
		
		Email email = pjs.getPpEmailInfo(Integer.valueOf(ppId), (String)request.getSession().getAttribute(Constants.SESSION_EMAIL), COKEMETI_URL);
		outHttpJson(response, email);
	}
	
	//获取给单个专家发送邮件的模板英文
	@RequestMapping(value="/professional/email_info_english", method=RequestMethod.POST)
	public void getPpEmailInfoEnglish(@RequestParam(value="ppId") String ppId,
			HttpServletResponse response,
			HttpServletRequest request){
		
		Email email = pjs.getPpEmailInfoEnglish(Integer.valueOf(ppId), (String)request.getSession().getAttribute(Constants.SESSION_EMAIL), COKEMETI_URL);
		outHttpJson(response, email);
	}
	
	@RequestMapping(value="/professional/email_payinfo" , method=RequestMethod.POST)
	public void getPpEmailPayInfo(@RequestParam(value="ppId") String ppId, HttpServletResponse response,
			HttpServletRequest request){
		
		Email email = pjs.getPpEmailPayInfo(Integer.valueOf(ppId), (String)request.getSession().getAttribute(Constants.SESSION_EMAIL), (String)request.getSession().getAttribute(Constants.SESSION_USERNAME));
		outHttpJson(response, email);
	}
	
	//更改PP状态
	@RequestMapping(value="/professional/status", method=RequestMethod.POST)
	public void updatePpStatus(@RequestParam(value="ppId") String ppId,
			@RequestParam(value="customerStatus") int status,
			HttpServletResponse response,
			HttpServletRequest request){
		String msg = "";
		int code = 0;
		try {
			pjs.updatePpCustomerStatus(Integer.valueOf(ppId), status);
			code = 1;
			msg = "更新成功";
		} catch (NumberFormatException e) {
			e.printStackTrace();
			msg = "更新失败，请重试";
		} catch (Exception e) {
			e.printStackTrace();
		}
		outHttpJson(response, JsonUtil.getJson(code, msg));
	}
	
	
	//获取给客户发送邮件的模板
	@RequestMapping(value="/customer/email_info", method=RequestMethod.POST)
	public void getPpsEmailInfo(@RequestParam(value="projectId") String projectId,@RequestParam("ppId") String ppId,
			HttpServletResponse response,
			HttpServletRequest request){
			
		Email email = pjs.getCustomerEmailInfo(Integer.valueOf(projectId), (String)request.getSession().getAttribute(Constants.SESSION_EMAIL),"profession_list",ppId);
		outHttpJson(response, email);
	}
	//获取给客户发送专家详情
	@RequestMapping(value="/profession_info/email_info",method=RequestMethod.POST)
	public void getPpsInfoEmailInfo(@RequestParam(value="projectId") String projectId,@RequestParam("ppId") String ppId,
			HttpServletResponse response,
			HttpServletRequest request){
			
		Email email = pjs.getCustomerEmailInfo(Integer.valueOf(projectId), (String)request.getSession().getAttribute(Constants.SESSION_EMAIL),"profession_info",ppId);
		outHttpJson(response, email);
	}
	
	@RequestMapping(value="/professional_email/add", method=RequestMethod.POST)
	public void sendProfessionalList(Email email,
			@RequestParam(value = "type")int type,
			@RequestParam(value="operatype")int operatype,
			@RequestParam(value = "ppId",required=false)String ppId,
			@RequestParam(value = "projectId")String projectId,
			HttpServletResponse response,
			HttpServletRequest request){
		String msg = "";
		int code = 0;
		try{
			es.sendEmail(email, (Integer)request.getSession().getAttribute(Constants.SESSION_ID),type,operatype,projectId,ppId);
			code = 1;
			msg = "发送成功";
		}catch (Exception e) {
			e.printStackTrace();
			msg = "发送失败，请重试";
		}
		outHttpJson(response, JsonUtil.getJson(code, msg));
	}
	
	/**
	 * 校验分类是否重复
	 * @param professionType
	 * @param response
	 */
	@RequestMapping(value="/profession_type/checkRepeat", method=RequestMethod.POST)
	@ResponseBody
	public String checkProfessionTypeRepeat(ProfessionType professionType , HttpServletResponse response){
		
		Boolean isRepeat = professionTypeService.checkTypeRepeat(professionType);
		return isRepeat ? "true" : "false";
	}
	/**
	 * 添加分类
	 * @param professionType
	 * @param response
	 */
	@RequestMapping(value="/profession_type/add", method=RequestMethod.POST)
	public void professionTypeAdd(ProfessionType professionType, HttpServletResponse response,
			HttpServletRequest request){
		professionType.setCreateUser((Integer)request.getSession().getAttribute(Constants.SESSION_ID));
		Boolean isCreated= professionTypeService.addProfessionType(professionType);
		outHttpString(response, isCreated ? "true" : "false"); 
	}
	
	/**
	 * 编辑professionType
	 * @param professionType
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/profession_type/update", method=RequestMethod.POST)
	public void professionTypeUpdate(ProfessionType professionType , HttpServletResponse response,
			HttpServletRequest request){
		professionType.setUpdateUser((Integer)request.getSession().getAttribute(Constants.SESSION_ID));
		Boolean isUpdated = professionTypeService.updateProfessionType(professionType);
		outHttpString(response, isUpdated ? "true" : "false"); 
	}
	
	/**
	 * 专家分类列表
	 * @param response
	 */
	@RequestMapping(value="/profession_type/list", method=RequestMethod.POST)
	@ResponseBody
	public void professionTypeList(ProfessionType professionType,HttpServletResponse response){
		List<ProfessionType> professionTypeList = professionTypeService.getProfessionList(professionType);
		outHttpJson(response, professionTypeList);
	}
	
	@RequestMapping(value="/profession_type/delete", method=RequestMethod.POST)
	@ResponseBody
	public void deleteProfessionByType(ProjectProfessional projectProfession , HttpServletResponse response){
		Boolean isdelete = professionTypeService.deleteProfessionByType(projectProfession);
		outHttpString(response, isdelete ? "成功" : "失败"); 
	}
	
	@RequestMapping(value="/mailcheck/yes")
	public String toYes(){
		return "analyst/invitation_yes";
	}
	
	@RequestMapping(value="/mailcheck/no")
	public String toNo(){
		return "analyst/invitation_no";
	}
	
	
	
	
}
