/**
 * 
 */
package com.cokemeti.jpt.controller.analyst;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cokemeti.jpt.controller.common.BaseController;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.professional.Professional;
import com.cokemeti.jpt.domain.professional.ProfessionalWithBLOBs;
import com.cokemeti.jpt.domain.professional.ProfessionalWork;
import com.cokemeti.jpt.domain.project.ProjectProfessional;
import com.cokemeti.jpt.service.professional.ProfessionalServices;
import com.cokemeti.jpt.service.professional.ProfessionalWorkService;
import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.JsonUtil;
import com.cokemeti.jpt.util.ResultBean;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/analyst/professional")
public class ProfessionalController extends BaseController {
	private Logger logger = Logger.getLogger(ProfessionalController.class);
	
	@Value("${file.path}")
	public String filepath;
	
	@Value("${file.url}")
	private String fileUrl;
	
	@Autowired
	private ProfessionalWorkService pws;
	
	@Autowired
	private ProfessionalServices ps;
	
	@RequestMapping(value="/index")
	public void index(HttpServletResponse response , HttpServletRequest request){
		try {
			response.sendRedirect(request.getContextPath()+"/analyst/professional/detail");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public String toSearchPage(HttpServletResponse response , HttpServletRequest request){
		return "analyst/professional_search";
	}
	
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public void searchProfessionals(
			@RequestParam String keyword,
			@RequestParam(defaultValue = "1") Integer pageNo,
			HttpServletResponse response , HttpServletRequest request){
		request.getSession().setAttribute(Constants.SESSION_SEARCH_WORD, keyword);
		request.getSession().setAttribute(Constants.SESSION_SEARCH_PAGENO, pageNo);
		DataTablesVo vo = ps.searchProfessional(keyword, (pageNo-1)*10, 10);
		outHttpJson(response, JsonUtil.getJson(1,"",vo));
	}
	
	@RequestMapping(value="/search/mohu", method=RequestMethod.POST)
	public void MohuearchProfessionals(
			@RequestParam String keyword,
			@RequestParam(defaultValue = "1") Integer pageNo,
			HttpServletResponse response , HttpServletRequest request){
		request.getSession().setAttribute(Constants.SESSION_SEARCH_WORD, keyword);
		request.getSession().setAttribute(Constants.SESSION_SEARCH_PAGENO, pageNo);
		DataTablesVo vo = ps.MoHuSearchProfessional(keyword, (pageNo-1)*10, 10);
		outHttpJson(response, JsonUtil.getJson(1,"",vo));
	}
	
	@RequestMapping(value="/{id}")
	public void getProfessionalInfoById(@PathVariable int id, HttpServletResponse response){
		ProfessionalWithBLOBs professional = ps.getProfessionalById(id);
		int code = 0;
		Object data = null;
		if (professional != null) {
			code = 1;
			data = professional;
		}
		outHttpJson(response, JsonUtil.getJson(code, "", data));
	}
	
	@RequestMapping(value="/detail/{id}")
	public String toProfessionalDetail(@PathVariable int id, Model model){
		ProfessionalWithBLOBs professional = ps.getProfessionalById(id);
		model.addAttribute("professional", professional);
		return "analyst/professional_detail";
	}
	
	@RequestMapping(value="/tosingleadd")
	public String toAddProfession(){
		return "analyst/professional_detail";
	}
	
	@RequestMapping(value="/workexps", method=RequestMethod.POST)
	public void getPorfessionalWorks(@RequestParam int professionalId, HttpServletResponse response){
		List<ProfessionalWork> list = pws.getProfessionalWorks(professionalId);
		DataTablesVo vo = new DataTablesVo(1, list.size(), list.size(), list);
		logger.info("**********************" + JSON.toJSONString(vo));
		outHttpJson(response, vo);
	}
	
	@RequestMapping(value="/projects", method=RequestMethod.POST)
	public void getProfessionalList(
			@RequestParam(value="professionalId") int professionalId,
			HttpServletRequest request, 
			HttpServletResponse response,
			Model model){
		
		DataTablesVo vo = ps.getProjectsList(professionalId);
		outHttpJson(response, vo);
	}
	
	@RequestMapping(value="/info/save", method=RequestMethod.POST)
	public void saveProfessionalInfo(ProfessionalWithBLOBs professional, HttpServletRequest request,
			HttpServletResponse response){
		String msg = "0";
		if(professional != null){
			professional.setUpdateUser((Integer)request.getSession().getAttribute(Constants.SESSION_ID));
			professional.setUpdateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
			try {
				ps.updateProfessionalInfo(professional);
				msg = "1";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		outHttpString(response, msg);
	}
	
	@RequestMapping(value = "isexist" , method = RequestMethod.POST)
	public void isExist(ProfessionalWithBLOBs professional, HttpServletRequest request,
			HttpServletResponse response){
		outHttpJson(response, ps.isExist(professional)); 
	}
	
	@RequestMapping(value="/singleadd", method=RequestMethod.POST)
	public void addProfessionalInfo(ProfessionalWithBLOBs professional, HttpServletRequest request,
			HttpServletResponse response , Model model){
		Integer professionId = null;
		if(professional != null){
			professional.setCreateUser((Integer)request.getSession().getAttribute(Constants.SESSION_ID));
			professional.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
			try {
				professionId = ps.insertProfessional(professional);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		outHttpString(response, professionId);
	}
	
	@RequestMapping(value = "addWorkExp" , method = RequestMethod.POST)
	@ResponseBody
	public void addProfessionWorkExp(ProfessionalWork professionWorkExp , HttpServletRequest request , HttpServletResponse response){
		if(professionWorkExp.getId() != null && !"".equals(professionWorkExp.getId())){
			professionWorkExp.setUpdateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
			professionWorkExp.setUpdateUser((Integer)request.getSession().getAttribute(Constants.SESSION_ID));
			int count =  pws.updateProfessionWork(professionWorkExp);
			outHttpString(response, count);
		}else{
			professionWorkExp.setCreateTime(DateUtil.getFormatDateTime(System.currentTimeMillis()));
			professionWorkExp.setCreateUser((Integer)request.getSession().getAttribute(Constants.SESSION_ID));
			int count = pws.addProfessionWorks(professionWorkExp);
			outHttpString(response, count);
		}
		
	}
	
	@RequestMapping("/workexp/{professWorkId}")
	@ResponseBody
	public void professionWorkexp(@PathVariable("professWorkId") Integer professWorkId, HttpServletResponse response){
		ProfessionalWork professionWork = pws.getProfessionWork(professWorkId);
		outHttpString(response, JSONObject.toJSON(professionWork));
	}
	
	@RequestMapping(value = "/workexp/del" , method = RequestMethod.POST)
	@ResponseBody
	public void professionWorkDel(ProfessionalWork professionWork , HttpServletResponse response){
		int count = pws.deleteById(professionWork.getId());
		outHttpString(response, count);
	}
	
	@RequestMapping(value="/file")
	public String file(){
		return "analyst/professional_file";
	}
	
	@ResponseBody
	@RequestMapping(value="/file",method=RequestMethod.POST)
	public void _file(
			@RequestParam(value="reports") String reports,
			@RequestParam(value="filenames") String filenames,
			HttpServletResponse response,
			HttpServletRequest request, 
			Model model){
		Map<String,String> resMap = new HashMap<>();
		try {
			long beginTime = System.currentTimeMillis();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss:SSS");
			Date begin = new Date(beginTime);
			System.out.println("开始时间" + dateFormat.format(begin));
			resMap = ps.uploadProfessionals(reports, filepath,fileUrl);
			long endTime = System.currentTimeMillis();
			Date end = new Date(endTime);
			System.out.println("结束时间:" + dateFormat.format(end));
			System.out.println("耗时:-->" + (endTime - beginTime));
		} catch (Exception e) {
			e.printStackTrace();
			outHttpString(response, false);
		}
		outHttpJson(response, resMap);
	}
	
	@RequestMapping(value="/sync",method=RequestMethod.GET)
	public void syncProfessionalIndex(){
		ps.test();
	}
	
	@RequestMapping("/delete")
	public void delete(@RequestParam (value="professionId") String professionId,
			@RequestParam(value="projectId") String projectId,HttpServletResponse response){
		Boolean isDelete = ps.deleteById(professionId,projectId);
		outHttpString(response, isDelete ? "成功" : "失败");
	}
	
	@RequestMapping(value = "toProfessionUpdate/{projectId}/{professioinId}", method=RequestMethod.POST)
	public void toEditProfession(@PathVariable(value="projectId") String projectId , 
			@PathVariable(value="professioinId") String professionId, HttpServletResponse response){
		
		ProjectProfessional projectProfession = ps.getProfessionInfoByProjectIdAndProfessionId(projectId,professionId);
		Map<String, Object> resutMap = new HashMap<>();
		resutMap.put("projectProfession", projectProfession);
		resutMap.put("type", 1);
		outHttpJson(response, resutMap);
	}
}
