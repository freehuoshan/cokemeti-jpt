package com.cokemeti.jpt.controller.sales;


import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.cokemeti.jpt.controller.common.BaseController;
import com.cokemeti.jpt.domain.attachement.Attachement;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.sales.Customer;
import com.cokemeti.jpt.service.sales.SalesService;
import com.cokemeti.jpt.util.DataTablesVoUtil;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.JsonUtil;
import com.cokemeti.jpt.util.ResultBean;

@Controller
@RequestMapping("/sales/attachement")
public class SalesAttachementController extends BaseController{
	private Logger logger = Logger.getLogger(SalesAttachementController.class);
	
	@Autowired
	private SalesService salesService;
	
	@ResponseBody
	@RequestMapping(value="/add")
	public ResultBean saveReports(@RequestParam(value="customerid") String customerid,
			@RequestParam(value="reports") String reports,
			@RequestParam(value="filenames") String filenames,
			HttpServletResponse response,
			HttpServletRequest request, 
			Model model){
		if(!StringUtils.isEmpty(reports)
				&& !StringUtils.isEmpty(filenames)
				&& !StringUtils.isEmpty(customerid)){
			String r[] = reports.split("\\|");
			String f[] = filenames.split("\\|");
			ArrayList<Attachement> list = new ArrayList<Attachement>();
			for(int i=0;i<r.length;i++){
				if(StringUtils.isEmpty(r[i])) continue;
				Attachement att = new Attachement();
				att.setName(r[i]);
				att.setRealname(f[i]);
				att.setFtype(getType(r[i]));//1:文件2:图片3压缩包
				att.setType(1);//1客户2项目3专家
				att.setCtime(DateUtil.getFormatDate(new Date()));
				list.add(att);
			}
			list = salesService.saveAttachement(list);
			Customer ct = salesService.getCustomerByid(Integer.valueOf(customerid).intValue());
			String ids = ct.getFileid();
			Customer record = new Customer();
			record.setId(Integer.valueOf(customerid).intValue());
			String fids = "";
			for(Attachement at:list){
					fids +=at.getId() + ",";
			}
			int len = fids.length()-1;
			if(fids.charAt(len)==','){
				fids = fids.substring(0,len);				
			}
			record.setFileid(ids==null?fids: ids+","+fids);
			salesService.saveFileIdByCustomerId(record);
			return JsonUtil.getJson(400, "保存成功");
		}else{
			return JsonUtil.getJson(400, "保存附件异常");
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/getlist")
	public DataTablesVo _list(@RequestParam(value="search[value]" ,defaultValue="") String search,
			@RequestParam(value="start",defaultValue = "0") String start,
			@RequestParam(value="length",defaultValue = "10") String length,
			@RequestParam(value="draw") int draw,
			@RequestParam(value="customerid",required=true) String id,
			HttpServletResponse response,
			HttpServletRequest request
			){
		DataTablesVo vo= null ;
		Customer c = salesService.getCustomerByid(Integer.valueOf(id).intValue());
		if(c==null) {
			vo = DataTablesVoUtil.getDataTablesVo(draw, 0, 0,new ArrayList<Attachement>());
		}else{
			String fids = c.getFileid();
			if(!StringUtils.isEmpty(fids)){
				vo =salesService.getAttachementList(fids, search, start, length, draw);
			}else{
				vo = DataTablesVoUtil.getDataTablesVo(draw, 0, 0,new ArrayList<Attachement>());
			}
			logger.info("**********************" + JSON.toJSONString(vo));
		}
		return vo;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(value="attachementId") String attachementId,
			HttpServletResponse response){
		Boolean isDelete = salesService.deleteAttachementById(attachementId);
		outHttpString(response, isDelete ? "成功" : "失败"); 
	}
	
	private static int getType(String filename){
		String[] fn = filename.split("\\.");
		String suffix = fn[1].toLowerCase();
		if ("jpg".equals(suffix) || "png".equals(suffix) || "jpeg".equals(suffix)) {
			return 2;
		}else if ("zip".equals(suffix) || "rar".equals(suffix) || "tar".equals(suffix)) {
			return 3;
		}else if ("xls".equals(suffix) || "xlsx".equals(suffix)
				|| "doc".equals(suffix)|| "doc".equals(suffix)
				|| "ppt".equals(suffix)|| "pptx".equals(suffix)
				) {
			return 1;
		}else{
			return 4;
		}
	}
}
