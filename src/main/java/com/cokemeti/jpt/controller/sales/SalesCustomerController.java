package com.cokemeti.jpt.controller.sales;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cokemeti.jpt.controller.common.BaseController;
import com.cokemeti.jpt.domain.admin.User;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.sales.Customer;
import com.cokemeti.jpt.domain.sales.CustomerAccount;
import com.cokemeti.jpt.domain.sales.CustomerCompany;
import com.cokemeti.jpt.domain.sales.vo.CustomerVo;
import com.cokemeti.jpt.service.admin.UserService;
import com.cokemeti.jpt.service.sales.SalesService;
import com.cokemeti.jpt.util.Constants;
import com.cokemeti.jpt.util.DateUtil;
import com.cokemeti.jpt.util.ResultBean;

@Controller
@RequestMapping("/sales/customer")
public class SalesCustomerController extends BaseController{
	private Logger logger = Logger.getLogger(SalesCustomerController.class);
	
	@Autowired
	private SalesService salesService;
	
	@Autowired 
	private UserService userService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String list(){
		return "sales/sales_customer_list";
	}
	
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String addcustomer(Model m){
		m.addAttribute("type","0");
		return "sales/sales_add_customer_plat";
	}
	
	@RequestMapping(value="/edit/{id}",method=RequestMethod.GET)
	public String edit(
			@PathVariable(value="id")int id,
			Model m
			){
		CustomerVo vo = salesService.getCustomerInfoByid(id);
		Customer custom = salesService.getCustomerByid(id);
		User createUser = userService.getUserById(custom.getCreateUser());
		
		m.addAttribute("id", id);
		m.addAttribute("vo", vo);
		m.addAttribute("type","1");
		m.addAttribute("create", createUser);
		return "sales/sales_add_customer_plat";
	}

	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public void delete(@RequestParam(value="id") String id, HttpServletResponse response){
		
		boolean isSuccess = salesService.deleteByCustomId(id);
		outHttpString(response , isSuccess ? "成功" : "失败");
	}
	
	@RequestMapping(value="/getlist")
	public void _list(@RequestParam(value="search[value]" ,defaultValue="") String search,
			@RequestParam(value="start",defaultValue = "0") String start,
			@RequestParam(value="length",defaultValue = "10") String length,
			@RequestParam(value="draw") int draw,
			HttpServletResponse response,
			HttpServletRequest request
			){
		DataTablesVo vo = salesService.getCustomers(search,start,length,draw);
		logger.info("**********************" + JSON.toJSONString(vo));
		outHttpJson(response, vo);
	}
	
	@ResponseBody
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ResultBean save(
			@RequestParam(value="id")String id,
			@RequestParam(value="name")String name,
			@RequestParam(value="phone")String phone,
			@RequestParam(value="email")String email,
			@RequestParam(value="company")String company,
			@RequestParam(value="industry")String industry,
			@RequestParam(value="website")String website,
			@RequestParam(value="description")String description,
			@RequestParam(value="prepay")String prepay,
			@RequestParam(value="type")String type,
			HttpServletRequest request
			){
		HttpSession session = request.getSession();
		Customer ct = new Customer();
		if("1".equals(type))
			ct.setId(Integer.valueOf(id));
		ct.setUsername(name);
		ct.setLevel("A");
		ct.setPhone(phone);
		ct.setEmail(email);
	    ct.setUpdateTime(DateUtil.getFormatDate(new Date()));
		ct.setStatus(1);
		
		
		CustomerCompany cc = new CustomerCompany();
		cc.setName(company);
		cc.setIndustry(industry);
		cc.setAddress(website);
		cc.setOrgNo(description);
		cc.setCreditNo(email);
		cc.setUpdateTime(DateUtil.getFormatDate(new Date()));
		CustomerAccount ca = new CustomerAccount();
		ca.setAmount(Double.valueOf(prepay));
		if("0".equals(type)){
			ca.setAmount(Double.valueOf(prepay));
			ca.setCreateTime(DateUtil.getFormatDate(new Date()));
		
			cc.setCreateTime(DateUtil.getFormatDate(new Date()));
			
			ct.setCreateTime(DateUtil.getFormatDate(new Date()));
			ct.setCreateUser((Integer)session.getAttribute(Constants.SESSION_ID));
		}
		ResultBean s = salesService.saveCustomer(ct, cc,ca,Integer.valueOf(type).intValue());			
		return s;
	}
}
