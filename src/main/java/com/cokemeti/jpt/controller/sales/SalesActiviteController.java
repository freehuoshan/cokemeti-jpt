package com.cokemeti.jpt.controller.sales;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cokemeti.jpt.controller.common.BaseController;
import com.cokemeti.jpt.domain.common.DataTablesVo;
import com.cokemeti.jpt.domain.project.Project;
import com.cokemeti.jpt.domain.sales.CustomerActivity;
import com.cokemeti.jpt.domain.sales.vo.CustomerVo;
import com.cokemeti.jpt.service.project.ProjectService;
import com.cokemeti.jpt.service.sales.SalesService;
import com.cokemeti.jpt.util.ResultBean;

@Controller
@RequestMapping("/sales/activite")
public class SalesActiviteController extends BaseController{
	private Logger logger = Logger.getLogger(SalesActiviteController.class);
	
	@Autowired
	private SalesService salesService;
	
	@Autowired
	private ProjectService projectService;
	
	@RequestMapping(value="/add/{id}",method=RequestMethod.GET)
	public String addactivite(@PathVariable(value="id") int id,
			Model m){
		m.addAttribute("type","0");
		m.addAttribute("id",id);
		CustomerVo vo = salesService.getCustomerInfoByid(id);
		m.addAttribute("vo",vo);
		List<Project> pjlist = projectService.getProjectInfosByCustomerId(id);
		m.addAttribute("pjlist",pjlist);
		return "sales/sales_add_activite";
	}
	
	@RequestMapping(value="/edit/{id}",method=RequestMethod.GET)
	public String edit(
			@PathVariable(value="id")int id,
			Model m
			){
		CustomerVo vo = salesService.getCustomerInfoByid(id);
		m.addAttribute("vo", vo);
		m.addAttribute("type","1");
		return "sales/sales_add_customer_plat";
	}
	
	@RequestMapping(value="/getlist")
	public void _list(@RequestParam(value="search[value]" ,defaultValue="") String search,
			@RequestParam(value="start",defaultValue = "0") String start,
			@RequestParam(value="length",defaultValue = "10") String length,
			@RequestParam(value="customerid",required=true) String customerid,
			@RequestParam(value="draw") int draw,
			HttpServletResponse response,
			HttpServletRequest request
			){
		DataTablesVo vo = salesService.getActivities(customerid,search, start, length, draw);
		logger.info("**********************" + JSON.toJSONString(vo));
		outHttpJson(response, vo);
	}
	
	@RequestMapping(value="/editoperate", method=RequestMethod.POST)
	public void editOperate(@RequestParam(value="id")String id,
			@RequestParam(value="theme")String theme,
			@RequestParam(value="method")String method,
			@RequestParam(value="single_cal")String time,
			@RequestParam(value="projectid",defaultValue="0")String projectid,
			@RequestParam(value="description")String description,
			@RequestParam(value="type")int type,
			@RequestParam(value="activityId")String activityId,
			HttpServletResponse response){
		
		String t[] = time.split("-");
		CustomerActivity ca = new CustomerActivity();
		ca.setTheme(theme);
		ca.setMethod(method);
		ca.setStarttime(t[0]);
		ca.setEndtime(t[1]);
		ca.setContent(description);
		if(!"0".equals(projectid)){			
			ca.setProjectId(Integer.valueOf(projectid).intValue());
		}
		ca.setCustomerId(id);
		ca.setId(Integer.valueOf(activityId));
		
		Boolean isUpdated = salesService.updateActivityById(ca);
		
		outHttpString(response, isUpdated ? "成功" : "失败"); 
		
	}
	
	@ResponseBody
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ResultBean save(
			@RequestParam(value="id")String id,
			@RequestParam(value="theme")String theme,
			@RequestParam(value="method")String method,
			@RequestParam(value="single_cal")String time,
			@RequestParam(value="projectid",defaultValue="0")String projectid,
			@RequestParam(value="description")String description,
			@RequestParam(value="type")int type
			){
		
		String t[] = time.split("-");
		CustomerActivity ca = new CustomerActivity();
		ca.setTheme(theme);
		ca.setMethod(method);
		ca.setStarttime(t[0]);
		ca.setEndtime(t[1]);
		ca.setContent(description);
		if(!"0".equals(projectid)){			
			ca.setProjectId(Integer.valueOf(projectid).intValue());
		}
		ca.setCustomerId(id);
		
		ResultBean s = salesService.saveActivite(ca, type);
		s.setData(id);
		return s;
	}
	
	@RequestMapping(value="/delete" , method=RequestMethod.POST)
	public void deleteActivityById(@RequestParam(value="activityId") String activityId , 
			HttpServletResponse response){
		Boolean isDelete = salesService.deleteActivityById(activityId);
		outHttpString(response, isDelete ? "成功" : "失败");
	}
	
	@RequestMapping(value="/edit/activity/{activityId}")
	public String toEditActivity(@PathVariable(value="activityId") String activityId,
			HttpServletResponse response, Model model){
		CustomerActivity activityInfo = salesService.getActivityById(activityId);
		model.addAttribute("type","1");
		model.addAttribute("id",activityInfo.getCustomerId());
		CustomerVo vo = salesService.getCustomerInfoByid(Integer.valueOf(activityInfo.getCustomerId()));
		model.addAttribute("vo",vo);
		List<Project> pjlist = projectService.getProjectInfosByCustomerId(Integer.valueOf(activityInfo.getCustomerId()));
		model.addAttribute("pjlist",pjlist);
		model.addAttribute("activityInfo",activityInfo);
		return "sales/sales_add_activite";
	}
}
