package com.cokemeti.jpt.domain.sales.vo;

import com.cokemeti.jpt.domain.sales.CustomerActivity;

public class ActivityVo extends CustomerActivity{

	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
