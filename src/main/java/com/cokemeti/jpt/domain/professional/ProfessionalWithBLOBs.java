package com.cokemeti.jpt.domain.professional;

public class ProfessionalWithBLOBs extends Professional {
    private String educationExp;

    private String workExp;

    private String projectExp;

    public String getEducationExp() {
        return educationExp;
    }

    public void setEducationExp(String educationExp) {
        this.educationExp = educationExp;
    }

    public String getWorkExp() {
        return workExp;
    }

    public void setWorkExp(String workExp) {
        this.workExp = workExp;
    }

    public String getProjectExp() {
        return projectExp;
    }

    public void setProjectExp(String projectExp) {
        this.projectExp = projectExp;
    }

	@Override
	public String toString() {
		return "ProfessionalWithBLOBs [educationExp=" + educationExp + ", workExp=" + workExp + ", projectExp="
				+ projectExp + "]";
	}
    
    
}