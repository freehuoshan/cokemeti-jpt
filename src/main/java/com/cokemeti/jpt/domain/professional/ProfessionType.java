/**
 * author: wangxj
 * create time: 下午12:59:20
 */
package com.cokemeti.jpt.domain.professional;

/**
 * @author huoshan
 * created by 2017年4月15日 下午12:59:20
 * 
 */
public class ProfessionType {
	
	private Integer id;
	private String type;
	private String createTime;
	private String updateTime;
	private Integer createUser;
	private Integer updateUser;
	private Integer projectId;
	
	public ProfessionType(){
		
	}

	public ProfessionType(Integer id, String type, String createTime, String updateTime, Integer createUser,
			Integer updateUser, Integer projectId) {
		super();
		this.id = id;
		this.type = type;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.createUser = createUser;
		this.updateUser = updateUser;
		this.projectId = projectId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Integer getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Integer updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	@Override
	public String toString() {
		return "ProfessionType [id=" + id + ", type=" + type + ", createTime=" + createTime + ", updateTime="
				+ updateTime + ", createUser=" + createUser + ", updateUser=" + updateUser + ", projectId=" + projectId
				+ "]";
	}

	
}
