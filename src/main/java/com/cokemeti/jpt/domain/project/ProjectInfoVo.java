/**
 * 
 */
package com.cokemeti.jpt.domain.project;

/**
 * @author Administrator
 *
 */
public class ProjectInfoVo extends ProjectInfo {

	private String createUserName;

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

}
