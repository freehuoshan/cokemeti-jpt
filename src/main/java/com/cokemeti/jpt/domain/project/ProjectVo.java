/**
 * 
 */
package com.cokemeti.jpt.domain.project;

/**
 * @author Administrator
 *
 */
public class ProjectVo extends Project {

	private String customerName;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
}
