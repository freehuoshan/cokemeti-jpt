/**
 * 
 */
package com.cokemeti.jpt.domain.project;

/**
 * @author Administrator
 *
 */
public class ProjectReportVo extends ProjectReport {

	private String porfessionalName;
	
	private String statusName;
	
	private String uploadUserName;
	
	private String typeName;

	public String getPorfessionalName() {
		return porfessionalName;
	}

	public void setPorfessionalName(String porfessionalName) {
		this.porfessionalName = porfessionalName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getUploadUserName() {
		return uploadUserName;
	}

	public void setUploadUserName(String uploadUserName) {
		this.uploadUserName = uploadUserName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	
}
