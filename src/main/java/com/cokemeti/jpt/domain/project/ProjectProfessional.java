package com.cokemeti.jpt.domain.project;

public class ProjectProfessional {
    private Integer id;

    private Integer projectId;
    
    private String professionalName;
    
    private Integer professionalId;

    private String title;

    private String company;

    private Integer status;

    private String attachment;

    private Double reportPrice;

    private Double professionalPrice;

    private Integer createUser;

    private String createTime;

    private Integer updateUser;

    private String updateTime;

    private String exp;
    
    private Integer professionType;
    
    private Integer customerStatus;
    //专家收费倍数
    private Double priceTimes; 

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getProfessionalId() {
		return professionalId;
	}

	public void setProfessionalId(Integer professionalId) {
		this.professionalId = professionalId;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Double getReportPrice() {
        return reportPrice;
    }

    public void setReportPrice(Double reportPrice) {
        this.reportPrice = reportPrice;
    }

    public Double getProfessionalPrice() {
        return professionalPrice;
    }

    public void setProfessionalPrice(Double professionalPrice) {
        this.professionalPrice = professionalPrice;
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

	public Integer getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Integer updateUser) {
		this.updateUser = updateUser;
	}

	public String getProfessionalName() {
		return professionalName;
	}

	public void setProfessionalName(String professionalName) {
		this.professionalName = professionalName;
	}

	public Integer getProfessionType() {
		return professionType;
	}

	public void setProfessionType(Integer professionType) {
		this.professionType = professionType;
	}

	public Integer getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(Integer customerStatus) {
		this.customerStatus = customerStatus;
	}

	public Double getPriceTimes() {
		return priceTimes;
	}

	public void setPriceTimes(Double priceTimes) {
		this.priceTimes = priceTimes;
	}

	
	
	
}