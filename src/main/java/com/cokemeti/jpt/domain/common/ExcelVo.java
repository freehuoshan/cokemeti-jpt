/**
 * 
 */
package com.cokemeti.jpt.domain.common;

import com.alibaba.fastjson.JSON;

/**
 * @author Administrator
 *
 */
public class ExcelVo {
	private String name;
	private String title;
	private String company;
	private String email;
	private String tel;
	private String price;
	private String workexp;
	private String desc;
	
	public ExcelVo(){
		
	}
	
	
	public ExcelVo(String name, String title, String company, String email, String tel, String price, String workexp,
			String desc) {
		super();
		this.name = name;
		this.title = title;
		this.company = company;
		this.email = email;
		this.tel = tel;
		this.price = price;
		this.workexp = workexp;
		this.desc = desc;
	}
	
	


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public String getWorkexp() {
		return workexp;
	}


	public void setWorkexp(String workexp) {
		this.workexp = workexp;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
}
