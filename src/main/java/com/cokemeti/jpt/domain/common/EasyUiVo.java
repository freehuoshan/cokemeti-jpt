/**
 * 
 */
package com.cokemeti.jpt.domain.common;

import java.util.List;

/**
 * @author xujunj
 *
 */
public class EasyUiVo {
	private long page;//页数
	private long total;//总件数
	private List<?> rows;//检索结果

	public long getPage() {
		return page;
	}
	public void setPage(long page) {
		this.page = page;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List getRows() {
		return rows;
	}
	public void setRows(List rows) {
		this.rows = rows;
	}
}
