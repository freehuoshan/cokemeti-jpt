/**
 * 
 */
package com.cokemeti.jpt.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.cokemeti.jpt.util.Constants;

/**
 * @author Administrator
 *
 */
public class SessionInterceptor implements HandlerInterceptor {

	private Logger logger = Logger.getLogger(SessionInterceptor.class);
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		logger.info("check session:::");
		
		/*String url = request.getRequestURI();
		logger.info("------------------->url:" +url);
		String u[] = url.split("/");
		
		if(u.length==0)
			return true;
		
		url = u[u.length - 1];
		
		if (url.equals("login") || url.equals("register") || url.equals("updatepwd")) {
			return true;
		}*/
		
		if (request.getSession().getAttribute(Constants.SESSION_ID) == null) {
			logger.info("------------------->url:session为空");
			String path =request.getContextPath();
			String str= "/login";
			
			response.sendRedirect(path +str);
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

}