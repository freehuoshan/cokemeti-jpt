package com.cokemeti.jpt.idao.attachement;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.cokemeti.jpt.domain.attachement.Attachement;
import com.cokemeti.jpt.domain.sales.vo.CustomerVo;

public interface AttachementMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Attachement record);

    int insertSelective(Attachement record);

    Attachement selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Attachement record);

    int updateByPrimaryKey(Attachement record);
    
    void insertByList(List<Attachement> list);
    
    List<Attachement> getAttachements(Map<String, Object> params);

    int getAttachementsTotal(Map<String, Object> params);
}