package com.cokemeti.jpt.idao.admin;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.admin.User;
import com.cokemeti.jpt.domain.project.Project;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);
    
    User selectByEmail(String email);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

	/**
	 * @param integer
	 * @param search
	 * @param valueOf
	 * @param valueOf2
	 * @return
	 */
	List<User> getUsers(@Param(value="id") Integer id,@Param(value="search") String search,@Param(value="bgrow")int bgrow,@Param(value="rows")int rows);

	/**
	 * @param integer
	 * @param search
	 * @return
	 */
	int getUserTotal(@Param(value="id") Integer id,@Param(value="search") String search);
}