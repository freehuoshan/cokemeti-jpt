package com.cokemeti.jpt.idao.admin;

import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.admin.UserLogin;

@Repository
public interface UserLoginMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserLogin record);

    int insertSelective(UserLogin record);

    UserLogin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserLogin record);

    int updateByPrimaryKey(UserLogin record);
}