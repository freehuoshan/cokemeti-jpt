package com.cokemeti.jpt.idao.project;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.project.ProjectReport;
import com.cokemeti.jpt.domain.project.ProjectReportVo;
@Repository
public interface ProjectReportMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProjectReport record);

    int insertSelective(ProjectReport record);

    ProjectReport selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProjectReport record);

    int updateByPrimaryKey(ProjectReport record);
    
    List<ProjectReportVo> getProjectReportsByProjectId(@Param(value="projectId")Integer projectId);
}