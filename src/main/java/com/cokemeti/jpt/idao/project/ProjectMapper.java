package com.cokemeti.jpt.idao.project;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.project.Project;
@Repository
public interface ProjectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Project record);

    int insertSelective(Project record);

    Project selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Project record);

    int updateByPrimaryKeyWithBLOBs(Project record);

    int updateByPrimaryKey(Project record);
    
    List<Project> getProjects(@Param(value="id") Integer id,@Param(value="search") String search,@Param(value="bgrow")int bgrow,@Param(value="rows")int rows);

    int getProjectsTotal(@Param(value="id") Integer id,@Param(value="search") String search);
    
    List<Project> getProjectInfosByCustomerId(@Param(value="id")Integer id);
}