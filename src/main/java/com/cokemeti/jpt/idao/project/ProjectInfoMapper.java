package com.cokemeti.jpt.idao.project;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.project.ProjectInfo;
import com.cokemeti.jpt.domain.project.ProjectInfoVo;

@Repository
public interface ProjectInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProjectInfo record);

    int insertSelective(ProjectInfo record);

    ProjectInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProjectInfo record);

    int updateByPrimaryKeyWithBLOBs(ProjectInfo record);

    int updateByPrimaryKey(ProjectInfo record);
    
    List<ProjectInfoVo> getProjectInfos(@Param(value="projectId")Integer projectId);
}