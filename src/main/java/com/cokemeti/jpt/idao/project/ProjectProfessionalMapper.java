package com.cokemeti.jpt.idao.project;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.project.ProjectProfessional;
@Repository
public interface ProjectProfessionalMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProjectProfessional record);

    int insertSelective(ProjectProfessional record);

    ProjectProfessional selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProjectProfessional record);

    int updateByPrimaryKeyWithBLOBs(ProjectProfessional record);

    int updateByPrimaryKey(ProjectProfessional record);
    
    List<ProjectProfessional> getPpByProjectId(@Param(value="projectId")Integer projectId,@Param(value="bgrow")int bgrow,@Param(value="rows")int rows);
    
    List<ProjectProfessional> getAllPpByProjectId(@Param(value="projectId")Integer projectId);
   
    List<ProjectProfessional> getAllPpByProjectIdWithStatus(@Param(value="projectId")Integer projectId, @Param(value="status")Integer status);
    
    List<ProjectProfessional> getAllPpByProfessionalId(@Param(value="professionalId")Integer professionalId);
    
    int getPpCount(@Param(value="projectId")Integer projectId);
    
    ProjectProfessional getPPByProjectIdAndProfessionalId(@Param(value="projectId")Integer projectId,@Param(value="professionalId")Integer professionalId);

	int deleteByProfessProjectId(Map<String, Integer> paramMap);

	/**根据projectId获取所有专家分类
	 * @param valueOf
	 * @return
	 */
	List<Integer> getAllProfessionTypeByProjectId(Integer projectId);

	/**根据professionType获取,专家列表
	 * @param professionTypeId
	 * @return
	 */
	List<ProjectProfessional> getAllPpByProfessionType(Integer professionTypeId);

	/**
	 * @param projectId
	 * @param professionTypeId
	 * @return
	 */
	List<ProjectProfessional> getDataByProjectIdAndProfessionTypeId(@Param("projectId") Integer projectId,
			@Param("professionTypeId")Integer professionTypeId);

	/**
	 * @param valueOf
	 * @param ppidList
	 * @return
	 */
	List<Integer> getProfessionTypesByProjectIdAndPpids(@Param("projectId")Integer projectId,@Param("ids") List<Integer> ids);

	/**
	 * @param ppidList
	 * @return
	 */
	List<ProjectProfessional> getDataByPPidsAndProfessionType(@Param("ids")List<Integer> ppidList, @Param("professionTypeId") Integer professionTypeId);

	/**
	 * @param projectProfession
	 * @return
	 */
	int deleteProfessionByTypeAndProject(ProjectProfessional projectProfession);


}