package com.cokemeti.jpt.idao.professional;

import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.professional.ProfessionalComment;

@Repository
public interface ProfessionalCommentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProfessionalComment record);

    int insertSelective(ProfessionalComment record);

    ProfessionalComment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProfessionalComment record);

    int updateByPrimaryKeyWithBLOBs(ProfessionalComment record);

    int updateByPrimaryKey(ProfessionalComment record);
}