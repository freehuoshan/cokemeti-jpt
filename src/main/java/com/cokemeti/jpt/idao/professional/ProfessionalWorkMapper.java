package com.cokemeti.jpt.idao.professional;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.professional.ProfessionalWork;
@Repository
public interface ProfessionalWorkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProfessionalWork record);

    int insertSelective(ProfessionalWork record);

    ProfessionalWork selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProfessionalWork record);

    int updateByPrimaryKeyWithBLOBs(ProfessionalWork record);

    int updateByPrimaryKey(ProfessionalWork record);
    
    List<ProfessionalWork> getProfessionalWorksByProfessionalId(Integer professional_id);
}