package com.cokemeti.jpt.idao.professional;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.professional.Professional;
import com.cokemeti.jpt.domain.professional.ProfessionalWithBLOBs;
@Repository
public interface ProfessionalMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProfessionalWithBLOBs record);

    int insertSelective(ProfessionalWithBLOBs record);

    ProfessionalWithBLOBs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProfessionalWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ProfessionalWithBLOBs record);

    int updateByPrimaryKey(Professional record);
    
    List<Professional> getProfessionals();

	/**
	 * @param professionPhone
	 * @return
	 */
	List<Professional> selectByCondition(ProfessionalWithBLOBs professionPhone);
	
	List<ProfessionalWithBLOBs> mohuQuery(Map<String, Object> params);
	
	int mohuCount(Map<String, Object> params);
}