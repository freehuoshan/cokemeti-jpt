package com.cokemeti.jpt.idao.professional;

import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.professional.ProfessionalRelationWork;
@Repository
public interface ProfessionalRelationWorkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProfessionalRelationWork record);

    int insertSelective(ProfessionalRelationWork record);

    ProfessionalRelationWork selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProfessionalRelationWork record);

    int updateByPrimaryKeyWithBLOBs(ProfessionalRelationWork record);

    int updateByPrimaryKey(ProfessionalRelationWork record);
}