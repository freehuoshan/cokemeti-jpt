
package com.cokemeti.jpt.idao.professional;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.professional.ProfessionType;

/**
 * @author huoshan
 * created by 2017年4月15日 下午1:17:36
 * 
 */
@Repository
public interface ProfessionTypeMapper {
	
	/**
	 * 添加
	 * @param professionType
	 * @return
	 */
	public Integer insert(ProfessionType professionType);
	/**
	 * 根据主键id更新
	 * @param professionType
	 * @return
	 */
	public Integer updateByUuid(ProfessionType professionType);
	/**
	 * 条件查询
	 * @param professionType
	 * @return
	 */
	public List<ProfessionType> selectByCondition(ProfessionType professionType);	
	/**
	 * 条件查询数量
	 * @param professionType
	 * @return
	 */
	public Integer selectCountByCondition(ProfessionType professionType);
	/**
	 * @param professiontype
	 * @return
	 */
	public int deleteById(ProfessionType professiontype);

}
