package com.cokemeti.jpt.idao.sales;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cokemeti.jpt.domain.sales.CustomerActivity;
import com.cokemeti.jpt.domain.sales.vo.ActivityVo;

public interface CustomerActivityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CustomerActivity record);

    int insertSelective(CustomerActivity record);

    CustomerActivity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CustomerActivity record);

    int updateByPrimaryKey(CustomerActivity record);
    
    List<ActivityVo> getActivities(@Param(value="customerid") String customerid,@Param(value="search") String search,@Param(value="bgrow")int bgrow,@Param(value="rows")int rows);
    
    int getActivitiesTotal(@Param(value="customerid") String customerid,@Param(value="search") String search);
    
    int deleteByCustomId(Integer customId);
}