package com.cokemeti.jpt.idao.sales;

import com.cokemeti.jpt.domain.sales.CustomerAccountLog;

public interface CustomerAccountLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CustomerAccountLog record);

    int insertSelective(CustomerAccountLog record);

    CustomerAccountLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CustomerAccountLog record);

    int updateByPrimaryKey(CustomerAccountLog record);
    
    int deleteByCustomId(Integer customId);
}