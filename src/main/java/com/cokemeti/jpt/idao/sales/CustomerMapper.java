package com.cokemeti.jpt.idao.sales;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.cokemeti.jpt.domain.sales.Customer;
import com.cokemeti.jpt.domain.sales.vo.CustomerVo;

@Repository
public interface CustomerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Customer record);

    int insertSelective(Customer record);

    Customer selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Customer record);

    int updateByPrimaryKey(Customer record);
    
    List<CustomerVo> getCustomers(@Param(value="search") String search,@Param(value="bgrow")int bgrow,@Param(value="rows")int rows);
    
    int getCustomersTotal(@Param(value="search") String search);
}