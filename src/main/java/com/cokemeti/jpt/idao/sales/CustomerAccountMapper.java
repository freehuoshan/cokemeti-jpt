package com.cokemeti.jpt.idao.sales;

import com.cokemeti.jpt.domain.sales.CustomerAccount;

public interface CustomerAccountMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CustomerAccount record);

    int insertSelective(CustomerAccount record);

    CustomerAccount selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CustomerAccount record);

    int updateByPrimaryKey(CustomerAccount record);
    
    CustomerAccount selectAccountByUserid(int id);
    
    int deleteByCustomId(Integer customId);

	/**
	 * @param ca
	 */
	void updateByCustomId(CustomerAccount ca);
}