package com.cokemeti.jpt.idao.sales;

import com.cokemeti.jpt.domain.sales.CustomerCompany;

public interface CustomerCompanyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CustomerCompany record);

    int insertSelective(CustomerCompany record);

    CustomerCompany selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CustomerCompany record);

    int updateByPrimaryKey(CustomerCompany record);
}